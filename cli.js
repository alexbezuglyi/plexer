#!/usr/bin/env node

const { Command } = require('commander');
const { Plexer, pl2js: pl } = require('plexer');

const fs = require("fs");

const program = new Command();
const plex = new Plexer();

program
    .command('exec <script>')
    .description('An application for executing PL/I scripts')
    .option('-e, --exec_mode <mode>', 'Which exec mode to use', 'fast')
    .action((script, options) => {
        try {
            var src = fs.readFileSync(script, "utf8");
            let preped = plex.preproc(src);
            const translated = plex.parse(preped).join('\n');
            if (translated.length) fs.writeFileSync('lastsrc.js', translated, [encoding = "utf8", flag = 'w']);
            console.log(eval(translated));
        } catch (error) {
            console.error(error);
        }
    });

program
    .command('save <script>')
    .description('An application for converting PL/I scripts')
    .action((script, options) => {
        try {
            var src = fs.readFileSync(script, "utf8");
            let preped = plex.preproc(src);
            const translated = plex.parse(preped).join('\n');
            console.log(translated);
        } catch (error) {
            console.error(error);
        }
    }).addHelpText('after', `
Examples:
  $ node plexer exec helloworld.pli`);



try {
    program.parse(process.argv);
} catch (error) {
    console.error(error);
}

