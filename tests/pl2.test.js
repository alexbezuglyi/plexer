
const Plexer = require('plexer').Plexer;
const { pl2js: pl } = require("plexer");
const fs = require("fs");

const plex = new Plexer();

test('Tests in PLI sources', () => {
    try {
        const testdir = "./tests/plisrcs/";
        fs.readdirSync(testdir).forEach(file => {
            var src = fs.readFileSync(testdir + file, "utf8");
            let preped = plex.preproc(src);
            const main = plex.parse(preped).join('\n');
            // console.log(eval(main));
            expect(eval(main)).toBe(1);
        });

    } catch (error) {
        console.error(error);
    }
})


