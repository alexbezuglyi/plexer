#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri May  8 12:10:26 2020

@author: oleksii.bezuhlyi
formerly SETn3270
"""

import socket
import thread
import struct
import select
import binascii
import redis
from ScreenManager import *
from sysgen import sysgen
# from collections import namedtuple

# Telnet protocol commands
SE   = chr(240) #End of subnegotiation parameters
SB   = chr(250) #Sub-option to follow
WILL = chr(251) #Will; request or confirm option begin
WONT = chr(252) #Wont; deny option request
DO   = chr(253) #Do = Request or confirm remote option
DONT = chr(254) #Don't = Demand or confirm option halt
IAC  = chr(0xff) #Interpret as Command
SEND = chr(1) #Sub-process negotiation SEND command
IS   = chr(0) #Sub-process negotiation IS command

#TN3270 Telnet Commands
TN_ASSOCIATE  = chr(0)
TN_CONNECT    = chr(1)
TN_DEVICETYPE = chr(2)
TN_FUNCTIONS  = chr(3)
TN_IS         = chr(4)
TN_REASON     = chr(5)
TN_REJECT     = chr(6)
TN_REQUEST    = chr(7)
TN_RESPONSES  = chr(2)
TN_SEND       = chr(8)
TN_EOR        = chr(0xef) #End of Record

#TN3270 Attention Identification (AIDS)
#####
# SoF ## Left this as hex because i coulnd't
#        be bothered to convert to decimal
#####
NO      = chr(0x60) #no aid
QREPLY  = chr(0x61) #reply
ENTER   = chr(0x7d) #enter
PF1     = chr(0xf1)
PF2     = chr(0xf2)
PF3     = chr(0xf3)
PF4     = chr(0xf4)
PF5     = chr(0xf5)
PF6     = chr(0xf6)
PF7     = chr(0xf7)
PF8     = chr(0xf8)
PF9     = chr(0xf9)
PF10    = chr(0x7a)
PF11    = chr(0x7b)
PF12    = chr(0x7c)
PF13    = chr(0xc1)
PF14    = chr(0xc2)
PF15    = chr(0xc3)
PF16    = chr(0xc4)
PF17    = chr(0xc5)
PF18    = chr(0xc6)
PF19    = chr(0xc7)
PF20    = chr(0xc8)
PF21    = chr(0xc9)
PF22    = chr(0x4a)
PF23    = chr(0x4b)
PF24    = chr(0x4c)
OICR    = chr(0xe6)
MSR_MHS = chr(0xe7)
SELECT  = chr(0x7e)
PA1     = chr(0x6c)
PA2     = chr(0x6e)
PA3     = chr(0x6b)
CLEAR   = chr(0x6d)
SYSREQ  = chr(0xf0)

#TN3270 Stream Orders
SF  = chr(0x1D)
SFE = chr(0x29)
SBA = chr(0x11)
SA  = chr(0x28)
MF  = chr(0x2C)
IC  = chr(0x13)
PT  = chr(0x5)
RA  = chr(0x3C)
EUA = chr(0x12)
GE  = chr(0x8)

#Supported Telnet Subcommand
Subcommand = {
    'BINARY'  : chr(0),
    'EOR'     : chr(25),
    'TTYPE'   : chr(24),
    'TN3270E'  : chr(40)
  }

DATA_TYPE = {
    '3270-DATA':        chr(0x00),   #The data portion of the message contains only the 3270 data stream.
    'SCS-DATA':         chr(0x01),   #The data portion of the message contains SNA Character Stream data.
    'RESPONSE'  :       chr(0x02),   #The data portion of the message constitutes device-status information and the RESPONSE-FLAG field indicates whether this is a positive or negative response (see below).
    'BIND-IMAGE' :      chr(0x03),   #The data portion of the message is the SNA bind image from the session established between the server and the host application.
    'UNBIND'   :        chr(0x04),   #The data portion of the message is an Unbind reason code.
    'NVT-DATA' :        chr(0x05),   #The data portion of the message is to be interpreted as NVT data.
    'REQUEST' :         chr(0x06),   #There is no data portion present in the message.  Only the REQUEST-FLAG field has any meaning.
    'SSCP-LU-DATA':     chr(0x07)    #The data portion of the message is data from the SSCP-LU session.
}

ERR_COND_CLEARED    = chr(0x00)


ADDR = ('', 23567)

def formatScreen(scr):
    # DFLD = chr(0)+chr(0)+chr(2)+chr(0)+chr(0)+chr(0xF5)+chr(0xC3)
    DFLD = struct.pack(">bbbH", 0, 0, 0, scr.TN3270Header["SEQ-NUMBER"]) + chr(0xF5) + chr(0xC3)
    scr.TN3270Header["SEQ-NUMBER"] += 1
    DFLD += SBA + scr.CURSOR + IC
    for key in scr.DFLD:
       DFLD += SBA + scr.DFLD[key][0] + SF + chr(scr.DFLD[key][3]) + T(scr.DFLD[key][4])

    return DFLD  + IAC + TN_EOR


def recv_tn(clientsock, timeout=100):
    rready,wready,err = select.select( [clientsock, ], [], [], timeout)
    #print len(rready)
    if len(rready):
        data = clientsock.recv(1920)
    else:
        data = ''
    return data

def get_all(sox):
    #TODO: something
    data = ''
    while True:
        d = recv_tn(sox,1)
        if not d:
            break
        else:
            data += d
    return data

def handler(clientsock, addr, r):
    screen = [TN3270screen()]
    f = open(r"example/HLWRLD.hlasm", "r")
    tree = screen[0].parse(f.read())
    screen[0].compile_tree(tree)
    
    #Begin TN3270E negotiation:
# =============================================================================
    clientsock.sendall( IAC + DO + Subcommand['TN3270E'])
    data  = recv_tn(clientsock)
    assert(data == IAC + WILL + Subcommand['TN3270E'])
# =============================================================================
    
# =================DEVICE-TYPE=================================================
    clientsock.sendall( IAC + SB + Subcommand['TN3270E'] + TN_SEND + TN_DEVICETYPE + IAC + SE)
    data  = recv_tn(clientsock)
    # print (data[5:-2])
    assert(data[:5] == IAC + SB + Subcommand['TN3270E'] + TN_DEVICETYPE + TN_REQUEST)
    assert(data[-2:] == IAC + SE)    
    clientsock.sendall( IAC + SB + Subcommand['TN3270E'] + TN_DEVICETYPE + TN_IS \
                       + "IBM-3278-2-E" + TN_CONNECT + "TCP" + str(addr[1]) + IAC + SE)
    screen[0].cur_dfld_type = 2
# =============================================================================

# =================FUNCTIONS REQUEST===========================================
    data  = recv_tn(clientsock)
    # assert(data == IAC + SB + Subcommand['TN3270E'] + TN_FUNCTIONS + TN_REQUEST \
    #        + TN_ASSOCIATE + TN_DEVICETYPE + TN_IS + IAC + SE)
    # print(2)   
    clientsock.sendall( IAC + SB + Subcommand['TN3270E'] + TN_FUNCTIONS + TN_IS \
                       + TN_ASSOCIATE + TN_DEVICETYPE + TN_IS + IAC + SE)
# =============================================================================

    # clientsock.sendall( DATA_TYPE['UNBIND'] + ERR_COND_CLEARED + chr(0) + chr(0) + chr(0)+ chr(1)+ IAC + TN_EOR )
    HeaderData = "TELNET".encode('cp500')
    clientsock.sendall( DATA_TYPE['BIND-IMAGE'] + ERR_COND_CLEARED \
                        + struct.pack(">BH", 0, 0) + HeaderData + IAC + TN_EOR )

    while True:
        clientsock.sendall(formatScreen(screen[0]))
        data  = recv_tn(clientsock)
        data  += get_all(clientsock)
        #print(binascii.hexlify(data))
        if not UserHandler(data, screen, r):
            break

    clientsock.close()
    print ("[+] Connection Closed", addr)

def UserHandler(data, screen, r):
    # print(binascii.hexlify(data))
    if len(data) <= 5:
        return True
    
    data = data[5:] # git rid of the header

    AID = data[0]
    if AID == CLEAR:
        screen[0] = TN3270screen()
        return True
    
    if len(data) <= 5:
        return True
    screen[0].CURSOR = data[1:1+2]

    for field in data[3:-2].split(SBA):
        # fields[binascii.hexlify(field[0:2])] = field[2:].decode('cp500')
        # print(binascii.hexlify(field))
        pos = field[0:2]
        if pos in screen[0].DFLD:
            # strlen = screen[0].DFLD[pos][2]
            # screen[0].DFLD[pos][4] = field[2:strlen].decode('cp500')#.[0:strlen]
            screen[0].DFLD[pos][4] = field[2:].decode('cp500')
            # print(screen[0].DFLD[pos][1] +": l="+ str(strlen) + " val: " + field[2:].decode('cp500'))

    if AID == ENTER:
        formatted = data.decode('cp500').upper()
        i = formatted.find("/FOR ")
        if i >= 0:
            screnname = formatted[i+5:-2]
            f = open(r"example/" + screnname + r".hlasm", "r")
            screen[0].TRAN = sysgen[screnname]
            tree = screen[0].parse(f.read())
            screen[0].compile_tree(tree)
            return True
        i = formatted.find("/QUIT")
        if i >= 0:
            return False
        
    if not screen[0].MFLDsend(r, AID):
            print ("send failed")
            return False
        
    screen[0].MFLDrecieve(r)
    return True

def T(text):
    return str((text).encode('cp500'))

tnsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tnsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

tnsock.bind(ADDR)
tnsock.listen(5)

r = redis.Redis('redis')

while 1:
    clientsock, addr = tnsock.accept()
    print ('[+] Connected to:', addr)
    thread.start_new_thread(handler, (clientsock, addr, r))
