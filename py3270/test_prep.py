# -*- coding: utf-8 -*-
"""
Created on Sun Dec 27 13:59:09 2020

@author: oleksii.bezuhlyi
"""


import unittest
from prep import preprocessor
from ScreenManager import TN3270screen, ENTER, PF1, PF2, PF3, PF4, PF5, PF6

subts = {r"\%PR\%": "PR",
         r"\%I\%" : "info",
         r"\%D\%" : "date",
         r"\%T\%" : "time"}

test = '''         TITLE '%PR%TEST1/%I% %D% %T%'                       
***  DIF/DOF FOR THE TN3270 SCREEN
         PRINT NOGEN                                                            
%PR%TSTFD FMT 
         DEV   FEAT=IGNORE,PFK=(TRANS,                                 X
               '%PR%TRANS1 F   ',    COMMENT                           X
               '%PR%TRANS1 2   ',    COMMENT                           X
               '/FOR %PR%TRAN2.',    COMMENT                           X
               '%PR%TRANS1 21  ',    COMMENT                           X
               '%PR%TRANS1 24  ',    COMMENT                           X
               ),DSCA=X'0090',SYSMSG=SYSMSG,TYPE=3270-A03              

         DPAGE CURSOR=((3,28,CURSOR)),FILL=PT
*...     DEVICE FIELDS FOR LINE 5                                       
*                                                                       
         DFLD  '-------------------------------------------------------X
               ------------------------',POS=(5,2)                      
*                                                                       
*...     DEVICE FIELDS FOR LINE 6                                       
*                                                                       
         DFLD  '      QWERTY UIOP ASDF GHJK L ZXCVBN M      0987654321DX
                END    OF TEST MESSAGE     ',POS=(6,2),                X
               ATTR=(HI) 
COMMND   DFLD  POS=(1,11),LTH=6,ATTR=(MOD)  CMNT ONE MORE
         DFLD  'ANDSOME EXTRA TEST TEXT (%PR%TRAN2)',                  X
               POS=(1,25),ATTR=(HI)  
         DFLD  'MFS TEST DETAIL (%PR%TRAN2)',POS=(1,25),ATTR=(HI) CMNT
         DFLD  'AUTO COMPLETE PROFILES         ',POS=(05,31) COMMENT2
*                                                                        
%PR%TSTFM   MSG   TYPE=INPUT,SOR=(%PR%TSTFD),NXT=%PR%TEST1
         MFLD  (TRANS,'PRTRAN3 E    '),LTH=13
         MFLD  'Z',LTH=1
         MFLD  (SCRNME,'%PR%TSTTST')                                             
         MFLD  (SCRDT,DATE2)      DATE MM/DD/YY                                 
%PR%TSTTST  MSG  TYPE=OUTPUT,SOR=(%PR%TSTOD,IGNORE),NXT=%PR%TSTOM,FILL=PT
LABEL    MFLD  (SCRTM,TIME)       TIME HH:MM                                    
         MFLD  ERRREP,LTH=3,ATTR=YES 
         MFLD  (,SCA),LTH=2                                                              
         MFLD  OCOMM,LTH=6,FILL=C' '                                   
         MFLD  COMMND,LTH=6  
'''

output = '''         TITLE 'PRTEST1/info date time'
         PRINT NOGEN
PRTSTFD FMT 
         DEV   FEAT=IGNORE,PFK=(TRANS,'PRTRANS1 F   ','PRTRANS1 2   ','/FOR PRTRAN2.','PRTRANS1 21  ','PRTRANS1 24  ',),DSCA=X'0090',SYSMSG=SYSMSG,TYPE=3270-A03              

         DPAGE CURSOR=((3,28,CURSOR)),FILL=PT
         DFLD  '-------------------------------------------------------------------------------',POS=(5,2)                      
         DFLD  '      QWERTY UIOP ASDF GHJK L ZXCVBN M      0987654321D END    OF TEST MESSAGE     ',POS=(6,2),ATTR=(HI) 
COMMND   DFLD  POS=(1,11),LTH=6,ATTR=(MOD)
         DFLD  'ANDSOME EXTRA TEST TEXT (PRTRAN2)',POS=(1,25),ATTR=(HI)  
         DFLD  'MFS TEST DETAIL (PRTRAN2)',POS=(1,25),ATTR=(HI)
         DFLD  'AUTO COMPLETE PROFILES         ',POS=(05,31)
PRTSTFM   MSG   TYPE=INPUT,SOR=(PRTSTFD),NXT=PRTEST1
         MFLD  (TRANS,'PRTRAN3 E    '),LTH=13
         MFLD  'Z',LTH=1
         MFLD  (SCRNME,'PRTSTTST')
         MFLD  (SCRDT,DATE2)
PRTSTTST  MSG  TYPE=OUTPUT,SOR=(PRTSTOD,IGNORE),NXT=PRTSTOM,FILL=PT
LABEL    MFLD  (SCRTM,TIME)
         MFLD  ERRREP,LTH=3,ATTR=YES
         MFLD  (,SCA),LTH=2
         MFLD  OCOMM,LTH=6,FILL=C' '
         MFLD  COMMND,LTH=6
'''

tree = [['TITLE', '', [('STR', ['PRTEST1/info date time'])]], 
        ['PRINT', '', [('ID', ['NOGEN'])]], 
        ['FMT', 'PRTSTFD'], 
        ['DEV', '', [('FEAT', ['IGNORE']), 
                     ('PFK', ['TRANS', 'PRTRANS1 F   ', 'PRTRANS1 2   ', '/FOR PRTRAN2.', 'PRTRANS1 21  ', 'PRTRANS1 24  ', None]), 
                     ('DSCA', [144]), 
                     ('SYSMSG', ['SYSMSG']), 
                     ('TYPE', ['3270-A03'])]], 
        ['DPAGE', '', [('CURSOR', ['3', '28', 'CURSOR']), ('FILL', ['PT'])]],
        ['DFLD', '', [('STR', ['-------------------------------------------------------------------------------']), ('POS', ['5', '2'])]], 
        ['DFLD', '', [('STR', ['      QWERTY UIOP ASDF GHJK L ZXCVBN M      0987654321D END    OF TEST MESSAGE     ']), ('POS', ['6', '2']), ('ATTR', ['HI'])]], 
        ['DFLD', 'COMMND', [('POS', ['1', '11']), ('LTH', ['6']), ('ATTR', ['MOD'])]], 
        ['DFLD', '', [('STR', ['ANDSOME EXTRA TEST TEXT (PRTRAN2)']), ('POS', ['1', '25']), ('ATTR', ['HI'])]], 
        ['DFLD', '', [('STR', ['MFS TEST DETAIL (PRTRAN2)']), ('POS', ['1', '25']), ('ATTR', ['HI'])]], 
        ['DFLD', '', [('STR', ['AUTO COMPLETE PROFILES         ']), ('POS', ['05', '31'])]], 
        ['MSG', 'PRTSTFM', [('TYPE', ['INPUT']), ('SOR', ['PRTSTFD']), ('NXT', ['PRTEST1'])]], 
        ['MFLD', '', [(None, ['TRANS', 'PRTRAN3 E    ']), ('LTH', ['13'])]], 
        ['MFLD', '', [('STR', ['Z']), ('LTH', ['1'])]],
        ['MFLD', '', [(None, ['SCRNME', 'PRTSTTST'])]], 
        ['MFLD', '', [(None, ['SCRDT', 'DATE2'])]], 
        ['MSG', 'PRTSTTST', [('TYPE', ['OUTPUT']), ('SOR', ['PRTSTOD','IGNORE']), ('NXT', ['PRTSTOM']), ('FILL', ['PT'])]], 
        ['MFLD', 'LABEL', [(None, ['SCRTM', 'TIME'])]], 
        ['MFLD', '', [('ID', ['ERRREP']), ('LTH', ['3']), ('ATTR', ['YES'])]], 
        ['MFLD', '', [(None, [None, 'SCA']), ('LTH', ['2'])]], 
        ['MFLD', '', [('ID', ['OCOMM']), ('LTH', ['6']), ('FILL', [' '])]], 
        ['MFLD', '', [('ID', ['COMMND']), ('LTH', ['6'])]]]

PFKtst = {ENTER: 'TRANS', 
          PF1: 'PRTRANS1 F   ', 
          PF2: 'PRTRANS1 2   ', 
          PF3: '/FOR PRTRAN2.', 
          PF4: 'PRTRANS1 21  ', 
          PF5: 'PRTRANS1 24  ', 
          PF6: None}

DFLDtst = [{},{},{'@\xc1': ['@@', '', 1, 1, ' ']},
           {'@\xc1': ['@@', '', 1, 1, ' '], 
            '\xc5\xc1':['\xc5@', '', 0, 240, '-------------------------------------------------------------------------------'],
           '\xc6\xd1':['\xc6P', '', 0, 248, '      QWERTY UIOP ASDF GHJK L ZXCVBN M      0987654321D END    OF TEST MESSAGE     '],
           '@J':['@\xc9', 'COMMND', 6, 1, ''],
           '@\xd8':['@\xd7', '', 0, 248, 'MFS TEST DETAIL (PRTRAN2)'],
           '\xc5^':['\xc5]', '', 0, 240, 'AUTO COMPLETE PROFILES         ']},{},{}]


class TestPrep(unittest.TestCase):
    
    def test_preprocessor(self):
        self.result = preprocessor(test, subts)
        self.assertEqual(self.result, output)
        
    def test_parser(self):
        scr = TN3270screen()
        self.result = scr.parse(output, subts)
        self.assertEqual(self.result, tree)
        
    def test_tree(self):
        scr = TN3270screen()
        scr.compile_tree(tree)
        self.assertEqual(scr.PFK, PFKtst)
        self.assertEqual(scr.CURSOR, scr.POS(3,28))
        self.assertEqual(scr.DFLDs, DFLDtst)
        self.assertEqual(scr.MSGTYPE, 'OUTPUT')
        # print(scr.MFLDin)
        # print(scr.MFLDout)





if __name__ == '__main__':
    unittest.main()

