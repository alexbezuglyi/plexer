import ply.lex as lex
import ply.yacc as yacc

tokens = (
    'NUMBER',
    'LBL',
    'INSTR',
    'ID',
    'STR',
    'COMA'
 )

t_COMA    = r'\,'

literals = ['=', ',', '(', ')', "'"]
instructions = []

def t_HEX(t):
    r"X\'[\dA-F]+\'"
    t.type = 'NUMBER'
    t.value = int(t.value[2:-1], 16)    
    return t

def t_CHAR(t):
    r"C\'.+\'"
    t.type = 'STR'
    t.value = t.value[2:-1]
    return t

def t_ID(t):
    r'[A-Z0-9][A-Z0-9\-\:\/\.]{0,7}'
    # line_start = input.rfind('\n', 0, token.lexpos) + 1
    if t.value in instructions:
        t.type = 'INSTR'
    return t

def t_STR(t):
    # r'\'(.+)\''
    r"\'([^\']+)\'"
    t.value = t.value[1:-1]
    return t

def t_NUMBER(t):
    r'\d+'
    t.value = int(t.value)    
    return t

def t_LBL(t):
    r'\n\r?[A-Z]\w{0,7}'
    t.value = t.value[1:]
    return t

# def t_COMMENT(t):
#     # r',[\s\w]+\sX\s*|\*.*'
#     r',[\s\w]+X\s*|\*.*'
#     pass

def t_NL(t):
    r'\n'
    t.lexer.lineno += 1

# A string containing ignored characters (spaces and tabs)
t_ignore  = ' \t'

# Error handling rule
def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)

# Build the lexer
lexer = lex.lex()

# Error rule for syntax errors
def p_error(p):
    print("Syntax error in input!")
    print(p)

def p_lines(p):
    'lines : lines line'
    p[0] = p[1]+[p[2]]
    
def p_lines2(p):
    'lines : line'
    p[0] = [p[1]]

def p_line(p):
    '''line : INSTR params '''
    # 'line : ID params'
    p[0] = [p[1],'',p[2]]

def p_line2(p):
    '''line : LBL INSTR params '''
    # 'line : LBL ID params'
    p[0] = [p[2],p[1],p[3]]
    
def p_line3(p):
    '''line : LBL INSTR'''
    # 'line : LBL ID'
    p[0] = [p[2],p[1]]
    
def p_line4(p):
    '''line : INSTR'''
    # 'line : ID'
    p[0] = [p[1]]
        
def p_params(p):
    '''params : params COMA param '''
    p[0] = p[1] + [p[3]]
    
#May be wrong: COMA could be removed in multiline param
# def p_params1(p):
#     '''params : params param '''  
#     p[0] = p[1] + [p[2]]
    
def p_params2(p):
    '''params : param '''
    p[0] = [p[1]]
    
def p_param(p):
    '''param : ID '=' data '''
    p[0] = (p[1], p[3])
    
def p_param2(p):
    '''param : STR '''
    p[0] =  ('STR', [p[1]])
    
def p_param3(p):
    '''param : ID '''
    p[0] =  ('ID', [p[1]])
    # print(p[1])
    
def p_param4(p):
    '''param : '(' values ')' '''
    p[0] =  (None, p[2])
    
# def p_param5(p):
#     '''param : '(' empty COMA value ')' '''
#     p[0] =  (None, [None, p[4]])
    
# def p_param6(p):
#     '''param : '(' value COMA ')' '''
#     p[0] =  (None, [None, p[4]])
    

def p_data(p):
    '''data : value '''
    p[0] = [p[1]]
    
def p_data2(p):
    '''data : '(' values ')' '''
    p[0] = p[2]
    
def p_data3(p):
    '''data : '(' '(' values ')' ')' '''
    p[0] = p[3]

# def p_values(p):
#     '''values : values value '''
#     p[0] = p[1] + [p[2]]
    
def p_values2(p):
    '''values : values COMA value '''
    p[0] = p[1] + [p[3]] 
    
# def p_values4(p):
#     '''values : values COMA '''
#     p[0] = p[1]
    
def p_values3(p):
    'values : value'
    p[0] = [p[1]]

    
def p_value(p):
    '''value : ID
             | NUMBER 
             | STR 
             | empty '''
    p[0] = p[1]

def p_empty(p):
    'empty :'
    p[0] = None


parser = yacc.yacc()