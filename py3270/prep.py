# -*- coding: utf-8 -*-
"""
Created on Tue Dec 15 14:07:59 2020

@author: oleksii.bezuhlyi
"""

import re
from functools import reduce

subts = {r"\%PR\%":"PR",
        r"\%I\%" :"info",
        r"\%D\%" : "date",
        r"\%T\%" : "time"}



XlineWcoma = re.compile(r',[\s\w]+X')
cmnt = re.compile(r'([A-Z]{0,8}\s+[A-Z]+\s+(?:\'.+\')*[A-Z0-9\,\=\(\)\-\%]*(?:\'.+\')*[A-Z0-9\,\=\(\)\-\%]*)(.*)') 

def substituteM(data, subs=subts):
    'replaces %PR% %I% %D% %T% and so'
    ret = data
    for sub in subs:
        ret = re.sub(sub, subs[sub], ret)
    return ret

def removeComment(line):
    # if len(line) > 70:
    #     pass
    withCmnt = cmnt.match(line)
    if withCmnt:
        return withCmnt.group(1)
    else:
        return line

def truncateX(line, it):
    nextLine = next(it)
    
    withComa = XlineWcoma.search(line)
    if withComa:
        line = line[:withComa.start()+1] 
    else:
        line = line[:71]

    if len(nextLine) > 71 and nextLine[71] == 'X':
        line += truncateX(nextLine[15:], it)
    else:
        line += nextLine[15:]
    return line

def preprocessor(data, subs = None):
    lines = data.splitlines()
    newlines=['']
    it = iter(lines)
    for line in it:
        if len(line) and line[0] == '*':
            continue
        if len(line) > 71 and line[71] == 'X':
            line = truncateX(line, it)
            newlines.append(line)
        else:
            newlines.append(removeComment(line))

    return substituteM(reduce(lambda x,y:x+y+'\n', newlines), subs)

