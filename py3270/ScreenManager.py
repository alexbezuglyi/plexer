# -*- coding: utf-8 -*-
"""
Created on Fri May  8 13:53:48 2020

@author: oleksii.bezuhlyi
"""



from prep import subts, preprocessor
import hlasmparser 
import struct
# import redis
# import binascii



instructions = ["DFLD", "DEV", "DIV", "DPAGE", "FMT", "FMTEND", "EJECT", "SEG", \
                "MFLD", "END",  "PRINT", "MSG", "MSGEND", "TITLE"]
    
 #TN3270 Code table to transalte buffer addresses
code_table=[0x40, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7,
            0xC8, 0xC9, 0x4A, 0x4B, 0x4C, 0x4D, 0x4E, 0x4F,
            0x50, 0xD1, 0xD2, 0xD3, 0xD4, 0xD5, 0xD6, 0xD7,
            0xD8, 0xD9, 0x5A, 0x5B, 0x5C, 0x5D, 0x5E, 0x5F,
            0x60, 0x61, 0xE2, 0xE3, 0xE4, 0xE5, 0xE6, 0xE7,
            0xE8, 0xE9, 0x6A, 0x6B, 0x6C, 0x6D, 0x6E, 0x6F,
            0xF0, 0xF1, 0xF2, 0xF3, 0xF4, 0xF5, 0xF6, 0xF7,
            0xF8, 0xF9, 0x7A, 0x7B, 0x7C, 0x7D, 0x7E, 0x7F]

# Attributes
HI     = 0b11001000
ALPHA  = 0b01000000
PROT   = 0b00100000
NUM    = 0b00010000
NODISP = 0b00001100
MOD    = 0b00000001


NO      = chr(0x60) 
QREPLY  = chr(0x61) 
ENTER   = chr(0x7d)
PF1     = chr(0xf1)
PF2     = chr(0xf2)
PF3     = chr(0xf3)
PF4     = chr(0xf4)
PF5     = chr(0xf5)
PF6     = chr(0xf6)
PF7     = chr(0xf7)
PF8     = chr(0xf8)
PF9     = chr(0xf9)
PF10    = chr(0x7a)
PF11    = chr(0x7b)
PF12    = chr(0x7c)
PF13    = chr(0xc1)
PF14    = chr(0xc2)
PF15    = chr(0xc3)
PF16    = chr(0xc4)
PF17    = chr(0xc5)
PF18    = chr(0xc6)
PF19    = chr(0xc7)
PF20    = chr(0xc8)
PF21    = chr(0xc9)
PF22    = chr(0x4a)
PF23    = chr(0x4b)
PF24    = chr(0x4c)
OICR    = chr(0xe6)
MSR_MHS = chr(0xe7)
SELECT  = chr(0x7e)
PA1     = chr(0x6c)
PA2     = chr(0x6e)
PA3     = chr(0x6b)
CLEAR   = chr(0x6d)
SYSREQ  = chr(0xf0)

PFKs = [ENTER,PF1,PF2,PF3,PF4,PF5,PF6,PF7,PF8,PF9,PF10,PF11,PF12,PF13,PF14,
           PF15,PF16,PF17,PF18,PF19,PF20,PF21,PF22,PF23,PF24,OICR]
    


    

class EmptyScreen():
    def __init__(self):
        self.DFLD = {}
        self.CURSOR = '@Á'
        
class TN3270screen:
    def __init__(self):
        self.parser = hlasmparser.parser
        hlasmparser.instructions = instructions
        self.MFLDout = []
        self.cur_dfld_type = 2
        self.DFLDs = [{},{},{self.POS(1,2) :[self.POSout(1,2), "", 1, MOD, " "]},
                      {self.POS(1,2) :[self.POSout(1,2), "", 1, MOD, " "]},{},{}]
        self.DFLD = {}
        self.PFK = {}
        self.MSGTYPE = ""
        self.MFLDin = []
        self.MFLDout = []
        self.TRAN = ''
        self.CURSOR = self.POS(1,2)
        self.TN3270Header = {
            "DATA-TYPE":0,
            "REQUEST-FLAG":0,
            "RESPONSE-FLAG":0,
            "SEQ-NUMBER":0
            }
        
    def parse(self, dat, subts=subts):
        preprocessed = preprocessor(dat, subts)
        # print(preprocessed)
        tree = self.parser.parse(preprocessed)
        return tree
        
    def compile_tree(self, tree):
        for line in tree:
            if line[0] == 'DEV':
                self.dev(line)
            elif line[0] == 'DPAGE':
                self.dpage(line)
            elif line[0] == 'DIV':
                self.div(line)
            elif line[0] == 'DFLD':
                self.dfld(line)
            elif line[0] == 'MFLD':
                self.mfld(line[1], line[2])
            elif line[0] == 'TITLE':
                pass
            elif line[0] == 'FMT':
                pass
            elif line[0] == 'FMTEND':
                pass
            elif line[0] == 'EJECT':
                pass
            elif line[0] == 'MSG':
                self.msg(line)
            elif line[0] == 'MSGEND':
                pass
            elif line[0] == 'SEG':
                pass
            elif line[0] == 'END':
                pass
            elif line[0] == 'PRINT':
                pass

        self.DFLD = self.DFLDs[self.cur_dfld_type]
        return ''
    
    def dev(self, line):
        'define cur_dfld_type, PFK'
# ['DEV', '',[('FEAT', ['IGNORE']), 
#             ('PFK', ['TRANS', 'PRTRANS1 F   ', 'PRTRANS1 2   ', '/FOR PRTRAN2.', 'PRTRANS1 21  ', 'PRTRANS1 24  ', None]), 
#             ('DSCA',[144]), 
#             ('SYSMSG', ['SYSMSG']), 
#             ('TYPE', ['3270-A03'])]]
        for l in line[2]:
            Parameter = l[0]
            values = l[1]
            if Parameter == "PFK":
                for i,val in enumerate(values):
                    self.PFK[PFKs[i]] = val
            elif Parameter == "TYPE":
                if values == ['3270', '2']:
                    self.cur_dfld_type = 2
                elif values == ['3270-A03']:
                    self.cur_dfld_type = 3
            else:
                pass
    
    def dpage(self, line):
# ['DPAGE', '', [('CURSOR', ['3', '28', 'CURSOR']), ('FILL', ['PT'])]]
# to 
# self.CURSOR = POS(3,28)
        'DPAGE CURSOR=((3,28,CURSOR)),FILL=PT'
        for l in line[2]:
            if l[0] == 'CURSOR':
                self.CURSOR = self.POS(int(l[1][0]),int(l[1][1]))
            elif l[0] == 'FILL':
                pass
    
    def div(self, line):
        'DIV   TYPE=INOUT'
        pass
    
    def dfld(self, line):
# ['DFLD', 'LBL', [('STR', ['STR TEXT']), ('POS', ['5', '31'])]]
# to
# {POS(1,2)  :[POSout(1,2),  "LBL", 0, PROT, "STR TEXT"]}
        pos = self.POS(1,1)
        tmp = [self.POSout(1,1), line[1], 0, 0, ""]
        for l in line[2]:
            if l[0] == 'STR':
                tmp[4] = str(l[1][0])
                tmp[3] |= 0b11110000
            elif l[0] == 'POS':
                x = int(l[1][0])
                y = int(l[1][1])
                pos = self.POS(x,y)
                tmp[0] = self.POSout(x,y)
            elif l[0] == 'LTH':
                tmp[2] = int(l[1][0])
            elif l[0] == 'ATTR':
                for attr in l[1]:
                    if attr == 'PROT':
                        tmp[3] |= PROT
                    elif attr == 'NUM':
                        tmp[3] |= NUM
                    elif attr == 'HI':
                        tmp[3] |= HI
                    elif attr == 'NODISP':
                        tmp[3] |= NODISP
                        tmp[3] &= 0b01111111
                    elif attr == 'MOD':
                        tmp[3] |= MOD
                    elif attr == 'ALPHA':
                        tmp[3] |= ALPHA
        self.DFLDs[self.cur_dfld_type][pos] = tmp
    
    def mfld(self, lbl, params):
# ['MFLD', '', [(None, ['TRANS', 'PRTRAN3 E    ']), ('LTH', ['13'])]]
# to
# self.MFLDin = (POS(24,2),  81, True)
        POS = ''
        LEN = 0
        JSTF = ''
        FILL = 0
        ATTR = False
        STR = ''
        for param in params:
            if param[0] == None:
                STR = param[1][1]
                label = param[1][0]
                POS = self.label2pos(label)
                if label == self.PFK[ENTER]:
                    POS = label
            elif param[0] == 'ID':
                POS = self.label2pos(param[1][0])
            elif param[0] == 'LTH':
                LEN = int(param[1][0])
            elif param[0] == 'FILL':
                FILL = param[1][0]
            elif param[0] == 'JUST':
                JSTF = param[1][0]
            elif param[0] == 'STR':
                STR = param[1][0]
            elif param[0] == 'ATTR':
                ATTR = (param[1][0] == 'YES')
            else:
                print("error: MFLD Unknown param")
        if self.MSGTYPE == 'INPUT':
            self.MFLDin.append((POS,LEN,JSTF,FILL,STR))
        elif self.MSGTYPE == 'OUTPUT':
            self.MFLDout.append((POS, LEN, ATTR))
    
    def label2pos(self, lbl):
        ret = ''
        DFLD = self.DFLDs[self.cur_dfld_type]
        for pos in DFLD:
            if lbl == DFLD[pos][1]:
                ret = pos
        return ret
    
    def msg(self,line):
#['MSG', 'PROIFM', [('TYPE', ['INPUT']), ('SOR', ['PROIFD']), ('NXT', ['PRTEST1'])]]
#to self.MSGTYPE ='INPUT'
        for l in line[2]:
            if l[0] == 'TYPE':
                self.MSGTYPE = l[1][0]
            elif l[0] == 'SOR':
                pass
            elif l[0] == 'NXT':
                pass
            elif l[0] == 'FILL':
                pass
        pass
    
    def POSout(self, r,c):
        address = (r-1)*80+c-2
        b1 = chr(code_table[(address >> 6) & 0x3F])
        b2 = chr(code_table[address & 0x3F])
        return b1 + b2

    def POS(self, r,c):
        address = (r-1)*80+c-1
        b1 = chr(code_table[(address >> 6) & 0x3F])
        b2 = chr(code_table[address & 0x3F])
        return b1 + b2
    
    def MFLDsend(self, r, AID):
        # frmt = "<hh"
        frmt = ">HH" #test EBCDIC
        data = [0, 0]
                    
        for field in self.MFLDin:
            MFLDaction = field[0] #can be POS or 'CURSOR' or PFK
            MFLDlength = field[1]
            # MFLDjust   = field[2]
            MFLDfill   = field[3]
            # if field[0] in self.PFK:
            #     if AID in self.PFK[field[0]]:
            #         data.append((self.PFK[field[0]][AID]).encode())
            #     else:
            #         data.append(field[4]) #default from MFLDin
            if MFLDaction == self.PFK[ENTER]:
                if AID == ENTER:
                    data.append(field[4]) #default from MFLDin
                elif AID in self.PFK:
                    data.append((self.PFK[AID]).encode("cp500")) #test EBCDIC
                else:
                    pass
                frmt += str(MFLDlength) + "s" #index 1 is Length
            elif MFLDaction == 'CURSOR':
                data.append(bytes(self.CURSOR))
                frmt += "4p"
            elif MFLDaction == '':
                for _ in range(MFLDlength):
                    data.append(MFLDfill)
                    frmt += "B"
            else:
                if MFLDaction in self.DFLD: #MFLDaction is POS
                    # print(self.DFLD[MFLDaction])
                    data.append(self.DFLD[MFLDaction][4].encode("cp500")) #test EBCDIC
                else:
                    print("bad format")
                frmt += str(field[1]) + "s"
        # print("frmt: " + frmt)
        data[0] = struct.calcsize(frmt)
        # print (tuple(data))
        buf = struct.pack(frmt, *tuple(data))
        return r.rpush("T"+self.TRAN, buf)
    
    def MFLDrecieve(self, r):
        frmt = ">HH" #test EBCDIC
        for field in self.MFLDout:
            frmt += str(field[1]) + "s"
        tran, buf = r.blpop("S"+self.TRAN, 0)
        #TODO: lterm attach, if lterm different, then rpush back if exists
        #TODO: message switch
        #if something mean a new TRAN:
            #tran = read_new_tran(buf)
            #tree = self.parse(f.read(tran)) 
            #self.clear()
            #self.compile_tree(tree)
        #else:
        if tran:
            tple = struct.unpack(frmt, buf[:struct.calcsize(frmt)]) 
            # print(tple[0])
            # print(struct.calcsize(frmt))
            assert(struct.calcsize(frmt) == tple[0])
            for i, MFLD in enumerate(self.MFLDout):
                if MFLD[0] != "" and MFLD[0] != 0:
                    field = tple[i+2] #i+2 is position minus LL ZZ 
                    MFLDpos = MFLD[0]
                    ATTR = MFLD[2]
                    # MFLDlen = MFLD[1]
                    val = field[2:] if ATTR else field #1: is second position without ATTR=YES
                    # print("len:" + str(MFLDlen) + "  val:" + val)
                    self.DFLD[MFLDpos][4] = val.decode("cp500")  #test EBCDIC
    
    
