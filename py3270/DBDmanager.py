# -*- coding: utf-8 -*-
"""
Created on Fri May  8 13:53:48 2020

@author: oleksii.bezuhlyi
"""

from prep import subts, preprocessor
import hlasmparser 

instructions = ["TITLE", "DBD", "DATASET", "SEGM", "FIELD", "DBDGEN", "FINISH", "END"]

class DBDmanager:
    def __init__(self):
        self.parser = hlasmparser.parser
        hlasmparser.instructions = instructions

    def parse(self, dat, subts=subts):
        preprocessed = preprocessor(dat, subts)
        tree = self.parser.parse(preprocessed)
        return tree

    def compile_tree(self, tree):
        for line in tree:
            if line[0] == 'DBD':
                pass

