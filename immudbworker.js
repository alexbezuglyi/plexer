const ImmudbClient = require('immudb-node')
const { parentPort } = require('worker_threads');

const config = {
    address: '127.0.0.1:3322',
    rootPath: '.',
}

ImmudbClient(config, (err, cl) => {
    if (err) {
        return console.log(err)
    }
  
    cl.login({ username: 'immudb', password: 'immudb' }, (err, res) => {
        if (err) {
            return console.log(err)
        }

        // let res = await cl.set({ key: 'key1', value: 'value1' });
        // console.log(res.index);
    
        cl.get({ key: 'key1' }, (err, res) =>{
            console.log(res.key, res.value, res.index);
        });
    
    });
});


parentPort.on('message', params => {
    const command = params[0];
    const transaction = params[1];
    let sab = params[2];
    const blocker = new Int32Array(sab);
    let buf = Buffer.from(sab);
    switch(command) {
        case "blpop":
            redis.blpopBuffer(transaction, 0, (err, value) => {
                if (value) { 
                    let resultbuf = value[1];
                    resultbuf.copy(buf, 0, 0, resultbuf.length);
                }
                console.log(err);
                Atomics.notify(blocker, 0, 1);
            });
            break;
        case "get":
            let key = params[1];
            redis.getBuffer(key, (err, value) => {
                if (value) { 
                    value.copy(buf, 0, 0, value.length);
                }
                console.log(err);
                Atomics.notify(blocker, 0, 1);
            });
            break;
        case "rpush":
            let rpushbuf = Buffer.alloc(params[3]);
            buf.copy(rpushbuf, 0,0, params[3]);
            redis.rpushBuffer(transaction, rpushbuf, (err, value) => {
                console.log(value);
                console.log(err);
            });
            break;
        case "set":
            let setbuf = Buffer.alloc(params[3]);
            buf.copy(setbuf, 0,0, params[3]);
            redis.setBuffer(transaction, setbuf, (err, value) => {
                console.log(value);
                console.log(err);
            });
            break;
    }
});





