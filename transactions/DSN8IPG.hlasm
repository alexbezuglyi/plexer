**********************************************************************  
*                                                                    *  
*   MODULE NAME = DSN8IPG                                            *  
*                                                                    *  
*   DESCRIPTIVE NAME = IMS MFS FORMAT FOR SAMPLE PROGRAM, GENERAL    *  
*                                                                    *  
*     COPYRIGHT = 5740-XYR (C) COPYRIGHT IBM CORP 1982, 1985         *  
*     REFER TO COPYRIGHT INSTRUCTIONS FORM NUMBER G120-2083          *  
*                                                                    *  
*     STATUS = RELEASE 2, LEVEL 0                                    *  
*                                                                    *  
*   FUNCTION =                                                       *  
*      PROVIDES THE IMS MFS MAP FOR THE GENERAL SCREEN OF            *  
*      THE SAMPLE APPLICATION.                                       *  
*                                                                    *  
*   NOTES =                                                          *  
*      DEPENDENCIES = NONE                                           *  
*                                                                    *  
*      RESTRICTIONS = NONE                                           *  
*                                                                    *  
*      REGISTER CONVENTIONS = NONE, NO EXECUTABLE CODE               *  
*                                                                    *  
*   MODULE TYPE = PROCEDURE                                          *  
*                                                                    *  
*      PROCESSOR =  ASSEMBLER XF OR H                                *  
*                                                                    *  
*      MODULE SIZE = SEE ASSEMBLER LISTING                           *  
*                                                                    *  
*      ATTRIBUTES = REUSABLE                                         *  
*                                                                    *  
*   ENTRY POINT = DSN8IPG                                            *  
*                                                                    *  
*      PURPOSE = SEE FUNCTION                                        *  
*                                                                    *  
*      LINKAGE = NONE                                                *  
*                                                                    *  
*      INPUT = NONE                                                  *  
*                                                                    *  
*      OUTPUT = NONE                                                 *  
*                                                                    *  
*                                                                    *  
*   EXIT-NORMAL = NONE                                               *  
*                                                                    *  
*   EXIT-ERROR = NONE                                                *  
*                                                                    *  
*      RETURN CODE = NONE                                            *  
*         REASON CODE = NONE                                         *  
*                                                                    *  
*      ABEND CODES = NONE                                            *  
*                                                                    *  
*      ERROR-MESSAGES = NONE                                         *  
*                                                                    *  
*   EXTERNAL REFERENCES =                                            *  
*      ROUTINES/SERVICES = NONE                                      *  
*                                                                    *  
*      DATA-AREAS = NONE                                             *  
*                                                                    *  
*      CONTROL-BLOCKS = NONE                                         *  
*                                                                    *  
*      MACROS         =                                              *  
*         DEV                 - IMS MFS DEVICE DESCRIPTION           *  
*         DFLD                - IMS MFS FIELD  DESCRIPTION           *  
*         DIV                 - IMS MFS DIVISION DESCRIPTION         *  
*         DO                  - IMS MFS GROUPING                     *  
*         DPAGE               - IMS MFS PAGE   DESCRIPTION           *  
*         ENDDO               - IMS MFS END OF GROUPING              *  
*         FMT                 - IMS MFS FORMAT BEGINNING             *  
*         FMTEND              - IMS MFS FORMAT ENDING                *  
*         MFLD                - IMS MFS MESSAGE FIELD                *  
*         MSG                 - IMS MFS MESSAGE DESCRIPTION          *  
*         MSGEND              - IMS MFS MESSAGE ENDING               *  
*         SEG                 - IMS MFS MESSAGE SEGMENT              *  
*                                                                    *  
*   TABLES = NONE                                                    *  
*                                                                    *  
*   CHANGE-ACTIVITY =                                                *  
*      NONE                                                          *  
*                                                                    *  
*   PSEUDOCODE      =                                                *  
*                                                                    *  
*      THIS MODULE CONTAINS NO EXECUTABLE CODE.                      *  
*                                                                    *  
**********************************************************************  
         EJECT                                                          
*                                                                       
         PRINT NOGEN                                                    
DSN8GF   FMT                                                            
         DEV   TYPE=(3270,2),FEAT=IGNORE,SYSMSG=D0011,PFK=(D0014,1='01'X
               ,2='02',3='03',8='08',10='10',13='01',                  X
               14='02',15='03',20='08',22='10')                         
         DIV   TYPE=INOUT                                               
         DPAGE CURSOR=((03,20))                                         
         DFLD  'MAJOR SYSTEM ...: O          ORGANIZATION ',POS=(02,02) 
         DFLD  'ACTION .........:',POS=(03,02)                          
         DFLD  'OBJECT .........:',POS=(04,02)                          
         DFLD  'SEARCH CRITERIA :',POS=(05,02)                          
         DFLD  'DATA ...........:',POS=(06,02)                          
D0001    DFLD  POS=(01,15),LTH=50,ATTR=PROT            TITLE            
D0004    DFLD  POS=(03,20),LTH=01,ATTR=(HI,MOD)        ACTION           
D0005    DFLD  POS=(03,31),LTH=50,ATTR=PROT            ACTION DESCRIPT. 
D0006    DFLD  POS=(04,20),LTH=02,ATTR=(HI,MOD)        OBJECT           
D0007    DFLD  POS=(04,31),LTH=50,ATTR=PROT            OBJECT DESCRIPT. 
D0008    DFLD  POS=(05,20),LTH=02,ATTR=(HI,MOD)        SEARCH CRITERIA  
D0009    DFLD  POS=(05,31),LTH=50,ATTR=PROT            SEA CRIT DESCRI. 
D0010    DFLD  POS=(06,20),LTH=60,ATTR=(HI,MOD)        DATA             
D0011    DFLD  POS=(07,02),LTH=79,ATTR=(HI,PROT)       ERROR/HELP MESS. 
         DO    15                                                       
D0012    DFLD  POS=(09,02),LTH=79,ATTR=PROT            DISPLAY          
         ENDDO                                                          
D0013    DFLD  POS=(24,02),LTH=79,ATTR=PROT            PFK TEXT         
         FMTEND                                                         
*********************************************************************** 
*      PL/I SAMPLE APPLICATION OUTPUT DESCRIPTION:  DSN8IPGO          * 
*********************************************************************** 
*                                                                       
DSN8IPGO MSG   TYPE=OUTPUT,SOR=(DSN8GF,IGNORE),NXT=DSN8IPGI,FILL=PT     
         SEG                                                            
         MFLD  LTH=01                                  MAJSYS='O'       
         MFLD  D0004,LTH=01                            ACTION           
         MFLD  D0006,LTH=02                            OBJECT           
         MFLD  D0008,LTH=02                            SEARCH CRITERIA  
         MFLD  D0010,LTH=60                            DATA             
         MFLD  D0001,LTH=50                            TITLE            
         MFLD  D0005,LTH=50                            ACTION DESCRIPT. 
         MFLD  D0007,LTH=50                            OBJECT DESCRIPT. 
         MFLD  D0009,LTH=50                            SEA CRIT DESCRI. 
         MFLD  D0011,LTH=79                            ERROR/HELP MESS. 
         MFLD  D0013,LTH=79                            PFK TEXT         
         DO    15                                                       
         MFLD  D0012,LTH=79                            DISPLAY          
         ENDDO                                                          
         MSGEND                                                         
*********************************************************************** 
*       PLI/SAMPLE APPLICATION INPUT DESCRIPTION:  DSN8IPGI           * 
*********************************************************************** 
*                                                                       
DSN8IPGI MSG   TYPE=INPUT,SOR=DSN8GF                                    
         SEG                                                            
         MFLD  'DSN8PS '                               TRANSACT. CODE   
         MFLD  'O'                                     MAJSYS='O'       
         MFLD  D0004,LTH=01                            ACTION           
         MFLD  D0006,LTH=02                            OBJECT           
         MFLD  D0008,LTH=02                            SEARCH CRITERIA  
         MFLD  D0014,LTH=02                            PFK              
         MFLD  D0010,LTH=60                            DATA             
         MSGEND                                                         
                                                   