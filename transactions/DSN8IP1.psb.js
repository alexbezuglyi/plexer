const { Plexer, pl2js: pl, ims: ims } = require('plexer');
const fs = require("fs");

ims.SetWorker("./redisworker.js");
const plex = new Plexer();

const THREE = 3, FOUR = 4, FIVE = 5;

let PCB01buf = new pl.CHAR(255);

let iopcb = pl.objInit({
    lterm_name: new pl.CHAR(8),
    reserved: new pl.CHAR(2),
    status: new pl.CHAR(2),
    date_time: new pl.CHAR(8),
    msg_seq: new pl.FIXED_BIN(31),
    mod_name: new pl.CHAR(8),
}).BASED(PCB01buf);

iopcb.reserved.v = "io";
iopcb.status.v = "B1";
iopcb.mod_name.v = "DSN8IP1";

try {
    var src = fs.readFileSync("./transactions/DSN8IP1.pli", "utf8");
    let preped = plex.preproc(src);
    // console.log(preped);
    const translated = plex.parse(preped).join('\n');
    if (translated.length) 
        fs.writeFileSync('lastsrc.js', translated, [encoding = "utf8", flag = 'w']);
    eval(translated);
    console.log("code evaluated, starting DSN8IP1...");
    DSN8IP1(PCB01buf);
} catch (error) {
    console.error(error);
}



