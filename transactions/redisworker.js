const Redis = require("ioredis");
const redis = new Redis('redis'); 
// const redis = new Redis(); 
const { parentPort } = require('worker_threads');

parentPort.on('message', params => {
    const command = params[0];
    const transaction = params[1];
    let sab = params[2];
    const int32 = new Int32Array(sab);
    let buf = Buffer.from(sab);
    switch(command) {
        case "blpop":
            redis.blpopBuffer(transaction, 0, (err, value) => {
                if (err) console.error(err);
                else if (value) { 
                    let resultbuf = value[1];
                    resultbuf.copy(buf, 0, 0, resultbuf.length);
                }
                Atomics.notify(int32, 0, 1);
            });
            break;
        case "get":
            let key = params[1];
            redis.getBuffer(key, (err, value) => {
                if (err) console.error(err);
                else if (value) { 
                    value.copy(buf, 0, 0, value.length);
                }
                Atomics.notify(int32, 0, 1);
            });
            break;
        case "rpush":
            let rpushbuf = Buffer.alloc(params[3]);
            buf.copy(rpushbuf, 0,0, params[3]);
            redis.rpushBuffer(transaction, rpushbuf, (err, value) => {
                if (err) console.error(err);
            });
            break;
        case "set":
            let setbuf = Buffer.alloc(params[3]);
            buf.copy(setbuf, 0,0, params[3]);
            redis.setBuffer(transaction, setbuf, (err, value) => {
                if (err) console.error(err);
            });
            break;
    }
});




