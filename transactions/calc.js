
var jison = require('jison');
var bnf = `
/* description: Parses and executes mathematical expressions. */

/* lexical grammar */
%lex
%%

\s+                   /* skip whitespace */
[0-9]+("."[0-9]+)?\b  return 'NUMBER'
"*"                   return '*'
"/"                   return '/'
"-"                   return '-'
"+"                   return '+'
"^"                   return '^'
"!"                   return '!'
"%"                   return '%'
"("                   return '('
")"                   return ')'
"PI"                  return 'PI'
"E"                   return 'E'
<<EOF>>               return 'EOL'
.                     return 'INVALID'

/lex

%start exps

%% /* language grammar */
exps    : exp EOL { return $1; }
        | exps exp EOL { $$ = $2; }
        ;

exp:    factor  {$$ = $1;}
        | exp '+' factor { $$ = $1 + $3; }
        | exp '-' factor { $$ = $1 - $3; }
        ;

factor: term  { $$ = $1;}
        | factor '*' term { $$ = $1 * $3; }
        | factor '/' term { $$ = $1 / $3; }
        ;

term:   NUMBER {$$ = Number(yytext);}
        ;

`;

var parser = new jison.Parser(bnf);

const pl = require("plexer").pl2js;
const ims = require("plexer").ims;
ims.SetWorker("./redisworker.js");

var IOPCB = pl.objInit({
    LTERM_NAME      : new pl.CHAR(8),
    RESERVED        : new pl.CHAR(2).INIT("io"),
    STATUS          : new pl.CHAR(2),
    DATE_TIME       : new pl.CHAR(8),
    MSG_SEQ         : new pl.FIXED_BIN(31),
    MOD_NAME        : new pl.CHAR(8).INIT("CALC"),
});

var SCREEN_DATA = pl.objInit({ 
    LL:                      new pl.FIXED_BIN(15),
    ZZ:                      new pl.FIXED_BIN(15),
    TRANSACTION:             new pl.CHAR(8),
    PF_KEY:                  new pl.CHAR(5),
    CURSOR:                  new pl.CHAR(4),
    FORMULA:                 new pl.CHAR(36),
    RESULT:                  new pl.CHAR(28),
    SYSMSG:                  new pl.CHAR(79),
});

let PCB01buf = new pl.CHAR(255).BASED(IOPCB);

var INOUT_MSG_AREA = new pl.CHAR(164).INIT('');
var INOUT_MSG = SCREEN_DATA.BASED(INOUT_MSG_AREA);

IOPCB.MOD_NAME.v = "CALC";
IOPCB.RESERVED.v = "io";

ims.dli('GU  ', PCB01buf, INOUT_MSG_AREA);
while(IOPCB.STATUS == '  ') {
    try {
        INOUT_MSG.RESULT.v = parser.parse(String(INOUT_MSG.FORMULA));
        INOUT_MSG.SYSMSG.v = "Your value has been calculated";
    } catch (e) {
        INOUT_MSG.SYSMSG.v = String(e);
    } 
    
    ims.dli("ISRT", PCB01buf, SCREEN_DATA);
    ims.dli("GU  ", PCB01buf, INOUT_MSG);
}


