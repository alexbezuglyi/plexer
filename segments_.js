// let ssa1 = 'OIUTILSG'+'('+'RECKEY  '+'= '+'ACC_PARMS.UTILITY_KEY'+') ';
// let VGOI03DD1 = {
//     DBD: { NAME: "VGOI03DD", VERSION: 'info/BCC,1,64,BCC/' },
//     DATASET: { DD1:"VGOI03DD", DEVICE:3380, SIZE:4096 },
//     SEGM: { NAME: "OIUTILSG", PARENT:0, BYTES=[4071,18] },
//     FIELDs: {
//         RECKEY: { NAME:["", SEQ, U], BYTES: 16, START: 3 }
//     }
// };
// let OIUTILSG = new CHAR(4071);
// let VGOI03DD = BASED(OIUTILSG, {
//     RECKEY: new CHAR(16, offset=3),
//     key: "RECKEY"
// });

const pl = require("./pl2.js");

let segs = {}
let dbds = {}

dbds.VGOI03DD = { //DATASET DD1=VGOI03DD,DEVICE=3380,SIZE=4096
    // size:4096,    //no impact
    // device:3380,  //no impact
    OIUTILSG: new pl.CHAR(/*4071*/ 512) // SEGM  NAME=OIUTILSG,PARENT=0,BYTES=(4071,18)
}

segs.OIUTILSG = pl.objInit({
    RECKEY: new pl.CHAR(16, offset=3),         //FIELD NAME=(RECKEY,SEQ,U),BYTES=16,START=3            
}).BASED(dbds.VGOI03DD.OIUTILSG);
segs.OIUTILSG.key = new pl.CHAR(8).INIT("RECKEY");

dbds.VGOI04DD = {
    OIRQSTSG: new pl.CHAR(8000),
    OIMSGSG : new pl.CHAR(4070)
}
// VGOI04DD.OIMSGSG.root = VGOI04DD.OIRQSTSG;

segs.OIRQSTSG = pl.objInit({
    // key: "REQUEST$",
    REQUEST$: new pl.CHAR(8, offset=13),
    STATUS  : new pl.CHAR(1, offset=21),
    PRIORITY: new pl.CHAR(1, offset=22),
    INITIATE: new pl.CHAR(8, offset=23),
    // parent:0
}).BASED(dbds.VGOI04DD.OIRQSTSG);

segs.OIMSGSG = pl.objInit({
    // key: "MSGKEY",
    MSGKEY: new pl.CHAR(4, offset=3),
    // parent: segs.OIRQSTSG
}).BASED(dbds.VGOI04DD.OIMSGSG);

module.exports = {segs, dbds}