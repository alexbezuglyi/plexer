// # -*- coding: utf-8 -*-
// """
// Created on Tue Dec 15 14:07:59 2020

// @author: oleksii.bezuhlyi
// """

// import re
// from functools import reduce

subts = {"\%PR\%":"PR",
        "\%I\%" :"info",
        "\%D\%" : "date",
        "\%T\%" : "time"};

const XlineWcoma = /,[\s\w]*X/;
// const cmnt = /([A-Z0-9%]{0,10}\s+[A-Z]+\s+(?:'.+')*[A-Z0-9,=\(\)\-%]*(?:'.+')*[A-Z0-9,=\(\)\-%]*)(.*)/;
const cmnt = /([A-Z0-9%]{0,10}\s+[A-Z]+\s+(\'[^\']+\')*[A-Z0-9,=\(\)\-%]*(\'[^\']+\')*[A-Z0-9,=\(\)\-%]*)(.*)/;

function substituteM(data, subs=subts) {
    let ret = data;
    for (const sub of Object.keys(subs)) {
        const replacer = new RegExp(sub, 'gm')
        ret = ret.replace(replacer, subs[sub]);
    }
    return ret;
}

function removeComment(line) {
    // # if len(line) > 70:
    // #     pass
    let withCmnt = cmnt.exec(line);
    if (withCmnt)
        return withCmnt[1];
    else
        return line;
}

function truncateX(line, it) {
    let nextLine = it.next().value;
    let XlineIdx = line.search(XlineWcoma);
    
    if (XlineIdx > 0)
        line = line.slice(0, XlineIdx + 1) ;
    else
        line = line.slice(0, 71);

    if (nextLine.length > 71 && nextLine[71] == 'X')
        line += truncateX(nextLine.slice(15), it);
    else
        line += nextLine.slice(15);
    return line;
}

function preprocessor(data , subs = subts) {
    let lines = data.split('\n');
    let newlines = [''];
    let it = lines[Symbol.iterator]();
    for (let line of it) {
        if (line.length && line[0] == '*')
            continue;
        if (line.length > 71 && line[71] == 'X') {
            line = truncateX(line, it);
            newlines.push(line);
        } else {
            newlines.push(removeComment(line));
        }
    }

    return substituteM(newlines.join('\n'), subs);
}

module.exports = {preprocessor};