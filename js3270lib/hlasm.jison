%lex
%%

[^\S\n]+                      /* ignore whitespace other than newlines before label */
\n+[A-Z][A-Z0-9]{0,7}         return 'LBL'
\s+                           /* skip whitespace */
'TITLE'                       return 'INSTR'
"DFLD"                        return 'INSTR'
"DEV"                         return 'INSTR'
"DIV"                         return 'INSTR'
"DO"                          return 'DO'
"DPAGE"                       return 'INSTR'
"FMT"                         return 'INSTR'
"FMTEND"                      return 'INSTR' 
"EJECT"                       return 'INSTR' 
"SEG"                         return 'INSTR' 
"MFLD"                        return 'INSTR' 
"END"                         return 'INSTR'    
"ENDDO"                       return 'ENDDO'  
"PRINT"                       return 'INSTR' 
"MSG"                         return 'INSTR' 
"MSGEND"                      return 'INSTR'

"3270-A03"                    return 'ID'
[0-9]+                        return 'NUMBER'
X\'[\dA-F]+\'                 return 'HEX'
C\'.+\'                       return 'CHAR'
[A-Z0-9][A-Z0-9\-\/\.]{0,7}   return 'ID'
\'([^\']+)\'                  return 'STR'

"="                           return '='
","                           return ','
'('                           return '('
")"                           return ')'
"'"                           return "'" //TODO: remove

<<EOF>>                       return 'EOF'

/lex

%start format

%%

value 
    : NUMBER  {$$ = $1}
    | NUMBER '=' STR {$$ = [Number($1), $3.slice(1,-1)]}
    | ID      {$$ = $1}
    | STR     {$$ = $1.slice(1,-1);}
    | HEX     {$$ = parseInt($1.slice(2,-1), 16)}
    | CHAR    {$$ = $1.slice(2,-1);}
    ;

values 
    : values ',' value {$$ = $1.concat([$3])}
    | values ','
    | value            {$$ = [$1]}
    ;

data 
    : value                  {$$ = [$1]}
    | '(' values ')'         {$$ = $2}
    | '(' '(' values ')' ')' {$$ = $3}
    ;

param 
    : ID '=' data       {$$ = [$1, $3]}
    // | value             {$$ = ['value', [$1]]} //todo: 'value' to lexeme
    | STR               {$$ = ['STR', [$1.slice(1,-1)]]}
    | ID                {$$ = ['ID', [$1]]}
    | NUMBER            {$$ = ['NUMBER', [$1]]}
    | '(' values ')'    {$$ = [undefined, $2]}         
    | '(' ',' value ')' {$$ = [undefined, [undefined,$3]]}     
    ;

params 
    : params ',' param {$$ = $1.concat([$3])}
    | param            {$$ = [$1]}
    ;

label
    : /* empty */ {$$ = ''}
    | LBL         {$$ = $1.trim()}
    ;

line
    : label INSTR         {$$ = [$2, $1]}
    | label INSTR params  {$$ = [$2, $1, $3]}
    // : INSTR             {$$ = [$1, '', '']}
    // | INSTR params      {$$ = [$1, '', $2]}
    // | LBL INSTR         {$$ = [$2, $1.trim()]}
    // | LBL INSTR params  {$$ = [$2, $1.trim(), $3]}
    | DO values lines ENDDO {$$ = [$1, '', [$2, $3]]}
    ;

lines 
    // : lines line EOF {{$$ = $1.concat([$2]); return $$;}}
    : lines line {$$ = $1.concat([$2]);}
    | line  {$$ = [$1]}
	;

format
    : lines EOF {$$ = $1; return $$;}
    ;

%%