//  Telnet protocol commands
const cmd = {
    SE   : 240, //End of subnegotiation parameters
    SB   : 250, //Sub-option to follow
    WILL : 251, //Will; request or confirm option begin
    WONT : 252, //Wont; deny option request
    DO   : 253, //Do = Request or confirm remote option
    DONT : 254, //Don't = Demand or confirm option halt
    IAC  : 0xff, //Interpret as Command
    SEND : 0x1,  //Sub-process negotiation SEND command
    IS   : 0x0   //Sub-process negotiation IS command
};

// TN3270 Telnet Commands
const TN = {
    ASSOCIATE  : 0,
    CONNECT    : 1,
    DEVICETYPE : 2,
    FUNCTIONS  : 3,
    IS         : 4,
    REASON     : 5,
    REJECT     : 6,
    REQUEST    : 7,
    RESPONSES  : 2,
    SEND       : 8,
    EOR        : 0xef //End of Record
}

// TN3270 Stream Orders
const streamOrder = {
    SF  : 0x1D,
    SFE : 0x29,
    SBA : 0x11,
    SA  : 0x28,
    MF  : 0x2C,
    IC  : 0x13,
    PT  : 0x5,
    RA  : 0x3C,
    EUA : 0x12,
    GE  : 0x8
}

const subcommand = {
    BINARY  : 0,
    EOR     : 25,
    TTYPE   : 24,
    TN3270E : 40
}

const DATA_TYPE = {
    '3270-DATA':    0x00,   //The data portion of the message contains only the 3270 data stream.
    'SCS-DATA':     0x01,   //The data portion of the message contains SNA Character Stream data.
    'RESPONSE'  :   0x02,   //The data portion of the message constitutes device-status information and the RESPONSE-FLAG field indicates whether this is a positive or negative response (see below).
    'BIND-IMAGE' :  0x03,   //The data portion of the message is the SNA bind image from the session established between the server and the host application.
    'UNBIND'   :    0x04,   //The data portion of the message is an Unbind reason code.
    'NVT-DATA' :    0x05,   //The data portion of the message is to be interpreted as NVT data.
    'REQUEST' :     0x06,   //There is no data portion present in the message.  Only the REQUEST-FLAG field has any meaning.
    'SSCP-LU-DATA': 0x07    //The data portion of the message is data from the SSCP-LU session.
}

// TN3270 Attention Identification (AIDS)
const AIDs = {
    NO      : 0x60, //no aid
    QREPLY  : 0x61, //reply
    ENTER   : 0x7d, //enter
    PF1     : 0xf1,
    PF2     : 0xf2,
    PF3     : 0xf3,
    PF4     : 0xf4,
    PF5     : 0xf5,
    PF6     : 0xf6,
    PF7     : 0xf7,
    PF8     : 0xf8,
    PF9     : 0xf9,
    PF10    : 0x7a,
    PF11    : 0x7b,
    PF12    : 0x7c,
    PF13    : 0xc1,
    PF14    : 0xc2,
    PF15    : 0xc3,
    PF16    : 0xc4,
    PF17    : 0xc5,
    PF18    : 0xc6,
    PF19    : 0xc7,
    PF20    : 0xc8,
    PF21    : 0xc9,
    PF22    : 0x4a,
    PF23    : 0x4b,
    PF24    : 0x4c,
    OICR    : 0xe6,
    MSR_MHS : 0xe7,
    SELECT  : 0x7e,
    PA1     : 0x6c,
    PA2     : 0x6e,
    PA3     : 0x6b,
    CLEAR   : 0x6d,
    SYSREQ  : 0xf0
}

// TN3270 Code table to transalte buffer addresses
const code_table = [0x40, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7,
    0xC8, 0xC9, 0x4A, 0x4B, 0x4C, 0x4D, 0x4E, 0x4F,
    0x50, 0xD1, 0xD2, 0xD3, 0xD4, 0xD5, 0xD6, 0xD7,
    0xD8, 0xD9, 0x5A, 0x5B, 0x5C, 0x5D, 0x5E, 0x5F,
    0x60, 0x61, 0xE2, 0xE3, 0xE4, 0xE5, 0xE6, 0xE7,
    0xE8, 0xE9, 0x6A, 0x6B, 0x6C, 0x6D, 0x6E, 0x6F,
    0xF0, 0xF1, 0xF2, 0xF3, 0xF4, 0xF5, 0xF6, 0xF7,
    0xF8, 0xF9, 0x7A, 0x7B, 0x7C, 0x7D, 0x7E, 0x7F]

// Attributes
const attrs = {
    HI     : 0b11001000,
    ALPHA  : 0b01000000,
    PROT   : 0b00100000,
    NUM    : 0b00010000,
    NODISP : 0b00001100,
    MOD    : 0b00000001
}


const PFKs = [AIDs.ENTER, AIDs.PF1, AIDs.PF2, AIDs.PF3, AIDs.PF4, AIDs.PF5, AIDs.PF6, AIDs.PF7, AIDs.PF8, AIDs.PF9, AIDs.PF10, AIDs.PF11, AIDs.PF12, AIDs.PF13, AIDs.PF14,
    AIDs.PF15, AIDs.PF16, AIDs.PF17, AIDs.PF18, AIDs.PF19, AIDs.PF20, AIDs.PF21, AIDs.PF22, AIDs.PF23, AIDs.PF24, AIDs.OICR]

module.exports = {cmd, TN, streamOrder, subcommand, DATA_TYPE, AIDs, code_table, attrs, PFKs}