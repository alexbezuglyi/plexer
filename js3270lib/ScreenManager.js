// // -*- coding: utf-8 -*-
// """
// Created on Fri May  8 13:53:48 2020

// @author: oleksii.bezuhlyi
// """

/**
 *  the module is middleman between DFLD and MFLD
 */
const fs = require("fs");
const prep = require("./hlasmprep");
const jison = require('jison');
const pl = require("plexer").pl2js;
const cnst = require("./constants");
const bnf = fs.readFileSync("./js3270lib/hlasm.jison", "utf8");
const parser = new jison.Parser(bnf);
const { sysgen } = require('../sysgen');
        
class TN3270screen {
    constructor() {
        this.lterm = "";
        this.clear();
    }
        
    compile_tree(tree) {
        for (const line of tree) {
            switch(line[0]) {
                case 'DEV': this.dev(line); 
                break;
                case 'DPAGE': this.dpage(line); 
                break;
                case 'DIV': this.div(line); 
                break;
                case 'DFLD': this.dfld(line, 0); 
                break;
                case 'DO': this.do(line[2]);
                break;
                case 'MFLD': this.mfld(line[1], line[2], 0); 
                break;
                case 'TITLE':
                break;
                case 'FMT':
                break;
                case 'FMTEND':
                break;
                case 'EJECT':
                break;
                case 'MSG': this.msg(line);
                break;
                case 'MSGEND': this.msgend();
                break;
                case 'SEG':
                break;
                case 'END':
                break;
                case 'PRINT':
                break;
            }
        }
        this.DFLD = this.DFLDs[this.cur_dfld_type];
        this.MFLDin.root.lterm.v = this.lterm;
        return this;
    }
    
    dev(line) {
// ['DEV', '',[('FEAT', ['IGNORE']), 
//             ('PFK', ['ENTER', 'PRTRANS1 F   ', 'PRTRANS1 2   ', '/FOR PRTRAN2.', 'PRTRANS1 21  ', 'PRTRANS1 24  ', None]), 
//             ('DSCA',[144]), 
//             ('SYSMSG', ['SYSMSG']), 
//             ('TYPE', ['3270-A03'])]]
        const params = line[2];
        for (const l of params) {
            let param = l[0];
            let values = l[1];
            if (param == "PFK") {
                values.forEach((element, index) => {
                    if (Array.isArray(element)) { 
                        this.PFK[cnst.PFKs[element[0]]] = element[1]; //PFK=(dfldname,integer='literal')
                    } else {                      
                        this.PFK[cnst.PFKs[index]] = element;         //PFK=(dfldname,'literal')
                    }
                });
                //POS == 0 is special case, where dfldname does not have label
                //PFK=(dfldname, where dfldname is values[0]; see https://www.ibm.com/docs/en/ims/15.1.0?topic=statements-dev-statement
                this.PFKlabel = values[0];
            } else if (param == "TYPE") {
                if (values == ['3270', '2'].join()) //values will 'join' automatically 
                    this.cur_dfld_type = 2;
                else if (values == ['3270-A03'].join())
                    this.cur_dfld_type = 3;
                this.DFLD = this.DFLDs[this.cur_dfld_type];
            }
        }
    }
    
    dpage(line) {
// 'DPAGE CURSOR=((3,28,CURSOR)),FILL=PT'
// to
// ['DPAGE', '', [('CURSOR', ['3', '28', 'CURSOR']), ('FILL', ['PT'])]]
// to 
// this.CURSOR = POS(3,28)
        for (const l of line[2]) {
            if (l[0] == 'CURSOR')
                this.CURSOR = this.POS(Number(l[1][0]), Number(l[1][1]));
            // else if (l[0] == 'FILL')
            //     pass
        }
    }
    
    div(line) {
        'DIV   TYPE=INOUT'
        // pass
    }
    
    dfld(line, line_inc) {
// LBL   DFLD  'STR TEXT:',POS=(1,2),ATTR=(PROT),LTH=8
// to
// ['DFLD', 'LBL', [('STR', ['STR TEXT']), ('POS', ['1', '2'])]]
// to
// this.DFLD = {POS(1,2): [POSout(1,2), "LBL", 8, PROT, "STR TEXT"]}
        let pos = this.POS(1,1);
        const label = line[1];
        let tmp = [this.POSout(1,1), label, 0, 0, ""];
        if (!this.label_pos_cache.hasOwnProperty(label))
            this.label_pos_cache[label] = [];
        for (const l of line[2]) {
            switch(l[0]) {
                case 'STR':
                    const str = l[1][0];
                    tmp[4] = new pl.CHAR(str.length).INIT(String(str));
                    tmp[3] |= 0b11110000;
                    break;
                case 'POS':
                    const x = Number(l[1][0]) + line_inc; //TODO: to Number at Parser
                    const y = Number(l[1][1]);
                    pos = this.POS(x,y);
                    tmp[0] = this.POSout(x,y);
                    this.label_pos_cache[label].push(pos);
                    break;
                case 'LTH':
                    tmp[2] = Number(l[1][0]);
                    tmp[4] = new pl.CHAR(tmp[2]);
                    break;
                case 'ATTR':
                    for (const attr of l[1]) {
                        switch(attr) {
                            case 'PROT': 
                                tmp[3] |= cnst.attrs.PROT; break;
                            case 'NUM':
                                tmp[3] |= cnst.attrs.NUM; break;
                            case 'HI':
                                tmp[3] |= cnst.attrs.HI; break;
                            case 'NODISP':
                                tmp[3] |= cnst.attrs.NODISP;
                                tmp[3] &= 0b01111111;
                                break;
                            case 'MOD': 
                                tmp[3] |= cnst.attrs.MOD; break;
                            case 'ALPHA': 
                                tmp[3] |= cnst.attrs.ALPHA; break;
                            default:
                                //TODO:
                                break;
                        }
                    }
                    break;
            }
        }
        this.DFLDs[this.cur_dfld_type][pos] = tmp;
    }
    
    mfld(label, params, line_inc) {
// LBL    MFLD  (ENTER,'PRTRAN3 E    '),LTH=13
// to
// ['MFLD', '', [(undefined, ['ENTER', 'PRTRAN3 E    ']), ('LTH', ['13'])]]
// to
// is.MFLDin.root = {POS(1,2):  pl.CHAR(13).INIT('PRTRAN3 E    ')}
        let POS = undefined;
        let LEN = undefined;
        let JSTF = '';
        let FILL = 0;
        let ATTR = false;
        let STR = '';
        for (const param of params) {
            switch (param[0]) {
                case undefined:
                    // MFLD (ENTER,'literal') where ENTER is hidden label (this.PFKlabel)
                    // MFLD (LABEL,'literal') where LABEL is normal label
                    // MFLD (DATE,DATE2)
                    STR = param[1][1]; //literal
                    let label = param[1][0];
                    if (label === this.PFKlabel) {
                        this.PFK[cnst.AIDs.ENTER] = STR;
                        POS = this.PFKlabel;
                    } else {
                        POS = this.label2pos(label, line_inc);
                    }
                    break;
                case 'ID':
                    POS = this.label2pos(param[1][0], line_inc); break;
                case 'LTH':
                    LEN = Number(param[1][0]); break;
                case 'FILL':
                    FILL = param[1][0]; break;
                case 'JUST': 
                    JSTF = param[1][0]; break;
                case 'STR':
                    STR = param[1][0]; break;
                case 'ATTR':
                    ATTR = (param[1][0] == 'YES'); break;
                default:
                    console.error("MFLD Unknown param: " + param[0]);
            }
        }
        if (this.MSGTYPE == 'INPUT') {
            if (!POS) POS = Math.random();
            if (LEN === undefined)
                this.MFLDin.root[POS] = new pl.CHAR(STR.length);
            else 
                this.MFLDin.root[POS] = new pl.CHAR(LEN);
            if (STR) this.MFLDin.root[POS].v = STR;
            // if (FILL) this.MFLDin.root[POS].v = FILL.repeat(LEN);
        } else if (this.MSGTYPE == 'OUTPUT') {
            if (LEN !== undefined) {
                if (!POS) POS = Math.random();
                if (ATTR) {
                    this.MFLDout.root[POS] = {"ATTR": new pl.CHAR(2), "STR": new pl.CHAR(LEN - 2)};
                } else {
                    this.MFLDout.root[POS] = {"STR": new pl.CHAR(LEN)};
                }
                if (STR) this.MFLDout.root[POS]["STR"].v = STR;
            } else { //TODO:
            }
        }
        else 
            console.error("MSGTYPE not set");
    }
    
    label2pos(lbl, line_inc) {
        if (this.label_pos_cache.hasOwnProperty(lbl) && line_inc < this.label_pos_cache[lbl].length)
            return this.label_pos_cache[lbl][line_inc];
        // for (const pos of Object.keys(this.DFLD)) {
        //     if (lbl == this.DFLD[pos][1])
        //         return pos;
        // }
        return undefined;
    }
    
    msg(line) {
//['MSG', 'PROIFM', [('TYPE', ['INPUT']), ('SOR', ['PROIFD']), ('NXT', ['PRTEST1'])]]
//to this.MSGTYPE ='INPUT'
        for (const l of line[2]) {
            switch(l[0]) {
                case 'TYPE':
                    this.MSGTYPE = l[1][0]; break;
                case 'SOR':
                    break;
                case 'NXT':
                    break;
                case 'FILL':
                    break;
                default:
                    console.error("MSG Unknown param: " + l[0]);
            }
        }
    }

/**
 * At the end of MSG block creates inited Object with based buffer to simplify transfering data
 *
 * MSGEND statement
 * The MSGEND statement terminates a message input or output definition and is required as the last statement in the definition.
 * If this is the end of the job submitted, it must also be followed by an END compilation statement.
 */
    msgend() {
        if (this.MSGTYPE == 'INPUT') {
            pl.objInit(this.MFLDin.root);
            this.MFLDin.outArea = new pl.CHAR(this.MFLDin.root.size);
            this.MFLDin.sharedbuf = new SharedArrayBuffer(Math.ceil(this.MFLDin.outArea.size/4)*4); 
            this.MFLDin.outArea.buf = Buffer.from(this.MFLDin.sharedbuf);

            //copy fields to shared buffer due to unusual object creation
            for (const mfld of Object.values(this.MFLDin.root)) {
                if (mfld instanceof pl.CHAR)
                    mfld.buf.copy(this.MFLDin.outArea.buf, mfld.offset, 0, mfld.size);
            }
            //copy fields from shared buffer back
            this.MFLDin.root.BASED(this.MFLDin.outArea);

            this.MFLDin.root.LL.v = this.MFLDin.outArea.size;
        } else if (this.MSGTYPE == 'OUTPUT') { 
            pl.objInit(this.MFLDout.root);
            this.MFLDout.inArea = new pl.CHAR(this.MFLDout.root.size);
            this.MFLDout.root.BASED(this.MFLDout.inArea);
        }
    }
    
    POSout(r, c) {
        const address = (r - 1) * 80 + c - 2;
        const b1 = String.fromCharCode(cnst.code_table[(address >> 6) & 0x3F]);
        const b2 = String.fromCharCode(cnst.code_table[address & 0x3F]);
        return b1 + b2;
    }

    POS(r, c) {
        const address = (r - 1) * 80 + c - 1;
        const b1 = String.fromCharCode(cnst.code_table[(address >> 6) & 0x3F]);
        const b2 = String.fromCharCode(cnst.code_table[address & 0x3F]);
        return b1 + b2;
    }
    
    MFLDsend(worker, AID) {

        if (this.PFKlabel != '' && this.MFLDin.root[this.PFKlabel])
            this.MFLDin.root[this.PFKlabel].v = this.PFK[AID];
                    
        //copy DFLD to MFLD
        for (const pos of Object.keys(this.DFLD)) {
            if (pos in this.MFLDin.root)
                this.MFLDin.root[pos].v = this.DFLD[pos][4];
        }

        return worker.postMessage(["rpush", `T${this.TRAN}`, this.MFLDin.sharedbuf, this.MFLDin.outArea.size]);
    }
    
    MFLDrecieve(worker) {
        //let sharedbuf = new SharedArrayBuffer(Math.ceil(this.MFLDout.inArea.size/4)*4); 
        let sharedbuf = new SharedArrayBuffer(1920); 
        let buf = Buffer.from(sharedbuf);
        let blocker = new Int32Array(sharedbuf);

        worker.postMessage(["blpop", `S${this.TRAN}:${this.lterm}`, sharedbuf]); 
        Atomics.wait(blocker, 0, 0);

        buf.copy(this.MFLDout.inArea.buf, 0, 0, this.MFLDout.inArea.size);
        this.MFLDout.inArea.bump_subs();

        //this is message switch
        if (this.MFLDout.root.newformat != "") {
            this.readFormat(String(this.MFLDout.root.newformat));
            buf.copy(this.MFLDout.inArea.buf, 0, 0, this.MFLDout.inArea.size);
            this.MFLDout.inArea.bump_subs();
        }

        //copy MFLD to DFLD
        for (const pos of Object.keys(this.DFLD)) {
            if (pos in this.MFLDout.root)
                this.DFLD[pos][4].v = this.MFLDout.root[pos]["STR"];
        }
    }

    clear() {
        this.cur_dfld_type = 2;
        this.DFLDs = [{},{},{},{},{},{}];
        this.DFLD = {};
        this.PFK = {};
        this.PFKlabel = '';
        this.MSGTYPE = "";
        this.MFLDin = {};
        this.MFLDin.root = {};  
        this.MFLDout = {};
        this.MFLDout.root = {};
        this.TRAN = '';
        this.CURSOR = this.POS(1,2);
        this.label_pos_cache = {};
        this.TN3270Header = {
            "DATA-TYPE":     0,
            "REQUEST-FLAG":  0,
            "RESPONSE-FLAG": 0,
            "SEQ-NUMBER":    0
            };

        this.MFLDin.root.lterm = new pl.CHAR(8);
        this.MFLDin.root.LL = new pl.FIXED_BIN(15);
        this.MFLDin.root.ZZ = new pl.FIXED_BIN(15);

        this.MFLDout.root.newformat = new pl.CHAR(8);
        this.MFLDout.root.LL = new pl.FIXED_BIN(15);
        this.MFLDout.root.ZZ = new pl.FIXED_BIN(15);
    }

    do(Parameters) {
        let [count, line_increment, position_increment] = Parameters[0];
        if (!line_increment) line_increment = 1;
        for (let i = 0; i < Number(count); i += line_increment) {
            for (const line of Parameters[1]) {
                switch(line[0]) {
                case "DFLD":
                    this.dfld(line, i);
                break;
                case "MFLD":
                    this.mfld(line[1], line[2], i);
                break;
                }
            }   
        }
    }

    readFormat(screenfile) {
        try {
            const raw = fs.readFileSync(`./transactions/${screenfile}.hlasm`, "utf8");
            const output = prep.preprocessor(raw);
            const ast = parser.parse(output);
            this.clear();
            this.compile_tree(ast);
            this.TRAN = sysgen[screenfile];
        } catch (error) {
            return String(error);
        }
        return "";
    }
}
    
module.exports = { TN3270screen }
