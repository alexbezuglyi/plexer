# IBM IMS simulator

Features:
- 3270 message format service https://www.ibm.com/docs/en/ims/15.1.0?topic=reference-mfs-message-formats
- IMS DC transaction manager (dli calls)
- IMS DB database manager (SSA, GU and so)

# To run the to tool in a cloud (docker):
> docker-compose up --build

or start 3 separate services: 

> node tn3270e.js

> service redis-server start

> cd transactions & NODE_PATH=../ node --trace-uncaught calc.js 


# To connect to the server with 3270 terminal create a new session:
> 1. Host ................... : localhost
> 3. TCP Port ............... : 23567
> 4. Model Number ........... : 2 (24 rows x 80 columns)
> 5. Oversize  .............. : (none)
> 9. TLS (SSL) Tunnel ....... : No

# Convert (or try to convert) PL/I to JS
> NODE_PATH=./ node cli save ./transactions/DSN8IP6.pli > 1.js

where ./transactions/DSN8IP6.pli path to the PL/I source and 1.js is a desired file name 