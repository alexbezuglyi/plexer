
function DSN8IP1(IO_PCB, DBD_PCB) { //OPTIONS(MAIN)
var IO_TERMINAL_PCB = new pl.objInit({
IO_TERMINAL_NAME: new pl.CHAR(8),
IO_RESERVED: new pl.CHAR(2),
IO_STATUS_CODE: new pl.CHAR(2),
IO_PREFIX: {
IO_JULIAN_DATE: new pl.FIXED_BIN(31),
IO_TIME_OF_DAY: new pl.FIXED_BIN(31),
IO_MESSAGE_SEQ: new pl.FIXED_BIN(31),
MODNAME: new pl.CHAR(8),
},
}).BASED(pl.ADDR(IO_PCB));
var IN_AREA = new pl.CHAR(255);
var OUT_AREA = new pl.CHAR(1178);
var DSN8IPNO = new pl.objInit({
LL: new pl.FIXED_BIN(15).INIT(110),
ZZ: new pl.FIXED_BIN(15).INIT(0),
ERRMSG: new pl.CHAR(79),
LASTNAME: new pl.CHAR(15),
FIRSNAME: new pl.CHAR(12),
}).BASED(OUT_AREA);
var DSN8IPNI = new pl.objInit({
LL: new pl.FIXED_BIN(15),
ZZ: new pl.FIXED_BIN(15),
TRANSACT: new pl.CHAR(7),
ACTION: new pl.CHAR(1),
LASTNAME: new pl.CHAR(15),
FIRSNAME: new pl.CHAR(12),
}).BASED(IN_AREA);
var DSN8IPLO = new pl.objInit({
LL: new pl.FIXED_BIN(15),
ZZ: new pl.FIXED_BIN(15),
ERRMSG: new pl.CHAR(79),
LINES: new pl.ARR(15, {
FIRSNAME: new pl.CHAR(12),
MIDDLE: new pl.CHAR(1),
LASTNAME: new pl.CHAR(15),
PHONENO: new pl.CHAR(4),
EMPLOYEE: new pl.CHAR(6),
DEP: new pl.CHAR(3),
DEPNAME: new pl.CHAR(32),
}),
}).BASED(OUT_AREA);
var DSN8IPLI = new pl.objInit({
LL: new pl.FIXED_BIN(15),
ZZ: new pl.FIXED_BIN(15),
TRANSACT: new pl.CHAR(7),
ACTION: new pl.CHAR(1),
LINES: new pl.ARR(15, {
PHONENO: new pl.CHAR(4),
EMPLOYEE: new pl.CHAR(6),
}),
}).BASED(IN_AREA);
ims.tdli(THREE, "GU  ", IO_PCB, IN_AREA);
while(IO_TERMINAL_PCB.IO_STATUS_CODE == '  ') {

if ( DSN8IPNI.ACTION == 'L' )
{

if ( pl.SUBSTR(DSN8IPNI.LASTNAME, 1, 1) == '*' )
{

FILL_DSN8IPLO();
ims.tdli(FOUR, "ISRT", IO_PCB, OUT_AREA, "DSN8IPL");
ims.tdli(THREE, "GU  ", IO_PCB, IN_AREA);
}
else
{

DSN8IPNO.LASTNAME.v = 'Grace';
DSN8IPNO.FIRSNAME.v = 'Hopper';
DSN8IPNO.ERRMSG.v = 'User found succesfully';
}
}
if ( DSN8IPLI.ACTION == 'U' )
{

FILL_DSN8IPLO();
DSN8IPLO.ERRMSG.v = 'More Users found succesfully';
}
ims.tdli(THREE, "ISRT", IO_PCB, OUT_AREA);
ims.tdli(THREE, "GU  ", IO_PCB, IN_AREA);
}

function FILL_DSN8IPLO() {
DSN8IPLO.LINES(1).FIRSNAME.v = 'Grace';
DSN8IPLO.LINES(1).MIDDLE.v = 'B';
DSN8IPLO.LINES(1).LASTNAME.v = 'Hopper';
DSN8IPLO.LINES(1).PHONENO.v = '1800';
DSN8IPLO.LINES(1).EMPLOYEE.v = 'N1949';
DSN8IPLO.LINES(1).DEP.v = 'EMC';
DSN8IPLO.LINES(1).DEPNAME.v = 'Eckert–Mauchly Computer Corp';
DSN8IPLO.LINES(2).FIRSNAME.v = 'Alan';
DSN8IPLO.LINES(2).MIDDLE.v = 'M';
DSN8IPLO.LINES(2).LASTNAME.v = 'Turing';
DSN8IPLO.LINES(2).PHONENO.v = '1912';
DSN8IPLO.LINES(2).EMPLOYEE.v = 'GCHQ';
DSN8IPLO.LINES(2).DEP.v = 'BCO';
DSN8IPLO.LINES(2).DEPNAME.v = 'British codebreaking org';
} //FILL_DSN8IPLO
} //DSN8IP1