class ARR {
    constructor(...dim) {
        let arr;

        switch (dim.length) {
            case 2:
                if (typeof dim[0] === 'string') {
                    let [start, end] = dim[0].split(':');
                    start = Number(start);
                    end = Number(end);
                    arr = Array(end - start).fill(dim[1]);
                    arr.start = start;
                    arr.end = end;
                } else {
                    arr = Array(dim[0]).fill(dim[1]);
                }
                break;
            case 3: arr = Array(dim[0]).fill(Array(dim[1]).fill(dim[2])); break;
            case 4: arr = Array(dim[0]).fill(Array(dim[1]).fill(Array(dim[2]).fill(dim[3]))); break;
            default: throw "plexer does not support more than 3 dimensions";
        }

        this.ret = function (...dim) {
            switch (dim.length) {
                case 1:
                    if (arr.start) {
                        return arr[dim[0] - arr.start];
                    } else return arr[dim[0]];
                case 2: return arr[dim[0]][dim[1]];
                case 3: return arr[dim[0]][dim[1]][dim[2]];
                default: throw "plexer does not support more than 3 dimensions";
            }
        }

        return this.ret;
    }
}

// declare 1 A (2,2),
//   (2 B (2),
//     3 C,
//     3 D,
//   2 E) fixed bin(15);

var A = new ARR(2, 3, {
    B: new ARR(4, {
        C: "new pl.FIXED_BIN(15)",
        D: "new pl.FIXED_BIN(15)"
    }),
    E: "new pl.FIXED_BIN(15)"
});

//   if ( A.B.C(1, 1, 2) == A(1, 1, 2).B.C == A(1, 1).B(2).C == A.B(1, 1, 2).C ) return;

// Declare 1 Year(1901:2100),
//   3 Month(12),
//     5 Temperature,
//       7 High decimal fixed(4,1),
//       7 Low decimal fixed(4,1),
//     5 Wind_velocity,
//       7 High decimal fixed(3),
//       7 Low decimal fixed(3),
//     5 Precipitation,
//       7 Total decimal fixed(3,1),
//       7 Average decimal fixed(3,1),
//   3 * char(0);


var Year = new ARR("1901:2100", {
    Month: "blabla"
});

A(1, 2).B(3).C += ';';

console.dir(A(1, 2).B(3).C);

Year(1999).Month = "barfoo";

function dcls() { //OPTIONS(MAIN)
    var STR = new pl.CHAR(255);
    var A = new pl.FIXED_BIN(31);
    var B = new pl.FIXED_BIN(31);
    var C = new pl.FIXED_BIN(31);
    var D = new pl.FIXED_BIN(31);

    var E = new pl.FIXED_DEC(6, 5).STATIC();
    var F = new pl.CHAR(10).STATIC();

    var A = new pl.FIXED_BIN(10).EXTERNAL();
    var B = new pl.FIXED_BIN(10).EXTERNAL();
    var C = new pl.FLOAT_BIN(5).EXTERNAL();

    var Year = new new pl.ARR("1901:2100", {
        Month: new pl.ARR(12, {
            Temperature: {
                High: new pl.FIXED_DEC(4, 1),
                Low: new pl.FIXED_DEC(4, 1),
            },
            Wind_velocity: {
                High: new pl.FIXED_DEC(3),
                Low: new pl.FIXED_DEC(3),
            },
            Precipitation: {
                Total: new pl.FIXED_DEC(3, 1),
                Average: new pl.FIXED_DEC(3, 1),
            },
        }),
        0.9360070682844452: new pl.CHAR(0),
    });
    return (1);
} //dcls
dcls();