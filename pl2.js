/**
 * Author: Oleksii Bezuhlyi
 * 
 * This is part of plexer PL/I to JavaScript translator 
 * (or transpiler, see https://en.wikipedia.org/wiki/Source-to-source_compiler) 
 * Main idea is to make JavaScript similar to PL/I for the sake of simple translation
 */

const fs = require("fs");

function objInit(obj) {
    obj.offset = 0;
    scatter_root(obj, obj);
    obj.size = obj.offset;
    obj.offset = 0;
    obj.subs = [];
    obj.__proto__.BASED = function(based) {
        based.subs.push(this);
        this.base = based;
        if (based instanceof PTR) {
            initedObj_bump_subs(based);
        } else if (based instanceof plObj) {
            based.bump_subs();
        } else if (based.constructor.name === 'Object') {
            initedObj_bump_subs(based);
        }
        return this;
    }
    return obj;
}

/**
 * 1. calc size of Object
 * 2. calc offset for each property
 * 3. point each property with its root
 * @param {Object} that 
 */
function scatter_root(that, root) {
    for (const prop of Object.values(that)) {
        if (prop == undefined || prop == root || prop == that
            || prop == that.subs) {
            continue;
        } else if (prop instanceof plObj) {
            prop.root = root;
            prop.offset = that.offset;
            that.offset += prop.size;
        } else if (prop.constructor.name == "Object") {
            prop.root = root;
            prop.offset = that.offset;
            scatter_root(prop, root);
            prop.size = prop.offset - that.offset ;
            [that.offset, prop.offset] = [prop.offset, that.offset];
        } else if (prop.constructor.name == "Number") {
        } else if (prop.constructor.name == "Function") {
            if (prop.name === 'plarr') {
                prop.scatter_root(that, root);
            } else throw "TODO: scatter_root Function" + prop.name;
        } else if (prop.constructor.name == "Array") {
            //TODO: what to do if prop is subs
            throw("TODO: scatter_root Array");
        } else {
            throw("scatter_root: Unknown object: " + prop.constructor.name);
        }
    }
}

const ascii2ebcdic = [0x00,0x01,0x02,0x03,0x1A,0x09,0x1A,0x7F,0x1A,0x1A,0x1A,0x0B,0x0C,0x0D,0x0E,0x0F,0x10,0x11,0x12,0x13,0x3C,0x3D,0x32,0x26,0x18,0x19,0x3F,0x27,0x1C,0x1D,0x1E,0x1F,0x40,0x4F,0x7F,0x7B,0x5B,0x6C,0x50,0x7D,0x4D,0x5D,0x5C,0x4E,0x6B,0x60,0x4B,0x61,0xF0,0xF1,0xF2,0xF3,0xF4,0xF5,0xF6,0xF7,0xF8,0xF9,0x7A,0x5E,0x4C,0x7E,0x6E,0x6F,0x7C,0xC1,0xC2,0xC3,0xC4,0xC5,0xC6,0xC7,0xC8,0xC9,0xD1,0xD2,0xD3,0xD4,0xD5,0xD6,0xD7,0xD8,0xD9,0xE2,0xE3,0xE4,0xE5,0xE6,0xE7,0xE8,0xE9,0x4A,0xE0,0x5A,0x5F,0x6D,0x79,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x91,0x92,0x93,0x94,0x95,0x96,0x97,0x98,0x99,0xA2,0xA3,0xA4,0xA5,0xA6,0xA7,0xA8,0xA9,0xC0,0x6A,0xD0,0xA1,0x07,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x3F]
const ebcdic2ascii = [0x00,0x01,0x02,0x03,0x1A,0x09,0x1A,0x7F,0x1A,0x1A,0x1A,0x0B,0x0C,0x0D,0x0E,0x0F,0x10,0x11,0x12,0x13,0x1A,0x1A,0x08,0x1A,0x18,0x19,0x1A,0x1A,0x1C,0x1D,0x1E,0x1F,0x1A,0x1A,0x1A,0x1A,0x1A,0x0A,0x17,0x1B,0x1A,0x1A,0x1A,0x1A,0x1A,0x05,0x06,0x07,0x1A,0x1A,0x16,0x1A,0x1A,0x1A,0x1A,0x04,0x1A,0x1A,0x1A,0x1A,0x14,0x15,0x1A,0x1A,0x20,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x5B,0x2E,0x3C,0x28,0x2B,0x21,0x26,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x5D,0x24,0x2A,0x29,0x3B,0x5E,0x2D,0x2F,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x7C,0x2C,0x25,0x5F,0x3E,0x3F,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x60,0x3A,0x23,0x40,0x27,0x3D,0x22,0x1A,0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68,0x69,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x6A,0x6B,0x6C,0x6D,0x6E,0x6F,0x70,0x71,0x72,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x7E,0x73,0x74,0x75,0x76,0x77,0x78,0x79,0x7A,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x7B,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x7D,0x4A,0x4B,0x4C,0x4D,0x4E,0x4F,0x50,0x51,0x52,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x5C,0x1A,0x53,0x54,0x55,0x56,0x57,0x58,0x59,0x5A,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A,0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x1A,0x1A,0x1A,0x1A,0x1A,0x1A]

const oldWrite = Buffer.prototype.write;
Buffer.prototype.write = function (string, offset, length, encoding) {
    if (encoding === 'ascii2ebcdic') {
        for (let pos = length - 1; pos >= 0; pos--) {
            this.writeUInt8(ascii2ebcdic[string.charCodeAt(pos)], offset + pos, 1);
        }
        return length;
    } 
    else {
        return oldWrite.call(this, string, offset, length, encoding);
    }
}

const oldToString = Buffer.prototype.toString;
Buffer.prototype.toString = function (encoding, start, end) {
    if (encoding === 'ebcdic2ascii') {
        let ret = "";
        if (end === undefined) {
            end = this.length;
        }
        if (start === undefined) {
            start = 0;
        }
        for (let pos = start; pos <= end - 1; pos++) {
            //skip if 0x0
            if (this.readUInt8(pos)) ret += String.fromCharCode(ebcdic2ascii[this.readUInt8(pos)]);
        }
        return ret;
    }
    else {
        return oldToString.call(this, encoding, start, end);
    }
} 

let otherwisepl = undefined;
let typepl = undefined;
function select(param, func) {
    otherwisepl = true;
    if (param instanceof plObj) {
        typepl = param.native.trim();
    } else {
        typepl = param;
    }
    func();
}
function when(...params) {
    let paramhere = false;
    for (const param of params) {
        if (param.constructor.name === "Function" && paramhere) {
            param();
        } else if (param == typepl) { //typepl is closure
            paramhere = true;
            otherwisepl = false;
       }
    }
}
function otherwise(func) {
    if (otherwisepl) {
        func();
    }
}

class Internals {
    constructor(offset = 0, root = undefined, base = undefined) {
        this.offset = offset,
        this.root = root,
        this.base = base
    }
}

class plObj {
    constructor(sz, offset = 0) {
        this.subs = [];
        this.size = sz;
        // TODO: Static method: Buffer.from(object[, offsetOrEncoding[, length]]) if BASED
        this.buf = Buffer.alloc(sz);
        this.root = undefined;
        this.offset = offset;
        this.sss_pos = 0;
        this.sss_length = 0;
    }

    bump_subs() {
        for (const sub of this.subs) {
            this.unpack(sub);
        }
    }

    unpack(target) {
        let ret = target;
        if (target instanceof PTR) {

        } else if (target instanceof plObj) {
            target = { key1: target };
        }
        for (const key of Object.keys(target)) {
            let prop = target[key];
            if (prop == this  || prop == undefined) continue; /* property is base */
            if (key == "subs" || key == "root" || key == "buf") continue;
            if (prop instanceof plObj ) {
                this.buf.copy(prop.buf, 0, prop.offset, prop.offset + prop.size);
                prop.buf2native();
            } else if (prop.constructor.name == "Object") {
                this.unpack(prop);
            } else if (prop.constructor.name == "Number") {
            } else if (prop.constructor.name == "Function") {
                if (prop.name === 'plarr') {
                    prop.unpack(this);
                } else throw "TODO: scatter_root Function" + prop.name;
            } else if (prop.constructor.name == "Array") {
                throw("TODO: unpack Array");
            } else {
                throw("unpack: Unknown object: " + prop.constructor.name);
            }
        }
        return ret;
    }

    BASED(based) {
        // TODO: this.buf = Static method: Buffer.from(object[, offsetOrEncoding[, length]])  SharedArrayBuffer
        if (!Array.isArray(based.subs))
            throw "based isn't inited";
        based.subs.push(this);
        this.base = based;
    
        if (based instanceof PTR) {
            initedObj_bump_subs(based);
        } else if (based instanceof plObj) {
            based.bump_subs();
        } else if (based.constructor.name === 'Object') {
            initedObj_bump_subs(based);
        }
        return this;
    }

    INIT(value) {
        if (value !== undefined) {
            this.writeBuf(value);
            this.native = value;
        }
        return this;
    }

    STATIC() {
        return this;
    }

    EXTERNAL() {
        return this;
    }
}

class PTR extends plObj {
    constructor() {
        super(0);
    }

    set v(value) {
        Object.assign(this, value);
    }
}

//PROPOSTITION:
//base2chr
//base2obj
//base2bin

class CHAR extends plObj {
    constructor(sz, ...rest) {
        super(sz, ...rest);
        this.native = ' '.repeat(sz);
    }

    toString() {
        return String(this.native);
    }

    writeBuf(value) {
        this.buf.write(value, 0, this.size, "ascii2ebcdic");
    }

    /**
     * @param {string} value
     */
    set v(value) {
        if (value.constructor.name === "Number") {
            value = String(value);
        }
        if (value.constructor.name === "String") {
            // this.native = String(value);
            if (this.sss_pos > 0 || this.sss_length > 0) { //SUBSTR() = smth; case
                this.buf.write(value, this.sss_pos, this.sss_length, "ascii2ebcdic");
                this.sss_pos = 0;
                this.sss_length = 0;
            } else {
                // this.buf.write(value, 0, this.size, "ascii2ebcdic");
                this.writeBuf(value);
            }
        } else if (value instanceof plObj) {
            if (this.sss_pos > 0 || this.sss_length > 0) {
                if (value.sss_pos > 0 || value.sss_length > 0)
                    value.buf.copy(this.buf, this.sss_pos, value.sss_pos, value.sss_length);
                else 
                    value.buf.copy(this.buf, this.sss_pos, 0, this.sss_length);
                this.sss_pos = 0;
                this.sss_length = 0;
                //TODO: sss_pos and sss_length to sss (sub string set)
                
            } else {
                value.buf.copy(this.buf, 0, 0, this.size);
            }
            this.buf2native();
        }
        if (this.base !== undefined) {
            // if (this.base instanceof plObj) {
                // console.log("TODO: this.base instanceof plObj");
            // } else 
            if (this.base instanceof Object) {
                this.unpack(this.base);
            }
//TODO: bump_base and base is initedObj initedObj_bump_subs(based);
        }
        if (this.root !== undefined) {
            if (this.root.base !== undefined) {
                this.buf.copy(this.root.base.buf, this.offset);
                this.root.base.buf2native();
                this.root.base.bump_subs();
            } 

            if (Array.isArray(this.root.subs)) { 
                for (const sub of this.root.subs) { 
                    this.buf.copy(sub.buf, this.offset);
                    sub.buf2native();
                }
            }
        }
        if (value.constructor.name === "String") {
            this.native = String(value);
        }
        this.bump_subs(); //if this is CHAR
    }

    buf2native() {
        this.native = String(this.buf.toString('ebcdic2ascii'));
    }

    dup() {
        return new CHAR(this.size);
    }

    array(...dim) {

    }

    // * generator() {
    //     while(true) {
    //         yield new CHAR(this.size);
    //     };
    // }
}

class FIXED_BIN extends plObj {
    constructor(sz) {
        if (sz <= 7) {
            super(1);
        } else if (sz <= 15) {
            super(2);
        } else if (sz <= 31) {
            super(4);
        } else {
            throw "wrong size of FIXED_BIN";
        }
        this.sz = sz;
    }

    valueOf() {
        return this.native;
    }

    toString() {
        return String(this.native);
    }
    
    /**
     * @param {Number} value
     */
    set v(value) {
        this.native = Number(value);
        this.writeBuf(this.native);
        if (this.root !== undefined && this.root.base !== undefined) {
            this.buf.copy(this.root.base.buf, this.offset);
        }
        this.bump_subs();
    }

    writeBuf(value) {
        switch(this.size) {
            case 1:
                this.buf.writeInt8(value);
                break;
            case 2:
                this.buf.writeInt16BE(value);
                break;
            case 4:
                this.buf.writeInt32BE(value);
                break;
            default:
                throw "wrong size of FIXED_BIN";
        }
    }

    buf2native() {
        switch(this.size) {
            case 1:
                this.native = this.buf.readInt8();
                break;
            case 2:
                this.native = this.buf.readInt16BE();
                break;
            case 4:
                this.native = this.buf.readInt32BE();
                break;
            default:
                throw "wrong size of FIXED_BIN";
        }
    }

    dup() {
        return new FIXED_BIN(this.sz);
    }
}

class FIXED_DEC extends plObj {
//FIXED DEC OPTIONS(IBM) => ceil( (prec+1)/2) ) bytes
    constructor(sz) {
        super(Math.ceil((sz+1)/2));
        this.sz = sz;
    }

    /**
     * @param {Number} value
     */
    set v(value) {
        this.native = value;
        this.writeBuf(value);
        if (this.root !== undefined && this.root.base !== undefined) {
            this.buf.copy(this.root.base.buf, this.offset);
        }
        this.bump_subs();
    }

    writeBuf(value) {
        let str = String(value);
        if (str.length % 2 == 0) {
            str = "0" + str;
        }
        if (value < 0) {
            str += 'D';
        } else {
            str += 'F';
        }
        const buf2 = Buffer.from(str, 'hex');
        const offset = this.size - buf2.length;
        this.buf.set(buf2, offset < 0 ? 0 : offset);
    }

    valueOf = function() {
        return this.native;
    };

    dup() {
        return new FIXED_DEC(this.sz);
    }
}

class FLOAT_BIN extends plObj {
//FLOAT BIN, precision 23 or less Intel x87 short floating-point => REAL4
//FLOAT BIN, precision 49 or less Intel x87 long floating-point  => REAL8
//FLOAT BIN, precision>50 Intel x87 extended floating-point      => REAL10
    constructor(sz) {
        super(sz);     
    }
    valueOf = function() {
        return this.native;
    };
}

class FLOAT_DEC extends plObj {
//FLOAT DEC, precision 7 or less  => REAL4
//FLOAT DEC, precision 15 or less => REAL8
//FLOAT DEC, precision > 15       => REAL10
    constructor(sz) {
        super(sz);     
    }
    valueOf = function() {
        return this.native;
    };
}

class ARR {
    constructor(...dim) {
        let arr;
        const factor = dim.length - 1;

        switch(dim.length) {
        case 1: throw "0 dimensions? really?";
        case 2: 
            if (typeof dim[0] === 'string') {
                let [start, end] = dim[0].split(':');
                start = Number(start);
                end = Number(end);
                arr = Array.from(Array(end - start), () => ObjDUP(dim[1]));
                arr.start = start;
                arr.end = end;
            } else {
                arr = Array.from(Array(dim[0]), () => ObjDUP(dim[1]));
            }
            break;
        case 3: arr = Array(dim[0]).fill(Array(dim[1]).fill(dim[2])); break;
        case 4: arr = Array(dim[0]).fill(Array(dim[1]).fill(Array(dim[2]).fill(dim[3]))); break;
        default: throw "plexer does not support more than 3 dimensions";
        }

        this.ret = function plarr(...dim) {
            switch(dim.length) {
            case 0: return arr;
            case 1: return arr.start ? arr[dim[0] - arr.start] : arr[dim[0] - 1];
            case 2: return arr[dim[0] - 1][dim[1] - 1];
            case 3: return arr[dim[0] - 1][dim[1] - 1][dim[2] - 1];
            default: throw "plexer does not support more than 3 dimensions";
            }
        }

        this.ret.scatter_root = function(that, root) {
            switch(factor) {
            case 1: 
                for (let prop of arr) {
                    if (prop instanceof plObj) {
                        prop.root = root;
                        prop.offset = that.offset;
                        that.offset += prop.size;
                    } else if(prop instanceof Function) {
                        throw ('TODO: prop instanceof Function');
                    } else if(prop instanceof Object) {
                        prop.root = root;
                        prop.offset = that.offset;
                        scatter_root(prop, root);
                        prop.size = prop.offset - that.offset ;
                        [that.offset, prop.offset] = [prop.offset, that.offset];
                    } else throw "TODO: scatter_root on ARR";
                }
                break;
            case 2: throw "TODO: scatter_root case 2"; break;
            case 3: throw "TODO: scatter_root case 3"; break;
            default: throw "plexer does not support more than 3 dimensions";
            }
        }

        /**
         * source is instanceof plObj
         */
        this.ret.unpack = function(source) {
            switch(factor) {
            case 1: 
                for (let prop of arr) {
                    if (prop instanceof plObj) {
                        source.buf.copy(prop.buf, 0, prop.offset, prop.offset + prop.size);
                        prop.buf2native();
                    } else if (prop.size) { //this is how plexer detects initedObj
                        source.unpack(prop);
                    } else throw "TODO: unpack on ARR ";
                }
                break;
            case 2: throw "TODO: scatter_root case 2"; break;
            case 3: throw "TODO: scatter_root case 3"; break;
            default: throw "plexer does not support more than 3 dimensions";
            }
        }

        this.ret.dup = function() {
            let ret;
            switch(factor) {
            case 1: 
                ret = new ARR(arr.length, arr[0]);
                break;
            case 2: throw "TODO: scatter_root case 2"; break;
            case 3: throw "TODO: scatter_root case 3"; break;
            default: throw "plexer does not support more than 3 dimensions";
            }
            return ret;
        }

        return this.ret;
    }
}

function ObjDUP(obj) {
    var ret = {};
    if (obj instanceof plObj)
        return obj.dup();
    for (const key of Object.keys(obj)) {
        switch(obj[key].constructor.name) {
            case "CHAR":
            case "FIXED_DEC":
            case "FIXED_BIN":
                ret[key] = obj[key].dup();
                break;
            case "Object":
                ret[key] = ObjDUP(obj[key]);
                break;
            case "ARR":
                throw "ObjDUP default case ARR";
            case "Function":
                if (obj[key].name === 'plarr') {
                    ret[key] = obj[key].dup();
                } else throw "ObjDUP Function: " + key;
                break;
            default: 
            throw "ObjDUP default: " + obj[key].constructor.name;
        }
    }
    return ret;
}

function BASED(based, include) {
    if (based.subs == undefined) {
        based.subs = [];
    }
    based.subs.push(include);
    include.base = based;

    if (based instanceof PTR) {
        initedObj_bump_subs(based);
    } else if (based instanceof plObj) {
        based.bump_subs();
    } else if (based.constructor.name === 'Object') {
        initedObj_bump_subs(based);
    }
    return include;
}

function initedObj_bump_subs(initedObj) {
    for (let sub of initedObj.subs) {
        initedObj_pack(initedObj, sub);
    }
}

function initedObj_pack(initedObj, sub) {
    let prop = undefined;
    // if (target instanceof PTR) {
    //     target = { key1: target };
    // }
    for (const key of Object.keys(initedObj)) {
        if (key == "subs" || key == "root") continue;
        prop = initedObj[key];
        if (prop == undefined) {
            continue;
        } else if (prop instanceof plObj) {
            prop.buf.copy(sub.buf, prop.offset, 0, prop.size);
        } else if (prop.constructor.name == "Number") {
        } else if (prop.constructor.name == "Buffer") {
        } else if (prop.constructor.name == "Array") {
            //skip subs
            throw("TODO: initedObj_pack Array");
        } else if (prop.constructor.name == "Object") {
            //skip root
            initedObj_pack(prop, sub);
            // console.log("TODO: initedObj_pack");
        } else {
            throw("initedObj_pack: Unknown object: " + prop.constructor.name);
        }
    }
}

function EXIT() {
    return;
}

function ENTRY() {
    return;
}

function LENGTH(subj) {
    if (subj instanceof plObj) {
        return subj.size;
    } else if (subj.constructor.name == "Object") {
        // return subj.offset;
        return subj.size;
    } else throw "LENGTH can do String or plObj only";    
}

function CSTG(subj) {
    if (subj instanceof String) {
        return subj.length();
    } else if (subj instanceof plObj) {
        return subj.size;
    } else if (subj.constructor.name == "Object") {
        // return subj.offset;
        return subj.size;
    } else throw "CSTG can do String or plObj only";
}

function VERIFY(str, against) {
    return false; //TODO: =)
}

function LINK(module, ...params) {
    let exec = require2("./" + module)[module];
    return exec(...params);
}

function SUBSTR(subj, position, length) {
    const pc_position = position - 1;
    if (subj instanceof String) {
        return subj.slice(pc_position, pc_position + length);
    } else if (subj instanceof CHAR) {
        subj.sss_length = length;
        subj.sss_pos = pc_position;
        subj.buf2native();
        subj.native = subj.native.slice(pc_position, pc_position + length);
        return subj;
    } else if (subj instanceof Object) { // inited Obj: pack to pl.CHAR
        throw "SUBSTR subj instanceof Object";
    } else throw "SUBSTR can do String or pl.CHAR only";
}

function HIGH(length) {
    //ebcdic 0xFF is ascii 0x1A
    return String.fromCharCode(0x1A).repeat(length);
}

function LOW(length) {
    return String.fromCharCode(0x00).repeat(length);
}

function ADDR(obj) {
    return obj;
}

function include(target, ...files) {
    for (const file of files) {
        Object.assign(target, require2(file));
    }
    // target.init(); 
    return objInit(target);
}

//overwrite require because we do not need caching
function require2(file) {
    var script = fs.readFileSync(file + ".js", "utf8");
    var module = {};
    var exports = {};
    eval(script);
    return module.exports ? module.exports : exports;
}

module.exports = {objInit, ARR, PTR, CHAR, FIXED_BIN, FIXED_DEC, FLOAT_BIN, FLOAT_DEC, EXIT, LINK, ENTRY, LENGTH, ADDR, SUBSTR, HIGH, LOW, VERIFY, CSTG, include, select, when, otherwise};
