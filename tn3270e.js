
const net = require("net");

const { cmd, TN, subcommand, DATA_TYPE, AIDs, streamOrder } = require("./js3270lib/constants");
const { TN3270screen } = require("./js3270lib/ScreenManager");

const { Worker } = require('worker_threads');
const worker = new Worker("./redisworker.js");

const ERR_COND_CLEARED = 0x00;

function str2arr(str) {
    return str.split('').map(el => el.charCodeAt(0));
}

function split(array, separator) {
    let ret = [], chunk = [];
    for (let byte of array) {
        if (byte === separator) {
            if (chunk.length)
                ret.push(chunk);
            chunk = [];
        } else {
            chunk.push(byte);
        }
    }
    if (chunk.length)
        ret.push(chunk);
    return ret;
}

function formatScreen(screen) {
    screen.TN3270Header["SEQ-NUMBER"] += 1;
    let DFLD = [0, 0, 0, 0, screen.TN3270Header["SEQ-NUMBER"], 0xF5, 0xC3]; //TODO: SEQ-NUMBER has 2 byte size
    DFLD.push(...[streamOrder.SBA, ...str2arr(screen.CURSOR), streamOrder.IC]);
    for (const key of Object.keys(screen.DFLD)) {
        const POS = str2arr(screen.DFLD[key][0]);
        const ATTR = screen.DFLD[key][3];
        const STR = screen.DFLD[key][4];
        DFLD.push(...[streamOrder.SBA, ...POS, streamOrder.SF, ATTR]);
        if (STR.buf != undefined)
            DFLD.push(...STR.buf);
    }

    return new Uint8Array(DFLD.concat([cmd.IAC, TN.EOR]));
}

function screenHandler(data, screen, worker) {
    let arrdata = new Uint8Array(data);

    if (arrdata.length <= 5) {
        return true
    }

    arrdata = arrdata.subarray(5); // git rid of the header

    const AID = arrdata[0];
    if (AID === AIDs.CLEAR) {
        screen.clear();
        return true;
    }

    //scatter input
    screen.CURSOR = String.fromCharCode(arrdata[1]) + String.fromCharCode(arrdata[2]);
    const fields = split(arrdata.slice(3, -2), streamOrder.SBA);
    for (const field of fields) {
        let POS = String.fromCharCode(field.shift()) + String.fromCharCode(field.shift());
        if (screen.DFLD.hasOwnProperty(POS)) {
            let STR = screen.DFLD[POS][4];
            STR.buf.set(field.slice(0, STR.buf.length));
            STR.buf2native();
            // console.log(`${screen.DFLD[POS][4]}`);
        } else {
            // console.log("POS without DFLD");//TODO: print debug x y
        }
    }

    if (AID === AIDs.ENTER) {
        var formatted = data.toString('ebcdic2ascii').toUpperCase();
        let i = formatted.indexOf("/FOR ");
        if (i >= 0) {
            try {
                const screnname = formatted.slice(i + 5, -2).trim();
                if (screen.readFormat(screnname) === "")
                    return true;
            } catch (e) {
                console.error(e);
            }
        }
        i = formatted.indexOf("/QUIT");
        if (i >= 0)
            return false;
    }

    screen.MFLDsend(worker, AID);

    screen.MFLDrecieve(worker);
    return true;
}

const server = net.createServer(socket => {
    let last_response = undefined;
    let screen = new TN3270screen();

    screen.readFormat("HLWRLD");
    screen.lterm = "TCP" + socket.remotePort.toString();

    let negotiation = {
        start: new Uint8Array([cmd.IAC, cmd.DO, subcommand.TN3270E]),
        DEVICE_TYPE: new Uint8Array([cmd.IAC, cmd.SB, subcommand.TN3270E, TN.SEND, TN.DEVICETYPE, cmd.IAC, cmd.SE]),
        DEVICE_TYPE2: new Uint8Array([cmd.IAC, cmd.SB, subcommand.TN3270E, TN.DEVICETYPE, TN.IS]
            .concat(str2arr("IBM-3278-2-E"))
            .concat([TN.CONNECT])
            .concat(str2arr(screen.lterm))
            .concat([cmd.IAC, cmd.SE])),
        FUNCTIONS_REQUEST: new Uint8Array([cmd.IAC, cmd.SB, subcommand.TN3270E, TN.FUNCTIONS, TN.IS, TN.ASSOCIATE, TN.DEVICETYPE, TN.IS, cmd.IAC, cmd.SE]),
        BIND_IMAGE: new Uint8Array([DATA_TYPE['BIND-IMAGE'], ERR_COND_CLEARED, 0, 0, 0]
            .concat(str2arr("TELNET"))
            .concat([cmd.IAC, TN.EOR]))
    }

    // const responses = {};
    // responses[negotiation.start] = [cmd.IAC, cmd.WILL, subcommand.TN3270E];
    // responses[negotiation.DEVICE_TYPE] = [[cmd.IAC, cmd.SB, subcommand.TN3270E, TN.DEVICETYPE, TN.REQUEST], [cmd.IAC, cmd.SE]];
    // responses[negotiation.DEVICE_TYPE2] = [cmd.IAC, cmd.SB, subcommand.TN3270E, TN.FUNCTIONS,  TN.REQUEST, TN.ASSOCIATE, TN.DEVICETYPE, TN.IS, cmd.IAC, cmd.SE];

    socket.write(negotiation.start);
    last_response = negotiation.start;

    socket.on("data", data => {
        if (last_response !== negotiation.BIND_IMAGE) {
            switch (last_response) {
                case negotiation.start:
                    // assert(data == IAC + WILL + Subcommand['TN3270E'])
                    socket.write(negotiation.DEVICE_TYPE);
                    last_response = negotiation.DEVICE_TYPE;
                    break;
                case negotiation.DEVICE_TYPE:
                    // assert(data[:5] == IAC + SB + Subcommand['TN3270E'] + TN_DEVICETYPE + TN_REQUEST)
                    // assert(data[-2:] == IAC + SE)  
                    socket.write(negotiation.DEVICE_TYPE2);
                    last_response = negotiation.DEVICE_TYPE2;
                    break;
                case negotiation.DEVICE_TYPE2:
                    // assert(data == IAC + SB + Subcommand['TN3270E'] + TN_FUNCTIONS + TN_REQUEST + TN_ASSOCIATE + TN_DEVICETYPE + TN_IS + IAC + SE)
                    socket.write(negotiation.FUNCTIONS_REQUEST);
                    socket.write(negotiation.BIND_IMAGE);
                    socket.write(formatScreen(screen));
                    last_response = negotiation.BIND_IMAGE;
                    break;
                default:
                    //TODO:
                    console.error("//TODO: switch(last_response)");
                    break;
            }
        } else {
            if (screenHandler(data, screen, worker))
                socket.write(formatScreen(screen));
            else
                socket.end();
        }

    });

});

worker.on('error', (code) => {
    if (code !== 0)
        throw (new Error(`Worker stopped with error code ${code}`));
});

worker.on('exit', (code) => {
    if (code !== 0)
        throw (new Error(`Worker stopped with exit code ${code}`));
});

const PORT = process.env.PORT || 23567;
server.listen(PORT, () => {
    console.log("tn3270 started on " + PORT + " port");
});
