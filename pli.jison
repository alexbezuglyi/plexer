%lex
%options case-insensitive
%%
/* Notes:
 * "Keywords, their allowable synonyms, and reserved parameters, appear in uppercase for the
 * MVS platform, and lowercase for UNIX platform. These items must be entered exactly as shown."
 *
 * "You can use a combination of lowercase and uppercase characters in a PL/I program.
 * When used in keywords or identifiers, the lowercase characters are treated as the corresponding
 * uppercase characters. This is true even if you entered a lowercase character as a DBCS character.
 * When used in a comment or in a character, mixed, or a graphic string constant, lowercase characters
 * remain lowercase."
 */
// [Pp][Rr][Oo][Cc][Ee][Ss][Ss]~[\n\r]* //'skip PROCESS'
\s+                    /* skip whitespace */ // fragment Ws : (' ' | '\t' | '\n' | '\r'); 
// \/\*.*?\*\/            //'skip COMMENT'

"ZERODIVIDE"         return 'ZERODIVIDE'
"ZDIV"               return 'ZERODIVIDE'
"XU"                 return 'XU'
"XN"                 return 'XN'
// [xX]                        return 'X'
"WX"                 return 'WX'
"WRITE"              return 'WRITE'
"WINMAIN"            return 'WINMAIN'
"WHILE"              return 'WHILE'
"WHENEVER"           return 'WHENEVER'
"WHEN"               return 'WHEN'
"WAIT"               return 'WAIT'
"WIDECHAR"           return 'WIDECHAR'
"WCHAR"              return 'WIDECHAR'
"VSAM"               return 'VSAM'
"VS"                 return 'VS'
"VBS"                return 'VBS'
"VB"                 return 'VB'
"VARIABLE"           return 'VARIABLE'
"VARYINGZ"           return 'VARYINGZ'
"VARZ"               return 'VARYINGZ'
"VARYING"            return 'VARYING'
"VAR"                return 'VARYING'
"VALUERANGE"         return 'VALUERANGE'
"VALUELISTFROM"      return 'VALUELISTFROM'
"VALUELIST"          return 'VALUELIST'
"VALUE"              return 'VALUE'
"VALIDATE"           return 'VALIDATE'
// [vV]                                        return 'V'
"UPTHRU"             return 'UPTHRU'
"UPDATE"             return 'UPDATE'
"UNTIL"              return 'UNTIL'
"UNSIGNED"           return 'UNSIGNED'
"UNS"                return 'UNSIGNED'
"UNLOCK"             return 'UNLOCK'
"UNION"              return 'UNION'
"UNDERFLOW"          return 'UNDERFLOW_'
"UFL"                return 'UNDERFLOW_'
"UNDEFINEDFILE"      return 'UNDEFINEDFILE'
"UNDF"               return 'UNDEFINEDFILE'
"UNCONNECTED"        return 'UNCONNECTED'
"UNBUFFERED"         return 'UNBUFFERED'
"UNBUFF"             return 'UNBUFFERED'
"UNALIGNED"          return 'UNALIGNED'
"UNAL"               return 'UNALIGNED'
// [uU]               return 'U'
"TYPE"               return 'TYPE'
"TSTACK"             return 'TSTACK'
"TRKOFL"             return 'TRKOFL'
"TRANSMIT"           return 'TRANSMIT'
"TRANSIENT"          return 'TRANSIENT'
"TP"                 return 'TP'
"TOTAL"              return 'TOTAL'
"TO"                 return 'TO'
"TITLE"              return 'TITLE'
"THREAD"             return 'THREAD'
"THEN"               return 'THEN'
"TASK"               return 'TASK'
"SYSTEM"             return 'SYSTEM'
"SUPPRESS"           return 'SUPPRESS'
"SUPPORT"            return 'SUPPORT'
"SUBSCRIPTRANGE"     return 'SUBSCRIPTRANGE'
"SUBRG"              return 'SUBSCRIPTRANGE'
"SUB"                return 'SUB'
"STRUCTURE"          return 'STRUCTURE'
"STRINGVALUE"        return 'STRINGVALUE'
"STRINGSIZE"         return 'STRINGSIZE'
"STRZ"               return 'STRINGSIZE'
"STRINGRANGE"        return 'STRINGRANGE'
"STRG"               return 'STRINGRANGE'
"STRING"             return 'STRING  '
"STREAM"             return 'STREAM'
"STORAGE"            return 'STORAGE'
"STOP"               return 'STOP'
"STDCALL"            return 'STDCALL'
"STATIC"             return 'STATIC'
"SNAP"               return 'SNAP'
"SKIP"               return 'SKIP_'
// "SIZE"               return 'SIZE' // Options not supported Enterprise PL/I for z/OS 5.2
"SIS"                return 'SIS'
"SIGNED"             return 'SIGNED'
"SIGNAL"             return 'SIGNAL'
"SET"                return 'SET'
"SEQUENTIAL"         return 'SEQUENTIAL'
"SEQL"               return 'SEQUENTIAL'
"SELECT"             return 'SELECT'
"SCALARVARYING"      return 'SCALARVARYING'
"REWRITE"            return 'REWRITE'
"REVERT"             return 'REVERT'
"REUSE"              return 'REUSE'
"RETURNS"            return 'RETURNS'
"RETURN"             return 'RETURN'
"RETCODE"            return 'RETCODE'
// RESIGNAL                        return 'RESIGNAL'
"RESERVES"           return 'RESERVES'
"RESERVED"           return 'RESERVED'
"REREAD"             return 'REREAD'
"REPLY"              return 'REPLY'
"REPLACE"            return 'REPLACE'
"REPEAT"             return 'REPEAT'
"REORDER"            return 'REORDER'
"RENAME"             return 'RENAME'
"RELEASE"            return 'RELEASE'
"REGIONAL"           return 'REGIONAL'
"REFER"              return 'REFER'
"REENTRANT"          return 'REENTRANT'
"REDUCIBLE"          return 'REDUCIBLE'
"RED"                return 'REDUCIBLE'
"RECURSIVE"          return 'RECURSIVE'
"RECSIZE"            return 'RECSIZE'
"RECORD"             return 'RECORD'
"REAL"               return 'REAL'
"READ"               return 'READ'
"RANGE"              return 'RANGE'
// [rR]                                        return 'R'
"PUT"                return 'PUT'
"PROCEDURE"          return 'PROCEDURE'
"PROC"               return 'PROCEDURE'
"PRIORITY"           return 'PRIORITY'
"PRINT"              return 'PRINT'
"PRECISION"          return 'PRECISION'
"PREC"               return 'PRECISION'
"POSITION"           return 'POSITION'
"POS"                return 'POSITION'
"POINTER"            return 'POINTER'
"PTR"                return 'POINTER'
"PICTURE"            return 'PICTURE'
"PIC"                return 'PICTURE'
"PENDING"            return 'PENDING'
"PASSWORD"           return 'PASSWORD'
"PARAMETER"          return 'PARAMETER'
"PARM"               return 'PARAMETER'
"PAGESIZE"           return 'PAGESIZE'
"PAGE"               return 'PAGE'
"PACKED"             return 'PACKED'
"PACKAGE"            return 'PACKAGE'
// [pP]                                                    return 'P'
"OVERFLOW"           return 'OVERFLOW_'
"OFL"                return 'OVERFLOW_'
"OUTPUT"             return 'OUTPUT'
"OUTONLY"            return 'OUTONLY'
"OTHERWISE"          return 'OTHERWISE'
"OTHER"              return 'OTHERWISE'
"ORDINAL"            return 'ORDINAL'
"ORDER"              return 'ORDER'
"OPTLINK"            return 'OPTLINK'
"OPTIONS"            return 'OPTIONS'
"OPTIONAL"           return 'OPTIONAL'
"OPEN"               return 'OPEN'
"ON"                 return 'ON'
"OFFSET"             return 'OFFSET'
"NULLINIT"           return 'NULLINIT'
"NOZERODIVIDE"		 return 'NOZERODIVIDE'
"NOZDIV"             return 'NOZERODIVIDE'
"NOWRITE"            return 'NOWRITE'
"NOUNDERFLOW"        return 'NOUNDERFLOW'
"NOUFL"              return 'NOUNDERFLOW'
"NOTE"               return 'NOTE'
"NOSUBSCRIPTRANGE"   return 'NOSUBSCRIPTRANGE'
"NOSUBRG"            return 'NOSUBSCRIPTRANGE'
"NOSTRINGSIZE"       return 'NOSTRINGSIZE'
"NOSTRZ"             return 'NOSTRINGSIZE'
"NOSTRINGRANGE"      return 'NOSTRINGRANGE'
"NOSTRG"             return 'NOSTRINGRANGE'
"NOSIZE"             return 'NOSIZE'
"NORMAL"             return 'NORMAL'
"NOPRINT"            return 'NOPRINT'
"NOOVERFLOW"         return 'NOOVERFLOW'
"NOOFL"              return 'NOOVERFLOW'
"NONVARYING"         return 'NONVARYING'
"NONVAR"             return 'NONVARYING'
"NONE"               return 'NONE'
"NONCONNECTED"       return 'NONCONNECTED'
"NONCONN"            return 'NONCONNECTED'
"NONASSIGNABLE"      return 'NONASSIGNABLE'
"NONASGN"            return 'NONASSIGNABLE'
"NOLOCK"             return 'NOLOCK'
"NOINVALIDOP"        return 'NOINVALIDOP'
"NOINLINE"           return 'NOINLINE'
"NOINIT"             return 'NOINIT'
"NOFIXEDOVERFLOWOFL" return 'NOFIXEDOVERFLOW'
"NOEXECOPS"          return 'NOEXECOPS'
"NODESCRIPTOR"       return 'NODESCRIPTOR'
"NOCONVERSION"       return 'NOCONVERSION'
"NOCONV"             return 'NOCONVERSION'
"NOCHECK"            return 'NOCHECK'
"NOCHARGRAPHIC"      return 'NOCHARGRAPHIC'
"NOCHARG"            return 'NOCHARGRAPHIC'
"NCP"                return 'NCP'
"NAME"               return 'NAME'
"MAIN"               return 'MAIN'
// [mM]                                        return 'M'
"LOOP"               return 'LOOP'
"LOCATE"             return 'LOCATE'
"LOCAL"              return 'LOCAL'
"LITTLEENDIAN"       return 'LITTLEENDIAN'
"LIST"               return 'LIST'
"LINKAGE"            return 'LINKAGE'
"LINESIZE"           return 'LINESIZE'
"LINE"               return 'LINE'
"LIMITED"            return 'LIMITED'
"LIKE"               return 'LIKE'
"LEAVE"              return 'LEAVE'
"LABEL"              return 'LABEL'
"KEYTO"              return 'KEYTO'
"KEYLOC"             return 'KEYLOC'
"KEYLENGTH"          return 'KEYLENGTH'
"KEYFROM"            return 'KEYFROM'
"KEYED"              return 'KEYED'
"KEY"                return 'KEY'
"ITERATE"            return 'ITERATE'
"IRREDUCIBLE"        return 'IRREDUCIBLE'
"IRRED"              return 'IRREDUCIBLE'
"INVALIDOP"          return 'INVALIDOP'
"INTO"               return 'INTO'
"INTERACTIVE"        return 'INTERACTIVE'
"INTER"              return 'INTER'
"INTERNAL"           return 'INTERNAL'
"INT"                return 'INTERNAL'
"INPUT"              return 'INPUT'
"INOUT"              return 'INOUT'
"INONLY"             return 'INONLY'
"INLINE"             return 'INLINE'
"INITIAL"            return 'INITIAL_'
"INIT"               return 'INITIAL_'
"INI"                return 'INITIAL_'
"INDFOR"             return 'INDFOR'
"INDEXED"            return 'INDEXED'
"INDEXAREA"          return 'INDEXAREA'
"INCLUDE"            return 'INCLUDE'
"IN"                 return 'IN'
"IMPORTED"           return 'IMPORTED'
"IGNORE"             return 'IGNORE'
"IF"                 return 'IF'
"IEEE"               return 'IEEE'
// [iI]                                        return 'I'
"HEXADEC"            return 'HEXADEC'
"HANDLE"             return 'HANDLE'
"GX"                 return 'GX'
"GRAPHIC"            return 'GRAPHIC'
"GOTO"               return 'GOTO'
"GO"                 return 'GO'
"GET"                return 'GET'
"GENKEY"             return 'GENKEY'
"GENERIC"            return 'GENERIC'
// [gG]                                                    return 'G'
"FS"                 return 'FS'
"FROMALIEN"          return 'FROMALIEN'
"FROM"               return 'FROM'
"FREE"               return 'FREE'
"FORTRAN"            return 'FORTRAN'
"FORMAT"             return 'FORMAT'
"FOREVER"            return 'FOREVER'
"FORCE"              return 'FORCE'
"FLUSH"              return 'FLUSH'
"FLOAT"              return 'FLOAT'
"FIXEDOVERFLOW"      return 'FIXEDOVERFLOW'
"FOFL"               return 'FIXEDOVERFLOW'
"FIXED"              return 'FIXED'
"FINISH"             return 'FINISH'
"FILE"               return 'FILE_'
"FETCHABLE"          return 'FETCHABLE'
"FETCH"              return 'FETCH'
"FBS"                return 'FBS'
"FB"                 return 'FB'
// [fF]                                                                    return 'F'
"EXTERNAL"           return 'EXTERNAL'
"EXT"                return 'EXTERNAL'
"EXPORTS"            return 'EXPORTS'
"EXIT"               return 'EXIT'
"EXEC"               return 'EXEC'
"EXCLUSIVE"          return 'EXCLUSIVE'
"EXCL"               return 'EXCLUSIVE'
"EVENT"              return 'EVENT'
"ERROR"              return 'ERROR'
"ENVIRONMENT"        return 'ENVIRONMENT'
"ENV"                return 'ENVIRONMENT'
"ENTRY"              return 'ENTRY'
"ENDPAGE"            return 'ENDPAGE'
"ENDFILE"            return 'ENDFILE'
"END"                return 'END'
"ELSE"               return 'ELSE'
"EDIT"               return 'EDIT'
// [eE]                                                return 'E'
"DOWNTHRU"           return 'DOWNTHRU'
"DO"                 return 'DO'
"DISPLAY"            return 'DISPLAY'
"DIRECT"             return 'DIRECT'
"DIMACROSS"          return 'DIMACROSS'
"DIMENSION"          return 'DIMENSION'
"DIM"                return 'DIMENSION'
"DETACH"             return 'DETACH'
"DESCRIPTORS"        return 'DESCRIPTORS'
"DESCRIPTOR"         return 'DESCRIPTOR'
"DELETE"             return 'DELETE'
"DELAY"              return 'DELAY'
"DEFINE"             return 'DEFINE'
"DEFAULT"            return 'DEFAULT'
"DFT"                return 'DEFAULT'
"DEFINED"            return 'DEFINED'
"DEF"                return 'DEFINED'
"DECLARE"            return 'DECLARE'
"DCL"                return 'DECLARE'
DECIMAL\s+FIXED      return 'DECIMAL'
FIXED\s+DECIMAL      return 'DECIMAL'
"DECIMAL"            return 'DECIMAL'
"DEC"                return 'DECIMAL'
"DEACTIVATE"         return 'DEACTIVATE'
"DEACT"              return 'DEACTIVATE'
"DB"                 return 'DB'
"DATE"               return 'DATE'
"DATA"               return 'DATA'
// [dD]                                                return 'D'
"CTLASA"             return 'CTLASA'
"CTL360"             return 'CTL360'
"COPY"               return 'COPY'
"CONVERSION"         return 'CONVERSION'
"CONV"               return 'CONVERSION'
"CONTROLLED"         return 'CONTROLLED'
"CTL"                return 'CONTROLLED'
"CONSTANT"           return 'CONSTANT'
"CONST"              return 'CONSTANT'
"CONSECUTIVE"        return 'CONSECUTIVE'
"CONNECTED"          return 'CONNECTED'
"CONN"               return 'CONNECTED'
"CONDITION"          return 'CONDITION'
"COND"               return 'CONDITION'
"COMPLEX"            return 'COMPLEX'
"CPLX"               return 'COMPLEX'
"COLUMN"             return 'COLUMN'
"COL"                return 'COLUMN'
"COBOL"              return 'COBOL'
"CLOSE"              return 'CLOSE'
"CHECK"              return 'CHECK'
"CHARGRAPHIC"        return 'CHARGRAPHIC'
"CHARG"              return 'CHARGRAPHIC'
"CHARACTER"          return 'CHARACTER'
"CHAR"               return 'CHARACTER'
"CELL"               return 'CELL'
"CDECL"              return 'CDECL'
"CALL"               return 'CALL'
"C"                  return 'C'
"BYVALUE"            return 'BYVALUE'
"BYADDR"             return 'BYADDR'
"BY"                 return 'BY'
"BX"                 return 'BX'
"BUILTIN"            return 'BUILTIN'
"BUFSP"              return 'BUFSP'
"BUFNI"              return 'BUFNI'
"BUFND"              return 'BUFND'
"BUFFOFF"            return 'BUFFOFF'
"BUFFERS"            return 'BUFFERS'
"BUFF"               return 'BUFFERS'
"BUFFERED"           return 'BUFFERED'
"BUF"                return 'BUFFERED'
"BLKSIZE"            return 'BLKSIZE'
"BKWD"               return 'BKWD'
"BIT"                return 'BIT'
"BINARY"             return 'BINARY'
"BIN"                return 'BINARY'
"BIGENDIAN"          return 'BIGENDIAN'
"BEGIN"              return 'BEGIN_'
"BASED"              return 'BASED'
"BACKWARDS"          return 'BACKWARDS'
"B4"                 return 'B4'
"B3"                 return 'B3'
"B2"                 return 'B2'
"B1"                 return 'B1'
"B"                  return 'B'
"AUTOMATIC"          return 'AUTOMATIC'
"AUTO"               return 'AUTOMATIC'
"ATTENTION"          return 'ATTENTION'
"ATTN"               return 'ATTENTION'
"ATTACH"             return 'ATTACH'
"ASSIGNABLE"         return 'ASSIGNABLE'
"ASGN"               return 'ASSIGNABLE'
"ASSEMBLER"          return 'ASSEMBLER'
"ASM"                return 'ASSEMBLER'
"ASCII"              return 'ASCII'
"AREA"               return 'AREA'
"ANYCONDITION"       return 'ANYCONDITION'
"ANYCOND"            return 'ANYCONDITION'
"ALLOCATE"           return 'ALLOCATE'
"ALLOC"              return 'ALLOCATE'
"ALIGNED"            return 'ALIGNED'
"ALIAS"              return 'ALIAS'
"ADDBUFF"            return 'ADDBUFF'
"ACTIVATE"           return 'ACTIVATE'
"ACT"                return 'ACTIVATE'
"ABNORMAL"           return 'ABNORMAL'
"A"                  return 'A'

'SEPARATE_STATIC'    return 'SEPARATE_STATIC'
'PACKED_DECIMAL'     return 'PACKED_DECIMAL'
'NON_QUICK'          return 'NON_QUICK'
'NO_QUICK_BLOCKS'    return 'NO_QUICK_BLOCKS'

'%PAGE'              return 'PP_PAGE'
'%PRINT'             return 'PP_PRINT'
'%SKIP'              return 'PP_SKIP'
'%NOPRINT'           return 'PP_NOPRINT'
'%NOTE'              return 'PP_NOTE'

\<\>|\^\=|\~\=       return 'NE'
\^\<|\~\<|\>\=       return 'GE'
\^\>|\~\>|\<\=       return 'LE'
[\+\-\*\|\!\&]\=     return 'SELFOP'
\=\>                 return 'HANDLEPTR'
\-\>                 return 'PTR'
\*\*                 return 'POWER'
[\^\~]               return 'NOT'
\&                   return 'AND'
\|\||\!\!            return 'CONCAT'
[\|\!]               return 'OR'
    
[0-9][0-9_]*         return 'NUM'
\'[01]*\'B           return 'BINCONST'
// fragment Nbree : [DEFSQ] [+-]? Nbr;
\"[^\"\n\r]*\"       return 'STR_CONSTANT'
\'[^\'\n\r]*\'       return 'STR_CONSTANT'
[a-zA-Z_$#@]([a-zA-Z_$#@]|[0-9])* return 'VARNAME'

";"                  return ';'
":"                  return ':'
"("                  return '('
")"                  return ')'
","                  return ','
"*"                  return '*'
"'"                  return "'"
"="                  return "="
"."                  return "."
"<"                  return "<"
">"                  return ">"

<<EOF>>              return 'EOF'

/lex

%{
	const pl = require("../../../plexer");

	const subts = {
		"#":"$",
        "date" : "time"
	};

	const newKWs = {
		"LOW" :"pl.LOW",
        "ADDR" : "pl.ADDR",
		"INDEX" : "pl.INDEX", 
		"SUBSTR" : "pl.SUBSTR",
		"PLITDLI": "ims.tdli"
	};

    function substituteM(varname) {
		let ret = varname;
    	for (const sub of Object.keys(subts)) {
			const replacer = new RegExp(sub, 'gm');
			ret = ret.replace(replacer, subts[sub]);
		}
		for (const sub of Object.keys(newKWs)) {
			if (varname === sub)
				ret = newKWs[sub];
		}
		return ret;				
    }

%}

%start pl1pgm

%%

pl1pgm
	: pl1stmtlist EOF { return $$; } //{console.dir($1, { depth: null });}
	| EOF
	;

pl1stmtlist
	: pl1stmt ';'               {{ $$ = [$1]; }}
	| pl1stmtlist pl1stmt ';'   {{ $$ = $1.concat($2); }}
	;

pl1stmt
	: stmt 
	| varname ':' stmt          { $$ = [$1 + ':', ...$3]; }
	| stmtscope
	| varname ':' stmtscope     { $$ = $1 + ':' + $3; }
	| varname ':' procedurestmt { $$ = '\nfunction ' + $1 + $3; }
	| endstmt
	| varname ':' endstmt       { $$ = $1 + ':' + $3; }
  	;

stmtscope
	: beginstmt     
	| dostmt		
	| entrystmt     
	| packagestmt   
	| SELECT              { throw "TODO:selectstmt"; }
	// | SELECT '(' expr ')' { $$ = "pl.select(" + $3 + ", () => {"; }	
	| PP_PAGE                                   { $$ = '//%PAGE';}	
	| PP_PAGE '(' NUM ')'                       { $$ = '//%PAGE';}	
	| PP_PRINT                                  { $$ = '//%PRINT';}	
	| PP_SKIP                                   { $$ = '//%SKIP';}	
	| PP_SKIP '(' NUM ')'                       { $$ = '//%SKIP';}	
	| PP_NOTE '(' STR_CONSTANT ',' constant ')' { $$ = '//%NOTE';}	
	;

stmt
	:/* empty */	
	| ALLOCATE allocateoptionlist { $$ = 'pl.ALLOCATE(' + $2; }	
	| assignstmt	
	| attachstmt	
	| CALL calloptionlist	{ $$ = $2.search(`\\(`) > 0 ? [$2 + ';'] : [$2 + '();'];}	
	| CLOSE closegrouplist	
	| dclstmt	
	| defaultstmt	
	| DEFINE ALIAS varname dcloptionlist 
	| defineordinalstmt
	| definestructurestmt 
	| DELAY '(' expr ')'	 
	| DELETE deleteoptionlist
	| DETACH THREAD '(' varnameref ')'
	| displaystmt
	// | ELSE pl1stmt	{ $$ = ["else", $2]; }
	| ELSE block 	{ $$ = ["else", ...$2]; }
	| EXEC varname INCLUDE varname { $$ = [`pl.EXEC(${$2}).INCLUDE(${$4});`]; }
	| EXEC varname WHENEVER varname GO TO varname { $$ = [`pl.EXEC(${$2}).WHENEVER(${$4}).GOTO(${$5});`]; }
	| EXIT	
	| FETCH fetchoptioncommalist	
	| flushstmt	
	| FORMAT_STMT '(' editformatlist ')'
	| FREE freeoption	
	| getstmt		
	| gotostmt	
	// | IF expr THEN pl1stmt { $$ = ["if", "( " + $2 + " )", $4]; }
	| IF expr THEN block { $$ = ["if ( " + $2 + " )", ...$4]; }
	| iteratestmt	
	| leavestmt	
	| LOCATE varnameref locateoptionlist
	| onstmt	
	| OPEN opengrouplist
	// | OTHERWISE pl1stmt      { $$ = ["pl.otherwise(() =>", [$2], ");"]; }	
	| OTHERWISE block      { $$ = ["pl.otherwise(() =>", ...$2, ");"]; }	
	| putstmt		
	| READ readoptionlist
	| releasestmt	
	| returnstmt	  { $$ = [$1 + ';'];}
	| revertstmt	
	| rewritestmt	
	| SELECT '(' expr ')' pl1stmtlist END { $$ = ["pl.select(" + $3 + ", () => {", ...$5, "});"]; }	
	| signalstmt	
	| stopstmt	
	| waitstmt	
	// | WHEN '(' varnamedimensioncommalist ')' pl1stmt { $$ = ["pl.when(", $3 + ", () => ", [$5], ");"]; }	
	| WHEN '(' varnamedimensioncommalist ')' block { $$ = ["pl.when(" + $3 + ", () => ", ...$5, ");"]; }	
	| writestmt	
	| unlockstmt
	;

block
	: DO pl1stmtlist END { $$ = ["{", ...$2, "}"]; }
	| stmt               { $$ = Array.isArray($1) ? $1 : [$1]; }
	;

allocateoptionlist
	: allocateoption
	| allocateoptionlist ',' allocateoption { throw "TODO:allocateoptionlist"; }
	;

allocateoption
	: varnameref
	| varnameref IN  '(' varnameref ')' { throw "TODO:allocateoption1"; }
	| varnameref SET '(' varnameref ')' { $$ = $1 + ').SET(' + $4 + ')'; }
	| varnameref IN  '(' varnameref ')' SET '(' varnameref ')' { throw "TODO:allocateoption3"; }
	| varnameref SET '(' varnameref ')' IN  '(' varnameref ')' { throw "TODO:allocateoption4"; }
	| NUM varnameref                { throw "TODO:allocateoption5"; }
	| varnameref ctloptionlist      { $$ = $1 + $2; }
	| NUM varnameref ctloptionlist  { throw "TODO:allocateoption6"; }
	;

attachstmt
	: ATTACH varnameref
	| ATTACH varnameref THREAD '(' varnameref ')'
	| ATTACH varnameref THREAD '(' varnameref ')' ENVIRONMENT '(' ')'
	| ATTACH varnameref THREAD '(' varnameref ')' ENVIRONMENT '(' TSTACK '(' expr ')' ')'
	| ATTACH varnameref ENVIRONMENT '(' ')'
	| ATTACH varnameref ENVIRONMENT '(' TSTACK '(' expr ')' ')'
	| ATTACH varnameref ENVIRONMENT '(' ')' 	THREAD '(' varnameref ')'
	| ATTACH varnameref ENVIRONMENT '(' TSTACK '(' expr ')' ')' 	THREAD '(' varnameref ')'
	;

/* dimension is part of the varnameref */
ctloptionlist
	: ctlvarattribute
	| ctlvarattribute dclinit
	| dclinit
	;

ctlvarattribute
	: CHARACTER '(' expr ')'
	| BIT  '(' expr ')'
	| GRAPHIC '(' expr ')'
	| AREA '(' expr ')'
	;

beginstmt
	: BEGIN_                                      { $$ = '{';}
	| BEGIN_ OPTIONS '(' beginstmtoptionlist ')'  { $$ = '{ // OPTIONS:' + $3; }
	;

beginstmtoptionlist
	: beginstmtoption
	| beginstmtoptionlist beginstmtoption     { throw "TODO:beginstmtoptionlist"; }
	| beginstmtoptionlist ',' beginstmtoption { throw "TODO:beginstmtoptionlist"; }
	;
	
beginstmtoption
	: ORDER
	| REORDER
	| NOCHARGRAPHIC
	| CHARGRAPHIC
	| NOINLINE
	| INLINE
	| NON_QUICK
	| NO_QUICK_BLOCKS
	| SUPPORT
	;

// defaultstmt	
// 	: DEFAULT defaultitemcommalist 
// 	| DEFAULT NONE
// 	;

defineordinalstmt 
	: DEFINE ORDINAL varname '(' ordinalmembercommalist  ')'  { throw "TODO:defineordinalstmt"; }
	| DEFINE ORDINAL varname '(' ordinalmembercommalist  ')' ordinaloptionlist { throw "TODO:defineordinalstmt"; }
	;

definestructurestmt
	: DEFINE STRUCTURE dclstructurecommalist { throw "TODO:definestructurestmt"; }
	;

dclstructurecommalist
	: dclstructure
	| dclstructurecommalist ',' dclstructure  { throw "TODO:dclstructurecommalist"; }
	;
	
dclstructure	
	: NUM varname           { throw "TODO:dclstructure0"; }
	| NUM varname CELL      { throw "TODO:dclstructure"; }
	| NUM varname UNION     { throw "TODO:dclstructure"; }
	| NUM varname dclfactor { throw "TODO:dclstructure:dclfactor"; }
	| NUM '*'               { throw "TODO:dclstructure"; }
	| NUM '*' CELL          { throw "TODO:dclstructure"; }
	| NUM '*' UNION         { throw "TODO:dclstructure"; }
	| NUM '*' dclfactor     { throw "TODO:dclstructure"; }
	;

ordinalmembercommalist
	: ordinalmember
	| ordinalmembercommalist ',' ordinalmember { throw "TODO:ordinalmembercommalist"; }
	;

ordinalmember
	: varname
	| varname VALUE '(' NUM ')' { throw "TODO:ordinalmember"; }
	;

ordinaloptionlist
	: ordinaloption
	| ordinaloptionlist ordinaloption { throw "TODO:ordinaloptionlist"; }
	;

ordinaloption
	: PRECISION '(' NUM ')' { throw "TODO:ordinaloption"; }
	| SIGNED
	| UNSIGNED
	;

displaystmt
	: DISPLAY '(' expr ')'
	| DISPLAY '(' expr ')' REPLY '(' varnameref ')'
	| DISPLAY '(' expr ')' REPLY '(' varnameref ')' EVENT '(' varnameref ')'
	| DISPLAY '(' expr ')' EVENT '(' varnameref ')' REPLY '(' varnameref ')'
	;

endstmt
	// : END         { $$ = '}';}
	: END varname { $$ = '} //' + $2;}
	;

entrystmt
	: ENTRY_STMT entrygrouplist
	| ENTRY_STMT
	;

fetchoptioncommalist
	: fetchoption
	| fetchoptioncommalist ',' fetchoption
	;

fetchoption
	: varnameref
	| varnameref SET   '(' varnameref ')'
	| varnameref TITLE '(' expr ')'
	| varnameref SET   '(' varnameref ')' TITLE '(' expr ')'
	| varnameref TITLE '(' expr ')' SET   '(' varnameref ')'
	;

flushstmt
	: FLUSH FILE_ '(' varnameref ')'
	| FLUSH FILE_ '(' '*' ')'
	;

freeoption	
	: varnameref
	| varnameref IN '(' varnameref ')'
	| freeoption ',' varnameref
	| freeoption ',' varnameref IN '(' varnameref ')'
	;

getstmt
	: GET getoptionlist
	// | GET '(' varnamedimensioncommalist ')' /* check that varnamedimensioncommalist doesnt contain '*' */
	// | GET '(' varnamedimensioncommalist ')' getoptionlist
	;

gotostmt
	: GO TO varnameref { $$ = 'throw ' + $3 + ';'; }
	| GOTO varnameref  { $$ = 'throw ' + $2 + ';'; }
	;

iteratestmt
	: ITERATE
	| ITERATE varnameref
	;

leavestmt
	: LEAVE
	| LEAVE varnameref
	;

onstmt
	: ON onconditioncommalist SYSTEM       { throw "TODO:onstmt1"; }
	| ON onconditioncommalist SNAP SYSTEM  { throw "TODO:onstmt2"; }
	| ON onconditioncommalist SNAP pl1stmt { throw "TODO:onstmt3"; }
	| ON onconditioncommalist pl1stmt      { throw "TODO:onstmt4"; }
	;

packagestmt 
	: PACKAGE    
	| PACKAGE packagegrouplist
	;
	
packagegrouplist
	: packagegroup
	| packagegrouplist packagegroup
	;

packagegroup
	: EXPORTS '(' '*' ')'
	| EXPORTS '(' packagevarnamecommalist ')'
	| RESERVES '(' '*' ')'
	| RESERVES '(' varnamecommalist ')'
	| OPTIONS '(' ')'
	| OPTIONS '(' packageoptionlist ')'
	;

packagevarnamecommalist
	: packagevarname
	| packagevarnamecommalist ',' packagevarname
	;

packagevarname
	: varname
	| varname EXTERNAL '(' STR_CONSTANT ')'
	;

packageoptionlist
	: packageoption
	| packageoptionlist packageoption
	| packageoptionlist ',' packageoption
	;

packageoption
	: NOCHARGRAPHIC
	| CHARGRAPHIC
	| ORDER
	| REORDER
	| REENTRANT
	;

putstmt
	: PUT putoptionlist
	// | PUT '(' varnamedimensioncommalist ')' /* check that varnamedimensioncommalist doesnt contain '*' */
	// | PUT '(' varnamedimensioncommalist ')'  putoptionlist
	;

procedurestmt
	: PROCEDURE procgrouplist { $$ = $2 ; }
	| PROCEDURE               { $$ = '() {' ; }
	;

releasestmt	
	: RELEASE varnamecommalist
	| RELEASE '*'
	;

// resignalstmt:	RESIGNAL ;

returnstmt
	: RETURN { $$ = "return"}
	| RETURN '(' expr ')' { $$ = "return ("+ $3 +")"}
	;

rewritestmt: REWRITE rewriteoptionlist ;

revertstmt:	REVERT onconditioncommalist	;

signalstmt:	SIGNAL oncondition ;

stopstmt:	STOP ;

unlockstmt:	UNLOCK unlockoptionlist	;

waitstmt
	: WAIT THREAD '(' varnameref ')'
	// | WAIT '(' varnamedimensioncommalist ')' /*TODO: varnamedimensioncommalist has to be varnameref's */
	// | WAIT '(' varnamedimensioncommalist ')' '(' expr ')'
	;

writestmt:	WRITE writeoptionlist ;

readoptionlist
	: readoption
	| readoptionlist readoption
	;

rewriteoptionlist
	: rewriteoption
	| rewriteoptionlist rewriteoption
	;

writeoptionlist
	: writeoption
	| writeoptionlist writeoption
	;

deleteoptionlist
	: deleteoption
	| deleteoptionlist deleteoption
	;

unlockoptionlist
	: unlockoption
	| unlockoptionlist unlockoption
	;

locateoptionlist
	: locateoption
	| locateoptionlist locateoption
	;

calloptionlist
	: varnameref
	| varnameref callmultitaskoptionlist { throw "TODO: calloptionlist3"}
	;

callmultitaskoptionlist
	: callmultitaskoption
	| callmultitaskoptionlist callmultitaskoption { throw "TODO: callmultitaskoptionlist"}
	;

callmultitaskoption
	: TASK
	| TASK '(' varnameref ')'      { throw "TODO: callmultitaskoption1"}
	| EVENT '(' varnameref ')'     { throw "TODO: callmultitaskoption2"}
	| PRIORITY '(' varnameref ')'  { throw "TODO: callmultitaskoption3"}
	;

closegrouplist
	: closegroup
	| closegrouplist ',' closegroup { throw "TODO: callmultitaskoption3"}
	;

defaultitemcommalist
  : defaultitem
	| defaultitemcommalist ',' defaultitem
	;
	
defaultitem
  : defaultpredicateexpr
	| defaultpredicateexpr dcloptionlist
	| defaultpredicateexpr dcloptionlist VALUE '(' dcloptionlist ')'
	| '(' defaultitemcommalist ')'
	| '(' defaultitemcommalist ')' dcloptionlist
	| '(' defaultitemcommalist ')' dcloptionlist  VALUE '(' dcloptionlist ')'
	| '(' defaultitemcommalist ')' ERROR
	;

defaultrangespec
 : varname
	| varname ':' varname
	| defaultrangespec ',' varname
	| defaultrangespec ',' varname ':' varname
	;


defaultpredicateexpr
 : defaultpredicateexpr AND defaultpredicateexpr
	| defaultpredicateexpr OR  defaultpredicateexpr
	| NOT defaultpredicateexpr %prec NOT
	| '(' defaultpredicateexpr ')' 
	| RANGE '(' '*' ')'
	| RANGE '(' defaultrangespec ')'
	| DESCRIPTORS
 | dcloption
	;

procgrouplist
	: procgroup
	| procgrouplist procgroup { $$=$1 + $2; }
	;

entrygrouplist
	: entrygroup
	| entrygrouplist entrygroup { $$=$1 + $2; }
	;

iooption
	: FILE_ '(' varnameref ')' { throw "TODO: iooption1"}
	| EVENT '(' varnameref ')' { throw "TODO: iooption2"}
	;

readoption
	: INTO   '(' varnameref ')' { throw "TODO: readoption0"}
	| SET    '(' varnameref ')' { throw "TODO: readoption1"}
	| IGNORE '(' expr ')'       { throw "TODO: readoption2"}
	| KEY    '(' expr ')'       { throw "TODO: readoption3"}
	| KEYTO  '(' varnameref ')' { throw "TODO: readoption4"}
	| NOLOCK
	| iooption
	;

writeoption
	: FROM    '(' varnameref ')' { throw "TODO: writeoption1"}
	| KEYFROM '(' expr ')'       { throw "TODO: writeoption2"}
	| KEYTO   '(' varnameref ')' { throw "TODO: writeoption3"}
	| iooption
	;

rewriteoption
	: FROM  '(' varnameref ')' { throw "TODO: rewriteoption1"}
	| KEY '(' expr ')'         { throw "TODO: rewriteoption2"}
	| iooption
	;

deleteoption
	: KEY '(' expr ')'  { throw "TODO: deleteoption"}
	| iooption
	;

unlockoption
	: FILE_ '(' varnameref ')' { throw "TODO: unlockoption1"}
	| KEY   '(' expr ')'       { throw "TODO: unlockoption2"}
	;

locateoption
	: FILE_ '(' varnameref ')' { throw "TODO: locateoption1"}
	| SET   '(' varnameref ')' { throw "TODO: locateoption2"}
	| KEYFROM '(' expr ')'     { throw "TODO: locateoption3"}
	;

opengrouplist
	: opengroup
	| opengrouplist ',' opengroup { throw "TODO: opengrouplist"}
	;

opengroup
	: FILE_ '(' varnameref ')' openoptionlist { throw "TODO: opengroup1"}
	| FILE_ '(' varnameref ')'                { throw "TODO: opengroup2"}
	;

openoptionlist
	: openoption
	| openoptionlist openoption { throw "TODO: openoptionlist"}
	;

openoption
	: STREAM
	| RECORD
	| INPUT
	| OUTPUT
	| UPDATE
	| DIRECT
	| SEQUENTIAL
	| TRANSIENT
	| BUFFERED
	| UNBUFFERED
	| BACKWARDS
	| EXCLUSIVE
	| KEYED
	| PRINT
	| TITLE '(' expr ')'    { throw "TODO: openoption1"}
	| LINESIZE '(' expr ')' { throw "TODO: openoption2"}
	| PAGESIZE '(' expr ')' { throw "TODO: openoption3"}
	| ENVIRONMENT '(' environmentspeclist ')' { throw "TODO: openoption4"}
	;

closegroup
	: FILE_ '(' varnameref ')' ENVIRONMENT '(' LEAVE ')'  { throw "TODO: closegroup1"}
	| FILE_ '(' varnameref ')' ENVIRONMENT '(' REREAD ')' { throw "TODO: closegroup2"}
	| FILE_ '(' varnameref ')'                            { throw "TODO: closegroup3"}
	;


putoptionlist
	: putoption
	| putoptionlist putoption { throw "TODO: putoptionlist"}
	;

putoption
	: FILE_ '(' varnameref ')'
	| PAGE
	| SKIP
	| SKIP '(' expr ')'
	| LINE '(' expr ')'
	| STRING '(' varnameref ')'
	| dataspecification
	;

entrygroup
	: '(' varnamecommalist ')'            { throw "TODO: entrygroup"}
	| RETURNS '(' entryparmcommalist ')'  { throw "TODO: entrygroup"}
	| REDUCIBLE                           { throw "TODO: entrygroup"}
	| IRREDUCIBLE                         { throw "TODO: entrygroup"}
	| OPTIONS '(' procoptionlist ')'      { throw "TODO: entrygroup"}
	| '(' ')'                             { throw "TODO: entrygroup"}
	;

procgroup
	: '(' varnamecommalist ')'           { $$ = '(' + $2 + ') {'; }
	| RETURNS '(' entryparmcommalist ')' { $$ = " //" + $1 + '(' + $3 + ')' ;}
	| OPTIONS '(' procoptionlist ')'     { $$ = " //" + $1 + '(' + $3 + ')' ;}
	| REDUCIBLE                          { $$ = ' //' + $1; }
	| IRREDUCIBLE                        { $$ = ' //' + $1; }
	| RECURSIVE	                         { $$ = ' //' + $1; }
	| ORDER		                         { $$ = ' //' + $1; }
	| REORDER		                     { $$ = ' //' + $1; }
	| CHARGRAPHIC                        { $$ = ' //' + $1; }
	| NOCHARGRAPHIC	                     { $$ = ' //' + $1; }
	| '(' ')'	                         { $$ = '() {'; }
	;

procoptionlist
	: procoption
	| procoptionlist procoption     { $$ = $1 + $2; }
	| procoptionlist ',' procoption { throw "TODO: procoptionlist"}
	;

procoption
	: MAIN
	| REENTRANT
	| NOEXECOPS
	| TASK 
	| VARIABLE
	| NON_QUICK
	| NO_QUICK_BLOCKS
	| PACKED_DECIMAL
	| SEPARATE_STATIC
	| SUPPORT
	| RENAME '(' renamepaircommalist ')' { throw "TODO: procoption"; }
	| VALIDATE
	| VALIDATE '(' varname ')'           { throw "TODO: procoption"; }
	;

renamepaircommalist
	: renamepair
	| renamepaircommalist ',' renamepair { throw "TODO: renamepaircommalist"; }
	;
	
renamepair: '(' varname ',' varname ')' ;

getoptionlist
	: getoption
	| getoptionlist getoption
	;

getoption
	: FILE_ '(' varnameref ')'
	| COPY  '(' varnameref ')'
	| PAGE
	| SKIP
	| SKIP   '(' expr ')'
	| STRING '(' expr ')'
	| dataspecification
	;

dataspecification
	: LIST listdataspec
	| DATA datadataspec
	| DATA
	| EDIT editdataspec
	;

listdataspec:	'(' datalist ')' ;

datadataspec:	'(' datalist ')' ;

editdataspec
	: '(' datalist ')' '(' editformatlist ')'
	| editdataspec '(' datalist ')' '(' editformatlist ')'
	;

editformatexpr
  // : A
	// | A '(' expr ')'
	// | B
	// | B '(' expr ')'
	// | C '(' realformatexpr ')'
	// | C '(' realformatexpr ',' realformatexpr ')'
	: realformatexpr
	// | G
	// | G '(' expr ')'
	// | P STR_CONSTANT
	// | R '(' varnameref ')'
	// | X '(' expr ')'
	| LINE '(' expr ')'
	| COLUMN '(' expr ')'
	| PAGE
	| SKIP
	| SKIP '(' expr ')'
	;

realformatexpr
	: E '(' expr ')'
	| E '(' expr ',' expr ')'
	| E '(' expr ',' expr ',' expr ')'
	| F '(' expr ')'
	| F '(' expr ',' expr ')'
	| F '(' expr ',' expr ',' expr ')'
	;


editformatitem
	: editformatexpr
	| NUM editformatexpr
	| '(' NUM ')' editformatexpr
	;

editformatlist
	: editformatitem
	| NUM '(' editformatlist ')'
	| '(' NUM ')' '(' editformatlist ')'
	| editformatlist ',' editformatitem
	| editformatlist ',' NUM '(' editformatlist ')'
	| editformatlist ',' '(' NUM ')' '(' editformatlist ')'
	;

datalist
	: expr //expr-list
	| '(' datalist do_type_3 ')'
	| datalist ',' '(' datalist do_type_3 ')'
	;

dostmt
	: DO            { $$ = "{"; }
	// | DO do_spec_2  { $$ = $2; }
	| DO do_spec_2 pl1stmtlist END { $$ = [$2, ...$3, "}"]; } // TODO: where is ';' ?
	| do_type_3     { throw "TODO: dostmt2"; }
	;

do_spec_2
	: WHILE '(' expr ')' { $$ = "while(" + $3 + ") {"; }
	| WHILE '(' expr ')' UNTIL '(' expr ')'
	| UNTIL '(' expr ')'
	| UNTIL '(' expr ')' WHILE '(' expr ')'
	| LOOP
	| FOREVER
	;

do_type_3:	DO varnameref '=' do_spec_3list ;

do_spec_3list
	: do_spec_3
	| do_spec_3 do_spec_2
	| do_spec_3list ',' do_spec_3
	| do_spec_3list ',' do_spec_3 do_spec_2
	;

do_spec_3
	: expr
	| expr do_spec_3_exprlist
	;

do_spec_3_exprlist
	: do_spec_3_expr
	| do_spec_3_exprlist do_spec_3_expr
	;

do_spec_3_expr
	: TO expr
	| BY expr
	| REPEAT expr
	| UPTHRU expr
	| DOWNTHRU expr
	;

assignstmt
	: varnameref '=' expr             { $$ = $1 + '.v = ' + $3 + ';'; }
	| varnameref '=' expr ',' BY NAME { $$ = $1 + '.v = ' + $3 + ';'; }
	| varnameref SELFOP expr          { $$ = $1 + '.v ' + $2 + $3 + ';'; }
	// | IF '(' expr ')' '=' expr
	// | IF '(' expr ')' '=' expr ',' BY NAME
	;

// assignstmt
//   : varnamerefcommalist SELFOP expr
// 	| varnamerefcommalist SELFOP expr ',' BY NAME
// 	| IF '(' expr ')' SELFOP expr
// 	| IF '(' expr ')' SELFOP expr ',' BY NAME
// 	;

expr
	: unary_expression
	| expr infix_operator unary_expression { $$ = $1 + $2 + $3; }
	;

unary_expression
	: elementary_expression 
	| prefix_operator elementary_expression { $$ = $1 + $2; }
	;

infix_operator
	: POWER
	| '+'  
	| '-'  
	| '*'  
	| '/'  
	| AND    { $$ = ' && '; }
	| OR     { $$ = ' || '; }
	| CONCAT { $$ = ' + '; }
	| '='    { $$ = ' == '; }
	| '<'
	| '>'
	| LE 
	| GE 
	| NE     { $$ = " != "; }
	;

prefix_operator
	: NOT { $$ = '!'; }
  // | '-' 
  // | '+'
	;

elementary_expression
	: '(' expr ')' { $$ = '(' + $2 + ')'; }
	| '(' expr ')' STR_CONSTANT { $$ = $4 + ".repeat(" + $2 + ")"; }
	| varnameref 
	| constant   
	;

constant
	: STR_CONSTANT 
	| NUM          
	| BINCONST     
	;

varnamerefcommalist
	: varnameref
	| varnamerefcommalist ',' varnameref { throw "TODO:varnamerefcommalist" }
	;

varnameref
	: varnamequal
	| varnameref '.' varnamequal       { $$ = $1 + $2 + $3; }
	| varnameref PTR varnamequal       { throw "TODO:varnameref3" }
	| varnameref HANDLEPTR varnamequal { throw "TODO:varnameref4" }
	;

/* note: the '(' varnamedimensioncommalist ')' following varname, can be either indexes to array or parameter list for procedure */
varnamequal
	: varname
	| varname '(' ')' { $$ = $1 + '()'; }
	| varname '(' varnamedimensioncommalist ')' { $$ = $1 + '(' + $3 + ')'; }
	| varname '(' varnamedimensioncommalist ')' '(' ')' {throw "TODO:varnamequal3"}
	| varname '(' varnamedimensioncommalist ')' '(' varnamedimensioncommalist ')' {throw "TODO:varnamequal4"}
	;

/* Factoring out keywords  */
// varnamequal //TODO: double rule
//	: DELAY '(' '*'  ',' varnamedimensioncommalist ')'
// 	| DELAY '(' '*'  ')'
// 	| DELAY '(' expr ',' varnamedimensioncommalist ')'
// 	| DELAY '(' expr ')'
// 	| DISPLAY '(' '*' ',' varnamedimensioncommalist ')'
// 	| DISPLAY '(' '*' ')'
// 	| DISPLAY '(' expr ',' varnamedimensioncommalist ')'
// 	| DISPLAY '(' expr  ')'
// 	| GET '(' varnamedimensioncommalist ')'
// 	| IF '(' '*'  ',' varnamedimensioncommalist ')'
// 	| IF '(' '*'  ')'
// 	| IF '(' expr ',' varnamedimensioncommalist ')'
// 	| IF '(' expr ')' 
// 	| PUT '(' varnamedimensioncommalist ')'
/*	| OTHERWISE '(' varnamedimensioncommalist ')' */
// | RETURN '(' '*'  ',' varnamedimensioncommalist ')'
// | RETURN '(' '*'  ')'
// | RETURN '(' expr ',' varnamedimensioncommalist ')'
// | RETURN '(' expr ')'
// | SELECT '(' '*'  ',' varnamedimensioncommalist ')'
// | SELECT '(' '*'  ')'
// | SELECT '(' expr ',' varnamedimensioncommalist ')'
// | SELECT '(' expr ')'
// | UNTIL '(' '*'  ')'
// | UNTIL '(' '*'  ',' varnamedimensioncommalist ')'
// | UNTIL '(' expr ',' varnamedimensioncommalist ')'
// | UNTIL '(' expr ')'
// | WHEN '(' varnamedimensioncommalist ')'
// | WAIT '(' varnamedimensioncommalist ')'
// | WHILE '(' '*'  ')'
// | WHILE '(' '*'  ',' varnamedimensioncommalist ')'
// | WHILE '(' expr ',' varnamedimensioncommalist ')'
// | WHILE '(' expr ')'
// ;

varnamedimensioncommalist
	: varnamedimension
	| varnamedimensioncommalist ',' varnamedimension { $$ = $1 + ', ' + $3; }
	| varnamedimensioncommalist ':' varnamedimension { $$ = $1 + ': ' + $3; }
	;

varnamedimension
	: expr
	// | '*'
	| NUM SUB
	;

varnamecommalist
	: varname                       
	| varnamecommalist ',' varname  { $$ = $1 + ', ' + $3; }
	;


varname
	: VARNAME { $$ = substituteM($1); }
	| varname_kw         
	| varname_kwpp       
	| varname_conditions 
	;

varname_kwpp
	: ACTIVATE
	| DEACTIVATE
	| INCLUDE
	| NOPRINT
	| NOTE
	| PAGE
	| REPLACE
	;

varname_conditions
	: ANYCONDITION
	| AREA
	| ATTENTION
	| CHECK
	| CONDITION
	| CONVERSION
	| ENDFILE
	| ENDPAGE
	| ERROR
	| FINISH
	| FIXEDOVERFLOW
	| INVALIDOP
	| KEY
	| NAME
	| OVERFLOW_
	| PENDING
	| RECORD
	// | SIZE
	| STORAGE
	| STRINGRANGE
	| STRINGSIZE
	| SUBSCRIPTRANGE
	| TRANSMIT
	| UNDERFLOW_
	| ZERODIVIDE
	;

/* Note: The varnames that are commented out, 
 *       are resolved in the scanner 
 *       and returned as token VARNAME 
 */         
varname_kw
	: A
	| ABNORMAL
	// | ADDBUFF
	// | ALIAS
	// | ALIGNED
	// | ALLOCATE
	// | ASCII
	// | ASSIGNABLE
	// | ASSEMBLER
	// | ATTACH
	// | AUTOMATIC
	| B
    // | B1
	// | B2
	// | B3
	// | B4
	// | BACKWARDS
	// | BASED
	// | BEGIN_
	// | BIGENDIAN
	// | BINARY
	// | BIT
	// | BKWD
	// | BLKSIZE
	// | BUFFERED
	// | BUFFERS
	// | BUFFOFF
	// | BUFND
	// | BUFNI
	// | BUFSP
	// | BUILTIN
	// | BY
	// | BYADDR
	// | BYVALUE
	// | BX
	| C
	// | CALL
	// | CELL
	// | CDECL
	// | CHARACTER
	// | CHARGRAPHIC
	// | CLOSE
	// | COBOL
	// | COLUMN
	// | COMPLEX
	// | CONNECTED
	// | CONSECUTIVE
	// | CONSTANT
	// | CONTROLLED
	// | COPY
	// | CTLASA
	// | CTL360
	// | D
	| DATA
	// | DATE
	// | DB
  	// | DECLARE 
	// | DECIMAL
	// | DEFINE
	// | DEFINED
 	// | DEFAULT 
	// | DELAY
	// | DELETE
	// | DESCRIPTOR
	// | DESCRIPTORS
	// | DETACH
	// | DIMENSION
	// | DISPLAY
	// | DIRECT
	// | DO
	// | DOWNTHRU
	// | E
	// | EDIT
	// | ELSE
	// | END
	// | ENTRY     /* note the statement variant is returned as ENTRY_STMT */
	// | ENVIRONMENT
	// | EVENT
	// | EXCLUSIVE
	// | EXPORTS
	// | EXTERNAL
	// | EXIT
	// | F
	// | FB
	// | FS
	// | FBS
	// | FETCH
	// | FETCHABLE
	// | FILE_
	// | FIXED
	// | FLOAT
	// | FLUSH
	// | FOREVER
	// | FORMAT    /* note the statement variant is returned as FORMAT_STMT */
	// | FORTRAN
	// | FREE
	// | FROM
	// | FROMALIEN
	// | G
	// | GENERIC
	// | GENKEY
	// | GET
	// | GO
	// | GOTO
	// | GRAPHIC
	// | GX
	// | HANDLE
	// | HEXADEC
	// | I
	// | IEEE
	// | IF
	// | IGNORE
	// | IMPORTED
	// | IN
	// | INDEXAREA
	// | INDEXED
	// | INITIAL_
	// | INLINE
	| INPUT
	// | INTER
	// | INTERACTIVE
	// | INTERNAL
	// | INTO
	// | IRREDUCIBLE
	// | ITERATE
	// | KEYED
	// | KEYLENGTH
	// | KEYLOC
	// | KEYTO
	// | KEYFROM
	// | LABEL
	// | LEAVE
	// | LIKE
	// | LIMITED
	| LINE
	// | LINESIZE
	// | LINKAGE
	// | LIST
	// | LITTLEENDIAN
	// | LOCAL
	// | LOCATE
	// | LOOP
	// | M
	// | MAIN
	// | NCP
	// | NOCHARGRAPHIC
	// | NOCHECK
	// | NOCONVERSION
	// | NODESCRIPTOR
	// | NOEXECOPS
	// | NOFIXEDOVERFLOW
	// | NOINIT
	// | NOINLINE
	// | NOINVALIDOP
	// | NOLOCK
	// | NONASSIGNABLE
	// | NONCONNECTED
	// | NONE
	// | NONVARYING
	// | NON_QUICK
	// | NO_QUICK_BLOCKS
	// | NOOVERFLOW
	// | NORMAL
	// | NOSIZE
	// | NOSUBSCRIPTRANGE
	// | NOSTRINGRANGE
	// | NOSTRINGSIZE
	// | NOUNDERFLOW
	// | NOWRITE
	// | NOZERODIVIDE
	// | OFFSET
	// | ON
	// | OPEN
	// | OPTIONAL
	// | OPTIONS
	// | OPTLINK
	// | ORDER
	// | ORDINAL
  	// | OTHERWISE
	| OUTPUT
	// | P
	// | PACKAGE
	// | PACKED
	// | PACKED_DECIMAL
	// | PAGE
	// | PAGESIZE
	// | PARAMETER
	// | PASSWORD
	// | PICTURE
	| POINTER
	// | POSITION
	// | PRECISION
	// | PRINT
	// | PRIORITY
  	// | PROCEDURE 
	// | PUT
	// | R
	// | RANGE
	// | REAL
	// | READ
	// | RECSIZE
	// | RECURSIVE
	// | REENTRANT
	// | REDUCIBLE
	// | REFER
	// | REGIONAL
	// | RELEASE
	// | RENAME
	// | REORDER
	// | REPEAT
	// | REPLY
	// | REREAD
	// | RESERVED
	// | RESERVES
	// | RESIGNAL
	// | RETCODE
	// | RETURN
	// | RETURNS
	// | REUSE
	// | REVERT
	// | REWRITE
	// | SCALARVARYING
	// | SELECT
	// | SEPARATE_STATIC
	// | SEQUENTIAL
	// | SET
	// | SIGNAL
	// | SIGNED
	// | SIS
	// | SKIP
  	// | SNAP
	// | STATIC
	// | STDCALL
	// | STOP
	// | STREAM
	// | STRING
	// | STRINGVALUE
	// | STRUCTURE
	// | SUB
	// | SUPPORT
	// | SYSTEM
	// | TASK
	// | THEN
	// | THREAD
	| TITLE
	| TO
	| TOTAL
	| TP
	| TRANSIENT
	| TRKOFL
	| TSTACK
	| TYPE
	// | U
	| UNALIGNED
	| UNBUFFERED
	| UNCONNECTED
	| UNDEFINEDFILE
	| UNION
	| UNLOCK
	| UNSIGNED
	// | UNTIL
	| UPDATE
	| UPTHRU
	// | V
	| VALIDATE
	| VALUE
	| VARIABLE
	| VARYING
	| VARYINGZ
	| VB
	| VBS
	| VS
	| VSAM
	| WAIT
	// | WHEN
	// | WHILE
	| WIDECHAR
	| WINMAIN
	| WRITE
	| WX
	// | X
	| XN
	| XU
	;

onconditioncommalist
	: oncondition
	| onconditioncommalist ',' oncondition
	;
	
oncondition
	: AREA
	| ATTENTION
	| ANYCONDITION
	| CHECK
	| CHECK '(' varnamerefcommalist ')'
	| CONDITION '(' varnameref ')'
	| CONVERSION
	| ENDFILE '(' varnameref ')'
	| ENDPAGE '(' varnameref ')'
	| ERROR
	| FINISH
	| FIXEDOVERFLOW
	| INVALIDOP
	| KEY '(' varnameref ')'
	| NAME '(' varnameref ')'
	| OVERFLOW_
	| PENDING '(' varnameref ')'
	| RECORD '(' varnameref ')'
	// | SIZE
	| STORAGE
	| STRINGRANGE
	| STRINGSIZE
	| SUBSCRIPTRANGE
	| TRANSMIT '(' varnameref ')'
	| UNDEFINEDFILE '(' varnameref ')'
	| UNDERFLOW_
	| ZERODIVIDE
	| VARNAME
	| varname_kw
	;

precondition
  : CHECK
	| CHECK '(' varnamerefcommalist ')'
	| CONVERSION
	| FIXEDOVERFLOW
	| INVALIDOP
	| OVERFLOW_
	// | SIZE
	| STRINGRANGE
	| STRINGSIZE
	| SUBSCRIPTRANGE
	| UNDERFLOW_
	| ZERODIVIDE
	| NOCHECK
	| NOCONVERSION
	| NOFIXEDOVERFLOW
	| NOINVALIDOP
	| NOOVERFLOW
	| NOSIZE
	| NOSUBSCRIPTRANGE
	| NOSTRINGRANGE
	| NOSTRINGSIZE
	| NOUNDERFLOW
	| NOZERODIVIDE
	;

dclstmt
	: DECLARE dcltermcommalist { $$ = [String(new pl.DclStmt($2))]; } 
	| DECLARE                  { throw "TODO: DECLARE"; }
	;

dcltermcommalist
	: dclterm                          { $$ = [$1]; }  
	| dclnamebase                      { $$ = [$1]; }  
	| dcltermcommalist ',' dclterm     { $$ = [...$1, $3]; } //{ $$ = $1.concat($3); }
	| dcltermcommalist ',' dclnamebase { $$ = [...$1, $3]; } //{ $$ = $1.concat($3); }
	;

dclterm
	: '(' dcltermcommalist ')'               { $$ = { dclist:$2, factor: [] }; }
	| '(' dcltermcommalist ')' dclfactor     { $$ = { dclist:$2, factor: $4 }; }
	| NUM '(' dcltermcommalist ')'           { $$ = { NUM:Number($1), dclist:$3, factor: [] }; }
	| NUM '(' dcltermcommalist ')' dclfactor { $$ = { NUM:Number($1), dclist:$3, factor: $5 }; }
	| NUM '(' dcltermcommalist ')' CELL  
	| NUM '(' dcltermcommalist ')' UNION 
	;

dclnamebase
	: varname               //{ $$ = [$1]; }
	| varname dclfactor     { $$ = $1 + " = " + $2; }
	| NUM varname           { $$ = {NUM:Number($1), dclist:[$2], factor: []}; }
	| NUM varname dclfactor { $$ = {NUM:Number($1), dclist:[$2], factor: $3}; }
	| NUM varname CELL      { throw "TODO:dclnamebase3"; }
	| NUM varname UNION     { throw "TODO:dclnamebase4"; }
	| NUM '*'               { throw "TODO:dclnamebase5"; }
	| NUM '*' dclfactor     { $$ = {NUM:Number($1), dclist:[String(Math.random())], factor: $3}; }
	| NUM '*' CELL          { throw "TODO:dclnamebase7"; }
	| NUM '*' UNION         { throw "TODO:dclnamebase8"; }
	;
	
dclfactor
	: '(' dclarrayboundcommalist ')'                { $$ = [...$2]; }
	| '(' dclarrayboundcommalist ')' dcloptionlist  { $$ = [...$2, $4]; }
	| dcloptionlist                                
	;

dclarrayboundcommalist
	: dclarraybound                            { $$ = [Number($1)]; }
	// | dclarraybound ':' dclarraybound          { $$ = [$1 + $2 + $3]; }
	| dclarraybound ':' dclarraybound          { $$ = [[$1, $3]]; }
	| dclarrayboundcommalist ',' dclarraybound { $$ = [...$1, Number($3)]; }
	| dclarrayboundcommalist ',' dclarraybound ':' dclarraybound { throw "TODO:dclfactor3"; }
	;

dclarraybound
	: expr
	| expr REFER '(' varnameref ')'
	// | '*'
	;

dcloptionlist
	: dcloption
	| dcloptionlist dcloption { $$ = $1 + $2; }
	;

dcloption
	: dclnumeric
	| dclio     
	| dclchar   
	| dclstg    
	| dclpgm    
	| dclmisc   
	| dclinit   
	;

dclnumeric
	// : FIXED
	: FIXED '(' dclnumericNUM ')'                     { $$ = "new pl.FIXED_BIN(" + $3 + ")"; } 
    | FIXED BINARY '(' dclnumericNUM ')'              { $$ = "new pl.FIXED_BIN(" + $4 + ")"; } 
	| BINARY FIXED '(' dclnumericNUM ')'              { $$ = "new pl.FIXED_BIN(" + $4 + ")"; } 
	| FIXED '(' dclnumericNUM ',' dclnumericNUM ')'   { $$ = "new pl.FIXED_BIN(" + $3 + ', ' + $5 + ")"; } 
	| FLOAT                       
	| FLOAT '(' dclnumericNUM ')'                     { $$ = "new pl.FLOAT_BIN(" + $3 + ")"; }
	| DECIMAL                                         { $$ = "new pl.FIXED_DEC()"; } 
	| DECIMAL '(' dclnumericNUM ')'                   { $$ = "new pl.FIXED_DEC(" + $3 + ")"; } 
	| DECIMAL '(' dclnumericNUM ',' dclnumericNUM ')' { $$ = "new pl.FIXED_DEC(" + $3 + ", " + $5 + ")"; } 
	// | BINARY
	// | BINARY '(' dclnumericNUM ')' 
	// | BINARY '(' dclnumericNUM ',' dclnumericNUM ')'
	| REAL
	| COMPLEX 
	| COMPLEX '(' dclnumericNUM ')'
	| PRECISION /* used by defaultpredicateexpr */
	| PRECISION '(' dclnumericNUM ')'
	| PRECISION '(' dclnumericNUM ':' dclnumericNUM ')' /* used as range selector in genericwhenoption */
	| PRECISION '(' dclnumericNUM ',' dclnumericNUM ')'	
	;

dclnumericNUM
	: NUM 
	| '-' NUM 
	| '+' NUM 
	;

dclio
	: BACKWARDS
	| BUFFERED
	| DIRECT
	| ENVIRONMENT '(' environmentspeclist ')'
	| EXCLUSIVE
	| FILE_
	| INPUT
	| KEYED
	| LINESIZE '(' expr ')'
	| OUTPUT
	| PAGESIZE '(' expr ')'
	| PRINT
	| RECORD
	| SEQUENTIAL
	| STREAM
	| TITLE '(' expr ')'
	| TRANSIENT
	| UNBUFFERED
	| UPDATE
	;

dclchar
	: AREA charspec 
	| BIT charspec       { $$ = "new pl.BIT" + $2; } 
	| CHARACTER charspec { $$ = "new pl.CHAR" + $2; } 
	| GRAPHIC charspec 
	| G charspec
	| PICTURE STR_CONSTANT
	| WIDECHAR charspec
	| DATE 
	| DATE '(' STR_CONSTANT ')' 
	;

dclstg
	: ALIGNED
	| AUTOMATIC 
	| BASED 
	| BASED '(' varnameref ')'  { $$ = ".BASED(" + $3 + ")"; }
	| BYADDR
	| BYVALUE
	| CONNECTED
	| CONSTANT
	| CONTROLLED
	| DEFINED varnameref 
	| DEFINED '(' varnameref ')' 
	| DIMENSION '(' dclarrayboundcommalist ')'
	| EXTERNAL                  { $$ = ".EXTERNAL()"; }
	| EXTERNAL '(' STR_CONSTANT ')' 
	| INTERNAL 
	| LIKE varnameref
	| LOCAL
	| NONCONNECTED
	| STATIC                    { $$ = ".STATIC()"; }
	| OFFSET
	| OFFSET '(' varnameref ')'
	| OPTIONAL
	| PARAMETER 
	| POSITION '(' expr ')'
	| RESERVED
	| RESERVED '(' IMPORTED ')'
	| UNALIGNED
	| UNCONNECTED
	| STRUCTURE
	; 

dclpgm
	: ENTRY
	| ENTRY '(' entryparmcommalist ')'
	| RETURNS '(' entryparmcommalist ')' /* only when a structure is commalist ok */
	| LABEL
	| CONDITION
	| GENERIC '(' genericspeccommalist ')'
	| TASK
	| LIMITED
	| FROMALIEN
	| FETCHABLE
	| CDECL
	| OPTLINK
	| STDCALL
	| WINMAIN
	| FORTRAN
	| DESCRIPTOR
	| NODESCRIPTOR
	| LINKAGE '(' STR_CONSTANT ')'
	| REDUCIBLE  
	| IRREDUCIBLE 
	;

dclmisc
	: VARIABLE
	| VARYING { $$ = ".VARYING()"; }
	| NONVARYING
	| VARYINGZ
	| SYSTEM
	| BUILTIN  { $$ = ".BUILTIN()"; }
	| POINTER  { $$ = "new pl.PTR()"; } 
	| ABNORMAL
	| NORMAL
	| ASSIGNABLE
	| NONASSIGNABLE
	| HEXADEC
	| IEEE
	| BIGENDIAN
	| LIST
	| LITTLEENDIAN
	| SIGNED
	| UNSIGNED
	| NOINIT
	| HANDLE varnameref
	| HANDLE '(' varnameref ')'
	| TYPE varnameref
	| TYPE '(' varnameref ')'
	| ORDINAL varnameref
	| OPTIONS '(' entryoptionlist ')'
	| FORMAT
	;

environmentspeclist
	: environmentspec
	| environmentspeclist environmentspec
	| environmentspeclist ',' environmentspec
	;
	
environmentspec
	: F	
	| F '(' NUM ')'
	| FB
	| FS
	| FBS
	| V
	| VB
	| VS
	| VBS
	| ADDBUFF
	| ASCII
	| BKWD
	| BLKSIZE '(' environmentspecparm ')'
	| BUFFERS '(' environmentspecparm ')'
	| BUFFOFF '(' environmentspecparm ')'
	| BUFND '(' environmentspecparm ')'
	| BUFNI '(' environmentspecparm ')'
	| BUFSP '(' environmentspecparm ')'
	| COBOL
	| CONSECUTIVE
	| CTLASA
	| CTL360
	| D
	| DB
	| GENKEY
	| INDEXAREA '(' environmentspecparm ')'
	| INDEXED
	| INTERACTIVE
	| KEYLENGTH '(' environmentspecparm ')'
	| KEYLOC '(' environmentspecparm ')'
	| LEAVE
	| NCP '(' environmentspecparm ')'
	| NOWRITE
	| RECSIZE '(' environmentspecparm ')'
	| REGIONAL '(' environmentspecparm ')'
	| REREAD
	| REUSE
	| PASSWORD
	| SCALARVARYING
	| SIS
	| SKIP
	| STRINGVALUE
	| TOTAL
	| TP '(' M ')'
	| TP '(' R ')'
	| TRKOFL
	| U
	| VSAM
	;
	
environmentspecparm
	: NUM
	| VARNAME
	;

entryparmcommalist
	: entryparm
	| entryparmcommalist ',' entryparm
	;

entryparm
	: /* empty */
	| dcloptionlist
	| '*'
	| '*' dcloptionlist
	| NUM dcloptionlist
	| NUM
	| '(' entryarrayspeccommalist ')' dcloptionlist
	| NUM '(' entryarrayspeccommalist ')' dcloptionlist
	| NUM '(' entryarrayspeccommalist ')' 
	;
	
entryarrayspec
	: '*'
	| NUM
	| NUM ':' NUM
	| NUM ':' '*'
	| '*' ':' NUM
	| '*' ':' '*'
	;
	
entryarrayspeccommalist
	: entryarrayspec
	| entryarrayspeccommalist ',' entryarrayspec
	;
	
entryoptionlist
	: entryoption
	| entryoptionlist entryoption
	| entryoptionlist ',' entryoption
	;

entryoption
	: ASSEMBLER
	| COBOL
	| FORTRAN
	| INTER
	| RETCODE
	| CONSTANT
	| VARIABLE
	| PACKED
	| SUPPORT
	;

genericspeccommalist
	: genericspec
	| genericspeccommalist ',' genericspec
	;

genericspec: varname WHEN '(' genericwhenoptionlist ')'	;

genericwhenoptionlist
	: genericwhenoption
	| genericwhenoptionlist ',' genericwhenoption
	;

genericwhenoption
	: /* EMPTY */
	| '*'
	| dcloptionlist
	;

charspec
	: /* EMPTY */
	| '(' '*' ')'  { $$ = '()'; }
	| '(' expr ')' { $$ = '(' + $2 + ')'; }
	| '(' expr REFER '(' varnameref ')' ')'
	;

dclinit
	: INITIAL_ '(' expr ')' { $$ = '.INIT(' + $3 + ')'; }
	| INITIAL_ CALL varnameref
	| INITIAL_ TO '(' initialtospec ')' '(' expr ')'
	;
	
initialtospec
	: VARYING
	| VARYINGZ
	| NONVARYING
	;


%%