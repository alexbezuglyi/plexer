const jison = require('jison');
const fs = require("fs");

const pl2js  = require("./pl2.js");
const ims    = require("./ims");


class Plexer {
    constructor() {
        this.bnf = fs.readFileSync("pli.jison", "utf8");
        this.parser = new jison.Parser(this.bnf);
    }

    preproc(src) {
        let ret = "";
        try {
            //cut off MF's markup
            for (const line of src.split('\n')) {
                ret += line.slice(1, 72) + "\n";
            }
            //remove PROCESS 
            ret = ret.replace(/^PROCESS.+;/g, '').trim();
            //remove comments
            ret = ret.replace(/\/\*[\s\S]*?\*\/|\/\/.*/g, '').trim();

            //substitute all includes
            ret = ret.replace(/%INCLUDE\s+([A-Z0-9,\s+]+);/ig, (_, val1) => {
                let include = "";
                const includies = val1.split(",");
                for (const incfile of includies) {
                    include += this.preproc(fs.readFileSync("./transactions/" + incfile.trim() + ".pli", "utf-8"));
                }
                return include;
            });
        } catch (error) {
            console.error(error);
        }
        return ret;
    }

    parse(src) {
        return this.parser.parse(src);
    }

    preprocFile(path) {
        var f = fs.readFileSync(path, "utf8");
        return this.preproc(f);
    }
};

class DclStmt {
    constructor(ast) {
        this.tree = ast;
        this.cmntFactor = [".EXTERNAL()", ".BUILTIN()"];
    }

    /**
     * @params ast: 
     * declare ((A,B) fixed(10), C float(5)) external;
     * or
     * {
        dclist: [ { dclist: [ 'A', 'B' ], factor: 'new pl.FIXED_BIN(10)' }, 'C = new pl.FLOAT(5)' ],
        factor: '.EXTERNAL()'
     * }
     * @returns var A = new pl.FIXED_BIN(10).EXTERNAL();
     *          var B = new pl.FIXED_BIN(10).EXTERNAL();
     *          var C = new pl.FLOAT(5).EXTERNAL();
     */
    dclist(ast) {
        let ret = '';
        let factor = ast.factor;
        if (factor && factor.constructor.name === 'String' && factor.charAt(0) === 'n') {
            factor = ' = ' + factor;
        }
        for (const dcl of ast.dclist) {
            switch (dcl.constructor.name) {
                case 'String':
                    this.cmntFactor.forEach(tobecomnt => {
                        if (factor.search(tobecomnt) >= 0) ret += '// ';
                    });
                    ret += "var " + dcl + factor + ";\n";
                    break;
                case 'Object':
                    if (dcl.factor) {
                        dcl.factor += factor;
                    } else {
                        dcl.factor = factor
                    }
                    ret += this.dclist(dcl);
                    break;
                default:
                    console.log("TODO: dclist " + dcl);
            }

        }
        return ret;
    }

    dclistnum2tree(ast) {
        let item;
        let ret = [];
        while (item = ast.shift()) {
            if (item.dclist.length > 1) {
                const newdclist = item.dclist.map(dcl => {
                    if (typeof dcl === 'string') {
                        return { NUM: item.NUM, dclist: [dcl], factor: item.factor };
                    } else {
                        dcl.factor.push(item.factor);
                        return dcl;
                    }
                });
                ret.push(...newdclist);
            } else {
                ret.push(item);
            }
        }
        return ret;
    }

    /**
     * Converts numed declaration to tree
     * @param {*} ast 
     * dcl 1 A, 2 LST(9), 3 (B,C,D) (3,2) binary fixed (15), 2 TRM char(5);
     * ast:
     * [ { NUM: 1, dclist: [ 'A' ], factor: [] },
     *   { NUM: 2, dclist: [ 'LST' ], factor: [ 9 ] },
     *   { NUM: 3, dclist: [ 'B' ], factor: [ 3, 2, 'new pl.FIXED_BIN(15)' ] },
     *   { NUM: 3, dclist: [ 'C' ], factor: [ 3, 2, 'new pl.FIXED_BIN(15)' ] },
     *   { NUM: 3, dclist: [ 'D' ], factor: [ 3, 2, 'new pl.FIXED_BIN(15)' ] },
     *   { NUM: 2, dclist: [ 'END' ], factor: 'new pl.CHAR(5)' } ]
     * @returns 
     * { dclist: [ 'A' ], 
     *   factor: [],
     *   members: [ {
            dclist: [ 'LST' ],
            factor: [ 9 ],
            members: [ { dclist: [ 'B' ], factor: [ 3, 2, 'new pl.FIXED_BIN(15)' ] },
                       { dclist: [ 'C' ], factor: [ 3, 2, 'new pl.FIXED_BIN(15)' ] },
                       { dclist: [ 'D' ], factor: [ 3, 2, 'new pl.FIXED_BIN(15)' ] } ]
            },
            { dclist: [ 'END' ], factor: 'new pl.CHAR(5)' }
        ]
     * }
     */
    dclnum2tree(start) {
        let ret = [];
        let item = {};
        let next_num = 0;
        for (; start.i < this.tree.length; start.i++) {
            (start.i + 1 < this.tree.length) ? (next_num = this.tree[start.i + 1].NUM) : (next_num = 0);
            item = { dclist: this.tree[start.i].dclist, factor: this.tree[start.i].factor };
            ret.push(item);
            if (next_num > this.tree[start.i].NUM) {
                const i = start.i;
                start.i++;
                item["members"] = this.dclnum2tree(start);
                (start.i + 1 < this.tree.length) ? (next_num = this.tree[start.i + 1].NUM) : (next_num = 0);
                if (this.tree[i].NUM > next_num)
                    return ret;
            } else if (next_num < this.tree[start.i].NUM) {
                return ret;
            }
        }
        return ret;
    }

    /**
     * dcl 1 A (2,2), (2 B (2), 3 C, 3 D, 2 E) fixed bin(15);
     * @param { [ 2, 2 ] } factor 
     * @param {*} members 
     * members: [
     *      { dclist: [ 'B' ],
     *          factor: [ 2, 'new pl.FIXED_BIN(15)' ],
     *          members: [
     *              { dclist: [ 'C' ], factor: [ 'new pl.FIXED_BIN(15)' ] },
     *              { dclist: [ 'D' ], factor: [ 'new pl.FIXED_BIN(15)' ] }
     *          ]
     *      },
     *      { dclist: [ 'E' ], factor: [ 'new pl.FIXED_BIN(15)' ] }
     *  ]
     * @returns
     * A: pl.ARR(2, 2, {
     *      B: pl.ARR(2, {
     *          C: new pl.FIXED_BIN(15),
     *          D: new pl.FIXED_BIN(15)
     *      }),
     *      E: new pl.FIXED_BIN(15)
     *  })
     */
    numfactor2str(factor, members) {
        let ret = '';
        if (Array.isArray(factor) && factor.length) {
            ret = "new pl.ARR(";
            for (const f of factor) {
                Array.isArray(f) ? ret += "\"" + f[0] + ":" + f[1] + "\", " : ret += String(f) + ", ";
            }
            if (members)
                ret += "{\n" + this.members2str(members) + "}";
            ret += ")";
        } else if (typeof factor === 'string') {
            ret += factor;
        } else {
            ret += "{\n" + this.members2str(members) + "}";
        }
        return ret;
    }

    members2str(members) {
        let ret = '';
        for (const member of members) {
            if (member.members) {
                ret += member.dclist[0] + ": " + this.numfactor2str(member.factor, member.members) + ",\n";
            } else if (Array.isArray(member.factor) && member.factor.length > 1) {
                ret += member.dclist[0] + ": " + this.numfactor2str(member.factor, member.members) + ",\n";
            } else {
                ret += member.dclist[0] + ": " + member.factor + ",\n";
            }
        }
        return ret;
    }

    toString() {
        let ret = '';
        if (this.tree.length === 1) {
            const dcl = this.tree[0];
            if (dcl.constructor.name === 'String') {
                this.cmntFactor.forEach(tobecomnt => {
                    if (dcl.search(tobecomnt) >= 0) ret += '// ';
                });
                ret += `var ${dcl};`;
            } else if (dcl.constructor.name === 'Array') {
                console.log("TODO: DclStmt>Array " + dcl);
            } else if (dcl.constructor.name === 'Object') {
                ret += this.dclist(dcl);
            } else {
                console.log("TODO: code 23234254565");
            }
        } else if (this.tree.length > 1) {
            // console.dir(this.tree, { depth: null });
            this.tree = this.dclistnum2tree(this.tree);
            // console.dir(this.tree, { depth: null });
            this.tree = this.dclnum2tree({ i: 0 });
            this.tree = this.tree[0];
            if (Array.isArray(this.tree.factor)) {
                const factorstr = this.numfactor2str(this.tree.factor, this.tree.members);
                if (this.tree.factor.length) {
                    this.cmntFactor.forEach(tobecomnt => {
                        if (factorstr.search(tobecomnt) >= 0) ret += '// ';
                    });
                    ret += "var " + this.tree.dclist[0] + " = " + factorstr + ";";
                } else {
                    ret += "var " + this.tree.dclist[0] + " = new pl.objInit(" + factorstr + ");";
                }
            } else {
                ret += "var " + this.tree.dclist[0] + " = new pl.objInit(" + this.numfactor2str([], this.tree.members) + ")" + this.tree.factor + ";";
            }

        }

        return ret;
    }
};



module.exports = { Plexer, DclStmt };
module.exports.pl2js = pl2js;
module.exports.ims = ims;
