# syntax=docker/dockerfile:1
FROM node:14-alpine
WORKDIR /code
COPY package*.json ./
RUN npm install
EXPOSE 23567
COPY . .
COPY ./ ./node_modules/plexer
CMD ["node", "tn3270e.js"]