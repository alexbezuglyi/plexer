const { Worker } = require('worker_threads');
const pl = require("./pl2.js");
const {segs} = require("./segments");
let worker = undefined;

function SetWorker(worker_path) {
    const fs = require("fs");
    try {
        var workerraw = fs.readFileSync(worker_path, "utf8");
        worker = new Worker(workerraw, {eval: true});
        worker.on('error', (code) => {
            if (code !== 0)
                throw (new Error(`Worker stopped with error code ${code}`));
        });
    } catch (e) {
        console.error(e);
    }
}

function dli(IMS_FUNC, pcb, IO_AREA, ...SSAs) {
    if (worker === undefined)
        throw "worker is undefined";
    let iopcb = pl.objInit({
        lterm_name      : new pl.CHAR(8),
        reserved        : new pl.CHAR(2),
        status          : new pl.CHAR(2),
        date_time       : new pl.CHAR(8),
        msg_seq         : new pl.FIXED_BIN(31),
        mod_name        : new pl.CHAR(8),
    });
    
    let dbpcb = pl.objInit({
        dbd_name        : new pl.CHAR(8),
        seg_level       : new pl.CHAR(2),       /* LEVEL OF LAST GOOD GET    */
        status          : new pl.CHAR(2),
        proc_options    : new pl.CHAR(4),       /* PCB PROCESSING OPTION     */
        dbd_reserved    : new pl.FIXED_BIN(31),
        seg_name        : new pl.CHAR(8),       /* SEGMENT NAME LAST GOOD GET*/
        length_fb_key   : new pl.FIXED_BIN(31), /* LENGTH(KEY_FEEDBACK)      */
        num_sens_segs   : new pl.FIXED_BIN(31),
        keyfba          : new pl.CHAR(1),       /* Variable Length Field allocated by IMS */
        key_fb_area     : new pl.CHAR(50)       /* FULLY CONCAT KEY LAST USED*/
    });
    
    pcb.unpack(iopcb);
    pcb.unpack(dbpcb);
    iopcb.base = pcb;
    dbpcb.base = pcb;

    if (dbpcb.seg_level !== "" && dbpcb.seg_level !== " " && Number(dbpcb.seg_level)) {
        
        let key = dbpcb.dbd_name;
        let segsize = 0;
        for (const ssa of SSAs) {
            const seglexed = /^([A-Z\s]{1,8})\(([A-Z]{1,8})[<=>\s]+(.+)\)/g.exec(ssa);
            const ssa_seg = seglexed[1];
            const ssa_field = seglexed[2];
            const ssa_value = seglexed[3];
            if (ssa_field == segs[ssa_seg]["key"]) {
                key += `:${ssa_value}`;
                const seg = segs[ssa_seg];
                segsize = seg.base.size;
            } else {
                throw "TODO:";
            }
            if (segsize == 0) {
                iopcb.status.v = 'VG';
                return;
            }
        }
        if (segsize == 0) segsize = 4096; //todo: segsize from another place

        const sab = new SharedArrayBuffer(Math.ceil(segsize/4)*4);
        const int32 = new Int32Array(sab);
        let buf = Buffer.from(sab);
        switch(String(IMS_FUNC)) {
            case "GHU ":
            case "GU  ":
                dbpcb.key_fb_area.v = key;
                worker.postMessage(["get", key, sab]); 
                Atomics.wait(int32, 0, 0);

                if (IO_AREA instanceof pl.CHAR) {
                    buf.copy(IO_AREA.buf, 0, 0, IO_AREA.buf.length);
                    IO_AREA.bump_subs();
                } else if (IO_AREA.constructor.name == "Object") {
                    let charbuf = new pl.CHAR(0);
                    charbuf.buf = buf;
                    charbuf.unpack(IO_AREA);
                } else {
                    throw("redis.get: Unacceptable object: " + prop.constructor.name);
                }

                iopcb.status.v = '  ';
                
                break;
            case "ISRT":
                // ret = redis.set(key, IO_AREA.buf, redis.print);
                break;
            case "REPL":
                let replsize = 0;
                if (IO_AREA instanceof pl.CHAR) {
                    replsize = IO_AREA.buf.length;
                    IO_AREA.buf.copy(buf, 0, 0, IO_AREA.buf.length);
                } else if (IO_AREA.constructor.name === "Object" && IO_AREA.base !== undefined) {
                    replsize = IO_AREA.size;
                    IO_AREA.base.buf.copy(buf, 0, 0, replsize);
                } else {
                    iopcb.status.v = 'BU';
                    throw("redis.rpush: Unacceptable object: " + prop.constructor.name);
                }
                worker.postMessage(["set", dbpcb.key_fb_area.native, sab, replsize]); 
                break;
            default:
                throw "TODO:"
        }
    } else {
        const ltermshift = 8;
        const sab = new SharedArrayBuffer(Math.ceil((IO_AREA.size + ltermshift) / 4) * 4);
        const int32 = new Int32Array(sab);
        let buf = Buffer.from(sab);
        switch(String(IMS_FUNC)) {
            case "GU  ":
                worker.postMessage(["blpop", `T${iopcb.mod_name}`, sab]); 
                Atomics.wait(int32, 0, 0);

                iopcb.lterm_name.v = String(buf.toString('ebcdic2ascii'));

                if (IO_AREA instanceof pl.CHAR) {
                    buf.copy(IO_AREA.buf, 0, ltermshift);
                    IO_AREA.bump_subs();
                } else if (IO_AREA.constructor.name == "Object") {
                    let tmpbuf = new pl.CHAR(IO_AREA.size);
                    buf.copy(tmpbuf.buf, 0, ltermshift);
                    tmpbuf.unpack(IO_AREA);
                } else {
                    throw("redis.blpop: Unacceptable object: " + prop.constructor.name);
                }

                iopcb.status.v = '  ';
                break;
            case "ISRT":
                if (SSAs.length) buf.write(SSAs[0], 0, 8, "ascii2ebcdic"); //new format at first 8 bytes
                const rpushsize = IO_AREA.size + ltermshift;
                if (IO_AREA instanceof pl.CHAR) {
                    IO_AREA.buf.copy(buf, ltermshift, 0);
                } else if (IO_AREA.constructor.name === "Object" && IO_AREA.base !== undefined) {
                    IO_AREA.base.buf.copy(buf, ltermshift, 0);
                } else {
                    iopcb.status.v = 'BU';
                    throw("redis.rpush: Unacceptable object: " + prop.constructor.name);
                }

                worker.postMessage(["rpush", `S${iopcb.mod_name}:${iopcb.lterm_name}`, sab, rpushsize]); 
                
                break;
            case "PURG":
                break;
            default:
                throw "TODO: unknown IMS_FUNC"
        }
    }
    

}

function tdli(parcnt, IMS_FUNC, pcb, IO_AREA, ...SSAs) {
    dli(IMS_FUNC, pcb, IO_AREA, ...SSAs);
}

module.exports = { dli, tdli, SetWorker };